var deEvent = function() {
	return {
		restrict: 'A',
		link: function(scope, elem, attrs) {
			var frm = angular.element('.form-horizontal');
			elem.bind('keyup', function (e) {
				var atoms = frm.find('input, select');
				toAtom = null;
				for (var i = atoms.length - 1; i >= 0; i--) {
	                if (atoms[i] === e.target) {
	                	//console.log(elem[0].localName);

	                    if (e.keyCode === 38 && elem[0].localName != 'select') {
	                        toAtom = atoms[i - 1];
	                    } else if (e.keyCode === 40 && elem[0].localName != 'select') {
	                        toAtom = atoms[i + 1];
	                    } else if (e.keyCode === 13) {
	                        toAtom = atoms[i + 1];
	                    }
	                    break;
	                }
	            }

	            if (toAtom) toAtom.focus();
	            return false;
				
			});
		}
	}
}

var deCenter = function() {
    return {
        restrict : 'A', 
        link : function($scope,$element,$attr) {
        	//console.log()
            $scope.$watch($attr.deCenter,function(focusVal) {
                if(focusVal === true) {
                    setTimeout(function() {
                    	console.log($element);
                        $element.focus();
                        //focusVal=false;
                    },100);
                }
            });
        }
    }
}