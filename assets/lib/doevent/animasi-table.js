/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create    : mbahsomo
 * Nama File    : animasi-table.js
 */
$(document).keyup(function(e)
{
    switch (e.which)
    {
        case 38:
            if (!e.ctrlKey) {
                var selected = $("tr.success").first();
                if (selected.prev().html() != null) {
                    selected.prev().addClass("success");
                    selected.removeClass("success");
                }
            }
            break;
        case 40:
            if (!e.ctrlKey) {
                var selected = $("tr.success").first();
                if (selected.next().html() != null) {
                    selected.next().addClass("success");
                    selected.removeClass("success");
                }
            }
            break;
    }
});