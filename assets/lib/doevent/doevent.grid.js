(function (window, document) {
    // Modules
    var doeventGrid = angular.module('ngDoeventrow',[]);
    doeventGrid.factory('objRow',function($rootScope){
        var objRow = [];
        var objRowId = 0 ;
        var objRowService = {};

        objRowService.getObjRow = function (){
            return objRow;
        }

        objRowService.setObjRow = function (data){
            objRow = data;
        }

        objRowService.setObjRowId  = function (tyid){
            objRowId = tyid;
        }

        objRowService.getObjRow  = function (){
            return objRow;
        }

        return objRowService;
    });
    doeventGrid.directive('ngDoeventrow', function (objRow) {
        return {
            restrict: 'A',
            scope: { data: '=ngDoeventrow', evtReturn:"&setObjRows"},
            link: function(scope, elem, attrs) {
                elem.bind('keyup', function (event) {
                    switch (event.which){
                        case 38:
                            if (!event.ctrlKey) {
                                if (elem.prev().html() != null) {
                                    //scope.evtReturn(scope.data);
                                    elem.prev().addClass("success");
                                    elem.removeClass("success");
                                    elem[0].previousElementSibling.focus();
                                }
                            }
                            break;
                        case 40:
                            if (!event.ctrlKey) {
                                if (elem.next().html() != null) {
                                    //scope.evtReturn(scope.data);
                                    elem.next().addClass("success");
                                    elem.removeClass("success");
                                    elem[0].nextElementSibling.focus();
                                }
                            }
                            break;
                        case 69:                            
                            var btn = elem.find('td a.btn-success');
                            btn.click();
                            return false;
                            break;
                        case 68:                            
                            var btn = elem.find('td a.btn-danger');
                            btn.click();
                            return false;
                            break;
                    }
                    
                })
            }
        }
    });
    
    var doeventGrid = angular.module('dGrid',['ngDoeventrow']);
    doeventGrid.directive('dGrid',function(){
        return{
            restrict : 'A',
            scope : {option:'=option'},
            link : function(scope, elem, attrs){
                var op = scope.option;
                var grid = elem.find('div.dGrid');
                var thgrid = elem.find('div.th');
                
                elem.attr('style','width:' + op.width + ';');
                grid.attr('style','width:' + op.width + ';');
                var styleth = [];
                angular.forEach(thgrid, function(value, key){
                    var th = elem.find(value);
                    styleth[key] = th.attr('style');                    
                });
            }
        }
    });

})(window, document);