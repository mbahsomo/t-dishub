<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */
        
class T_Controller extends CI_Controller {

    public $cid = "";
    public $fn ="";

    function __construct() {
        parent::__construct();
        $this->cid = $this->uri->rsegment(1);
        $this->fn = ($this->uri->rsegment(2)!='')?$this->uri->rsegment(2):'index';
        if(!$this->_check_akses()){
            redirect('/awal/login/', 'refresh');
        }
//        if(!$this->_check_akses())
//            show_error("<div class='alert alert-info'>Anda tidak mempunyai hak akses [$this->cid/$this->fn ] </div><br><a class='btn btn-danger' href='".site_url('awal/login')."'>Silakan Login Lagi</a>" , 404 );
    }

    function index(){}

    public function access_rules() {}

    private function _check_akses(){
        $rule = $this->access_rules();
        $control = false;
        $expression = false;
        if(is_array($rule)){
            $bil=0;
            foreach ($rule as $key => $value) {
                $expression = $value['expression'];
                if($value[0]=='allow'){
                    for ($a = 0 ; $a < count($value['actions']); $a++){
                        if($this->fn==$value['actions'][$a]){
                            $control = true;
                            break;
                        }
                    }
                    if($control)
                        break;
                }else if($value[0]=='deny' && $control){
                    $control = true;
                }
                
            }
            if($control && $expression){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
    }

    public function load_template($view,$data_dtl=null, $data_master = array()){
        $data['content'] =  $this->load->view($view,$data_dtl,true) ;
		$data = array_merge($data,$data_master);
        $this->load->view('template/main_view', $data,false);
    }

    public function get_max_page($ttl, $limit){
        $vmod = $ttl % $limit;
        return ($ttl-$vmod);
    }

    public function cetak(){
        $this->load->library('table');
        $this->table->set_caption($this->cid);
        $tmpl = array (
            'table_open' => '<table border="1" style="border: 1px solid black;border-collapse:collapse;font-family:sans-serif;
font-size:8pt;text-transform: capitalize;width:100%;">'
        );

        $this->table->set_template($tmpl);

        $head = array();
        $fields = array();
        foreach ($this->mdl->get_rule() as $val) {
            if (isset($val['grid'])) {
                if ($val['grid']){
                    $head[] = $this->mdl->get_label($val['field']) ;
                    $fields[] = $val['field'];
                }
            }else{
                $head[] = $this->mdl->get_label($val['field']);
                $fields[] = $val['field'];
            }
        }
        $this->table->set_heading($head);
        echo $this->table->generate( $this->mdl->get_all($fields) );
    }

}

/* End of file tcontroller.php */
/* Location: .//home/alif/project/public_html/t-rokok/payroll/controllers/tcontroller.php */