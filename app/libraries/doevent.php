<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: Doevent.php
 */

class Doevent {
    function angka2text($text,$panjang) {
        $hasil='';
        for($a=0; $a< $panjang-strlen($text); $a++ ) {
            $hasil.='0';
        }
        $hasil.=$text;
        return $hasil;
    }

    function get_nm_bulan($bil) {
        $arrBulan = $this->set_nm_bulan();
        return $arrBulan[$bil];
    }

	function set_nm_bulan() {
        return array(
                1=>'JANUARI',
                2=>'FEBRUARI',
                3=> 'MARET',
                4=> 'APRIL',
                5=> 'MEI',
                6=> 'JUNI',
                7=> 'JULI',
                8=> 'AGUSTUS',
                9=> 'SEPTEMBER',
                10=> 'OKTOBER',
                11=> 'NOVEMBER',
                12=> 'DESEMBER'
        );
        
    }

    public function rpt2xls($html, $nmfl='export.xls'){
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment; filename=".$nmfl);
        header("Pragma: no-cache");
        header("Expires: 0");
        $rpt = explode("<!--detail-->", $html);
        echo $rpt[1];
        exit();
    }
    
    public function set_tanggal($date){
        return  date( 'Y-m-d', strtotime($date));// substr($date, -4,4) . '-'. substr($date, 3,2) . '-'. substr($date, 0,2);
    }
    
    /**
    * 01012014
    */
    public function set_date($str){
        return substr($str, 4,4) . '-' . substr($str, 2,2) . '-' . substr($str, 0,2);
    }

    public function get_date($date){
        return date('d-m-Y', strtotime($date) );
    }
} 