<?php
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

 /**
 * User Create	: 
 * Nama File	: T_array_helper.php
 */

function  get_user_akses($arr,$fn, $evt=''){
	$nilai = 'F';
	foreach ($arr as  $rows) {
		if(strtoupper($rows['mn_link'])==strtoupper($fn)){
			$nilai = $rows[$evt];
			break;
		}		
	}
	if($nilai=='T'){
		return 1;
	}else{
		return 0;
	}

}