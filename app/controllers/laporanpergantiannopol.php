<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: laporanpergantiannopol.php
 */
class Laporanpergantiannopol extends T_Controller {

	private $doe ;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Po_model','mdl');
		$this->load->library('Doevent');
		$this->doe = new Doevent();
	}

	public function index()
	{
		echo 'not set';	
	}

	private function _cetak($params,$title){
		$this->mdl->set_null();
		$this->mdl->set_fields('tbl_po.*, bu_name, k_name');
        $this->mdl->set_join(array(
            array(
               'TABLE'  => 'tbl_badan_usaha',
               'FIELD'  => 'tbl_badan_usaha.bu_id='. $this->mdl->get_table() . '.bu_id',
               'JOIN'   => 'inner'
            ),
            array(
                'TABLE'  => 'tbl_kota',
                'FIELD'  => 'tbl_kota.k_code='. $this->mdl->get_table() . '.k_code',
                'JOIN'   => 'inner'
            )
        ));
		$this->mdl->set_params($params);
		$data['rec'] = $this->mdl->get_data();
		$data['title'] = $title;
		return $this->load->view('reports/laporanpo', $data,true);
	}

	public function cetakpertanggal($tgl1,$tgl2){
		$param = array(
			'po_tgl_akhir >=' => $tgl1,
			'po_tgl_akhir <=' => $tgl2
		);
		//$title = 'Periode ' . $this->doe->get_nm_bulan( date('m', strtotime($tgl1) ) ) . ' s/d ' . $this->doe->get_nm_bulan( date('m', strtotime($tgl2) ) );
		$title = 'Tgl ' . date('d-m-Y', strtotime($tgl1) )  . ' s/d ' . date('d-m-Y', strtotime($tgl2) );
		echo $this->_cetak($param, $title);
	}

	public function exportpertanggal($tgl1,$tgl2){
		$param = array(
			'po_tgl_akhir >=' => $tgl1,
			'po_tgl_akhir <=' => $tgl2
		);
		$title = 'Tgl ' . date('d-m-Y', strtotime($tgl1) )  . ' s/d ' . date('d-m-Y', strtotime($tgl2) );
		$this->doe->rpt2xls($this->_cetak($param, $title));
	}

	public function cetakperbulan($bln,$thn){
		$param = array(
			'month(po_tgl_akhir)' => $bln,
			'year(po_tgl_akhir)' => $thn
		);
		$title = 'Periode ' . $this->doe->get_nm_bulan( $bln ) . ' tahun ' . $thn;
		echo $this->_cetak($param, $title);
	}

	public function exportperbulan($bln,$thn){
		$param = array(
			'month(po_tgl_akhir)' => $bln,
			'year(po_tgl_akhir)' => $thn
		);
		$title = 'Periode ' . $this->doe->get_nm_bulan( $bln ) . ' tahun ' . $thn;
		$this->doe->rpt2xls($this->_cetak($param, $title));
	}

	public function cetakpertahun($thn){
		$param = array(
			'year(po_tgl_akhir)' => $thn
		);
		$title = 'Periode  tahun ' . $thn;
		echo $this->_cetak($param, $title);
	}

	public function exportpertahun($thn){
		$param = array(
			'year(po_tgl_akhir)' => $thn
		);
		$title = 'Periode tahun ' . $thn;
		$this->doe->rpt2xls($this->_cetak($param, $title));
	}

}

/* End of file laporanpergantiannopol.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/controllers/laporanpergantiannopol.php */