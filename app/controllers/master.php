<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: master.php
 */
class Master extends T_Controller {

    var $stop = 0;

    function __construct() {
        parent::__construct();
        $this->stop = BATAS_REC;
    }

    public function access_rules() {
        return array(
            array('allow',
                'actions' => array('trayekkendaraan','tarif', 'trayek', 'po','user', 'pejabat', 'propinsi', 'kota', 'terminal', 'layanan', 'jenisangkutan', 'jenistrayek', 'badan_usaha','kendaraan'),
                'expression' => get_user_akses($this->session->userdata('user_menu'), $this->cid . '/' . $this->fn, 'um_status')
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }
    
    public function trayekkendaraan(){
        $this->load->model('Po_kendaraan_model', 'mdl');
        $data['rec'] = $this->mdl->search('po_code', '%', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('trayekkendaraan/index_view', $data, array('path' => true, 'info' => ''));
    }
    
    public function tarif() {
        $this->load->model('Tarif_model', 'mdl');
        $this->load->model('Terminal_model');
        $data['terminal'] = $this->Terminal_model->get_all();
        $data['rec'] = $this->mdl->search('ta_asal', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('tarif/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function trayek() {
        $this->load->model('Trayek_model', 'mdl');
        $this->load->model('Layanan_model');
        $this->load->model('Jenis_trayek_model');
        $this->load->model('Terminal_model');
        $this->load->model('Jalan_model');
        $data['layanan'] = $this->Layanan_model->get_all();
        $data['terminal'] = $this->Terminal_model->get_all();
        $data['jalan'] = $this->Jalan_model->get_all();
        $data['jenis_trayek'] = $this->Jenis_trayek_model->get_all();
        $data['rec'] = $this->mdl->search('ty_name', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('trayek/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function po() {
        $this->load->model('Po_model', 'mdl');
        $this->load->model('Kota_model');
        $this->load->model('Badan_usaha_model');
        $data['kota'] = $this->Kota_model->get_all();
        $data['badan_usaha'] = $this->Badan_usaha_model->get_all();
        $data['rec'] = $this->mdl->search('po_name', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('po/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function user() {
        $this->load->model('User_model', 'mdl');
        $data['rec'] = $this->mdl->search('u_name', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('user/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function pejabat() {
        $this->load->model('Pejabat_model', 'mdl');
        $data['rec'] = $this->mdl->search('p_name', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('pejabat/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function propinsi() {
        $this->load->model('Propinsi_model', 'mdl');
        $data['rec'] = $this->mdl->search('p_name', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('propinsi/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function kota() {
        $this->load->model('Kota_model', 'mdl');
        $this->load->model('Propinsi_model');
        $data['rec'] = $this->mdl->search('k_code', '', 0, $this->stop);
        $data['propinsi'] = $this->Propinsi_model->get_all();
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('kota/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function terminal() {
        $this->load->model('Terminal_model', 'mdl');
        $this->load->model('Kota_model');
        $data['rec'] = $this->mdl->search('t_code', '', 0, $this->stop);
        $data['kota'] = $this->Kota_model->get_all();
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('terminal/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function layanan() {
        $this->load->model('Layanan_model', 'mdl');
        $data['rec'] = $this->mdl->search('l_name', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('layanan/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function jenisangkutan() {
        $this->load->model('Jenis_angkutan_model', 'mdl');
        $data['rec'] = $this->mdl->search('ja_name', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('jenisangkutan/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function jenistrayek() {
        $this->load->model('Jenis_trayek_model', 'mdl');
        $data['rec'] = $this->mdl->search('jt_name', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('jenistrayek/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function badan_usaha() {
        $this->load->model('Badan_usaha_model', 'mdl');
        $data['rec'] = $this->mdl->search('bu_name', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('badan_usaha/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function kendaraan() {
        $this->load->model('Po_bus_model', 'mdl');
        $data['rec'] = $this->mdl->search('pob_nopol', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('kendaraan/index_view', $data, array('path' => true, 'info' => ''));
    }

}

/* End of file master.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/controllers/master.php */