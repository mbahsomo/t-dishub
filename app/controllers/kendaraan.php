<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: kendaraan.php
 */
class Kendaraan extends T_Controller {

	var $stop;

    public function __construct() {
        parent::__construct();
        $this->load->model('Po_bus_model', 'mdl');
        $this->stop = BATAS_REC;
    }

    /*public function access_rules() {
        return array(
            array('allow',
                'actions' => array('index','insert_data', 'edit_data', 'delete_data', 'search'),
                'expression' => $this->session->userdata('login')
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }*/

    public function index() {
        $data['rec'] = $this->mdl->get_data();
        $this->load_template($this->cid . '/index_view', $data);
    }

    public function insert_data() {
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $_POST = $_GET;
        $this->form_validation->set_rules($this->mdl->get_rule(true));
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            //echo $error;
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => $this->mdl->insert(), 'key' => $this->mld->get_rec_id())));
        }
    }

    public function edit_data() {
        $this->load->model('Setting_model', 'mdl');
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $_POST = $_GET;
        $this->form_validation->set_rules($this->mdl->get_rule(false));
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => $this->mdl->update($this->input->get('pok_id', true)))));
        }
    }

    public function delete_data() {
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                    array(
                        'success' => $this->mdl->delete($this->input->get('pok_id', true)),
                        'max_page' => $this->get_max_page($this->mdl->get_tot_rows(), $this->stop)
                    )
        ));
    }

    public function search() {
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                    array(
                        'success' => true,
                        'rec' => $this->mdl->search(
                            $this->input->get('field', true), $this->input->get('value', true), $this->input->get('stop', true), $this->stop
                        ),
                        'max_page' => $this->get_max_page($this->mdl->get_tot_rows(), $this->stop)
                    )
        ));
    }

    public function cetak_nota($id = '%') {
        $this->load->model('Po_kendaraan_trayek_model');
        $data['rec'] = $this->mdl->search('pok_id', $id, 0, 1);
        $data['kota'] = $this->Po_kendaraan_trayek_model->get_group_trayek($id);
        $data['trayek'] = $this->Po_kendaraan_trayek_model->search('pok_id',$id,0,100);
        $this->load->view('reports/kps', $data, FALSE);
    }

    public function get_all() {
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                array(
                    'rec' => $this->mdl->get_all('*')
                )
        ));
    }

}

/* End of file kendaraan.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/t-dishub/app/controllers/kendaraan.php */