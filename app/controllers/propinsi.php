<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: propinsi.php
 */
class Propinsi extends T_Controller {

	var $stop;
	public function __construct(){
		parent::__construct();
		$this->load->model('Propinsi_model', 'mdl');
		$this->stop = BATAS_REC;
	}

	public function index()
	{
		$data['rec'] = $this->mdl->get_data();
		$this->load_template($this->cid . '/index_view',$data);
	}

	public function insert_data(){
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
		$_POST=$_GET;
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
			//echo $error;
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode(array('success' => false, 'msg' => $error)));
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => $this->mdl->insert(),'key'=> $this->mdl->get_rec_id() )));
        }
    }

    public function edit_data(){
        $this->load->model('Setting_model','mdl');
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
		$_POST=$_GET;
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => $this->mdl->update( $this->input->get('p_id',true) ))));
        }
    }

    
    public function delete_data(){
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
				array(
					'success' => $this->mdl->delete( $this->input->get('p_id',true)),
					'max_page' => $this->get_max_page($this->mdl->get_tot_rows(),$this->stop) 
				)
			));
    }

    public function search(){
    	$this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
            	array(
            		'success' => true, 
            		'rec'=> $this->mdl->search( 
            			$this->input->get('field',true),
            			$this->input->get('value',true),
            			$this->input->get('stop',true),
            			$this->stop
        			),
        			'max_page' => $this->get_max_page($this->mdl->get_tot_rows(),$this->stop)
        		)
        	));	
    }

}

/* End of file propinsi.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/controllers/propinsi.php */