<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: pejabat.php
 */
class Pejabat extends T_Controller {

	var $stop;
	public function __construct(){
		parent::__construct();
		$this->load->model('Pejabat_model', 'mdl');
		$this->stop = BATAS_REC;
	}

	public function index()
	{
		$data['rec'] = $this->mdl->get_data();
		$this->load_template($this->cid . '/index_view',$data);
	}

	public function insert_data(){
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
		$_POST=$_GET;
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
			//echo $error;
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode(array('success' => false, 'msg' => $error)));
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => $this->mdl->insert(),'key'=>$this->mdl->get_rec_id() )));
        }
    }

    public function edit_data(){
        $this->load->model('Setting_model','mdl');
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
		$_POST=$_GET;
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => $this->mdl->update( $this->input->get('p_id',true)) )));
        }
    }

    
    public function delete_data(){
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
				array(
					'success' => $this->mdl->delete( $this->input->get('p_id',true)),
					'max_page' => $this->get_max_page($this->mdl->get_tot_rows(),$this->stop) 
				)
			));
    }

    public function search(){
    	$this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
            	array(
            		'success' => true, 
            		'rec'=> $this->mdl->search( 
            			$this->input->get('field',true),
            			$this->input->get('value',true),
            			$this->input->get('stop',true),
            			$this->stop
        			),
        			'max_page' => $this->get_max_page($this->mdl->get_tot_rows(),$this->stop)
        		)
        	));	
    }

    /*public function cetak(){
        $this->load->library('table');
        $this->table->set_caption($this->cid);
        $tmpl = array (
            'table_open' => '<table border="1" style="border: 1px solid black;border-collapse:collapse;font-family:sans-serif;
font-size:8pt;text-transform: capitalize;width:100%;">'
        );

        $this->table->set_template($tmpl);

        $head = array();
        $fields = array();
        foreach ($this->mdl->get_rule() as $val) {
            if (isset($val['grid'])) {
                if ($val['grid']){
                    $head[] = $this->mdl->get_label($val['field']) ;
                    $fields[] = $val['field'];
                }
            }else{
                $head[] = $this->mdl->get_label($val['field']) ;
                $fields[] = $val['field'];
            }
        }
        $this->table->set_heading($head);
        echo $this->table->generate( $this->mdl->get_all($fields) );
    }*/

    public function xls(){

    }

    public function pdf(){

    }
}

/* End of file pejabat.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/controllers/pejabat.php */