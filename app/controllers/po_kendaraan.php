<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: po_kendaraan.php
 */
class Po_kendaraan extends T_Controller {

    var $stop;

    public function __construct() {
        parent::__construct();
        $this->load->model('Po_kendaraan_model', 'mdl');
        $this->stop = BATAS_REC;
    }

    public function access_rules() {
        return array(
            array('allow',
                'actions' => array('insert_data', 'edit_data', 'delete_data', 'search', 'cetak_nota','cetak_lampiran1','cetak_lampiran2','get_perpo','get_perpoty'),
                'expression' => $this->session->userdata('login')
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }

    public function index() {
        $data['rec'] = $this->mdl->get_data();
        $this->load_template($this->cid . '/index_view', $data);
    }

    public function insert_data() {
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $_POST = $_GET;
        $this->form_validation->set_rules($this->mdl->get_rule(true));
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            //echo $error;
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => $this->mdl->insert(), 'key' => $this->mld->get_rec_id())));
        }
    }

    public function edit_data() {
        $this->load->model('Setting_model', 'mdl');
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $_POST = $_GET;
        $this->form_validation->set_rules($this->mdl->get_rule(false));
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        } else {
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => $this->mdl->update($this->input->get('pok_id', true)))));
        }
    }

    public function delete_data() {
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                    array(
                        'success' => $this->mdl->delete($this->input->get('pok_id', true)),
                        'max_page' => $this->get_max_page($this->mdl->get_tot_rows(), $this->stop)
                    )
        ));
    }

    public function search() {
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                    array(
                        'success' => true,
                        'rec' => $this->mdl->search(
                            $this->input->get('field', true), $this->input->get('value', true), $this->input->get('stop', true), $this->stop
                        ),
                        'max_page' => $this->get_max_page($this->mdl->get_tot_rows(), $this->stop)
                    )
        ));
    }

    public function cetak_nota($id = '%') {
        $this->load->model('Po_kendaraan_trayek_model');
        $data['rec'] = $this->mdl->search('pok_id', $id, 0, 1);
        $data['kota'] = $this->Po_kendaraan_trayek_model->get_group_trayek($id);
        $data['trayek'] = $this->Po_kendaraan_trayek_model->search('pok_id',$id,0,100);
        $this->load->view('reports/kps', $data, FALSE);
    }

    public function cetak_lampiran1($po='',$trayek='') {
        $this->load->model('Po_kendaraan_model');
        $this->Po_kendaraan_model->set_null();
        $this->Po_kendaraan_model->set_fields('tbl_po_kendaraan.*,ty_name, ja_name, po_name, po_alamat, po_nosk, po_tgl_sk');
        $this->Po_kendaraan_model->set_join(array(
            array(
                'TABLE' => 'tbl_jenis_angkutan',
                'FIELD' => 'tbl_jenis_angkutan.ja_id=tbl_po_kendaraan.ja_id',
                'JOIN' => 'inner'
            ), array(
                'TABLE' => 'tbl_po',
                'FIELD' => 'tbl_po.po_code=tbl_po_kendaraan.po_code',
                'JOIN' => 'inner'
            ), array(
                'TABLE' => 'tbl_trayek',
                'FIELD' => 'tbl_trayek.ty_code=tbl_po_kendaraan.ty_code',
                'JOIN' => 'left'
            )
        ));
        //ubah dari asal ke tujuan atau PP
        $berangkat = substr($trayek, 0,6);
        $tujuan = substr($trayek,3, 3) . substr($trayek,0, 3);
        $this->Po_kendaraan_model->set_where_in(
            'substr(tbl_po_kendaraan.ty_code,0,6)' , array($berangkat,$tujuan)
        );
        $this->Po_kendaraan_model->set_params(array('tbl_po_kendaraan.po_code'=>$po));
        $data['rec'] = $this->Po_kendaraan_model->get_data();
        $this->load->view('reports/lampiran1', $data, FALSE);
    }    

    public function cetak_lampiran2($po='',$trayek='') {
        $this->load->model('Po_kendaraan_model');
        $this->Po_kendaraan_model->set_null();
        $this->Po_kendaraan_model->set_fields('tbl_po_kendaraan.*, tbl_trayek.ty_code,ty_name, ja_name, po_name, po_alamat, po_nosk, po_tgl_sk, asal.t_name as asal, tujuan.t_name as tujuan, pokt_status, pokt_rit1, pokt_rit2');
        $this->Po_kendaraan_model->set_join(array(
            array(
                'FIELD' => 'tbl_po_kendaraan_trayek.pok_id = tbl_po_kendaraan.pok_id ',
                'TABLE' => 'tbl_po_kendaraan_trayek',
                'JOIN'  => 'inner',
            ),array(
                'TABLE' => 'tbl_jenis_angkutan',
                'FIELD' => 'tbl_jenis_angkutan.ja_id=tbl_po_kendaraan.ja_id',
                'JOIN' => 'inner'
            ),array(
                'TABLE' => 'tbl_po',
                'FIELD' => 'tbl_po.po_code=tbl_po_kendaraan.po_code',
                'JOIN' => 'inner'
            ),array(
                'FIELD' => 'tbl_trayek.ty_code = tbl_po_kendaraan.ty_code ',
                'TABLE' => 'tbl_trayek',
                'JOIN'  => 'inner',
            ),array(
                'TABLE' => 'tbl_terminal as asal',
                'FIELD' => 'asal.t_code = tbl_trayek.t_code_asal',
                'JOIN' => 'inner'
            ), array(
                'TABLE' => 'tbl_terminal as tujuan',
                'FIELD' => 'tujuan.t_code = tbl_trayek.t_code_tujuan',
                'JOIN' => 'inner'
            )
        ));
        //ubah dari asal ke tujuan atau PP
        $berangkat = substr($trayek, 0,6);
        $tujuan = substr($trayek,3, 3) . substr($trayek,0, 3);
        /*$this->Po_kendaraan_model->set_where_in(
            'substr(tbl_trayek.ty_code,0,6)' , array($berangkat,$tujuan)
        );*/
        $this->Po_kendaraan_model->set_params(array('tbl_po_kendaraan.po_code'=>$po));
        $data['rec'] = $this->Po_kendaraan_model->get_data();
        $this->load->view('reports/lampiran2', $data, FALSE);
    }

    public function get_perpo($po){
        $this->mdl->set_null();
        $this->mdl->set_fields('tbl_po_kendaraan.ty_code,ty_name');
        $this->mdl->set_params(array(
            'po_code'=>$po
        ));
        $this->mdl->set_join(array(
            array(
                'TABLE' => 'tbl_trayek',
                'FIELD' => 'tbl_trayek.ty_code=tbl_po_kendaraan.ty_code',
                'JOIN'  => 'inner'
            )
        ));
        $this->mdl->set_groupby('ty_code,ty_name');
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                array(
                    'success' => true, 
                    'rec'=> $this->mdl->get_data()
                )
            )); 
    }

    public function get_perpoty($po,$ty){
        $this->mdl->set_null();
        $this->mdl->set_fields('tbl_po_kendaraan.*,ty_name');
        $this->mdl->set_join(array(
            array(
                'TABLE' => 'tbl_trayek',
                'FIELD' => 'tbl_trayek.ty_code=tbl_po_kendaraan.ty_code',
                'JOIN'  => 'inner'
            )
        ));
        $this->mdl->set_params(array(
            'tbl_po_kendaraan.po_code'=>$po
        ));
        $this->mdl->set_where_in('tbl_po_kendaraan.ty_code',array($ty,$this->_get_ty($ty)));
        $this->mdl->set_orderby('tbl_po_kendaraan.ty_code');
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                array(
                    'success' => true, 
                    'rec'=> $this->mdl->get_data()
                )
            )); 
    }
    private function _get_ty($ty){
        $kd1 = substr($ty, 0,3);
        $kd2 = substr($ty, 3,3);
        $kd3 = substr($ty, 6,2);
        return $kd2.$kd1.$kd3;
    }

}

/* End of file po_kendaraan.php */
/* Location: ./application/controllers/po_kendaraan.php */