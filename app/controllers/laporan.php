<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: laporan.php
 */
class Laporan extends T_Controller{
    var $stop = 0;
    var $de;
    function __construct(){
        parent::__construct();
        $this->stop = BATAS_REC;
        $this->load->library('Doevent');
        $this->de = new Doevent();
    }

    public function access_rules() {
        return array(
            array('allow',
                'actions' => array('laporanpo','laporankartupengawasan','laporanpergantiannopol','laporanperpanjanganstuk'),
                'expression' => $this->session->userdata('login')
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }

    public function laporanpo(){        
        $data['bulan'] = $this->de->set_nm_bulan();
        $this->load_template('laporanpo/index_view',$data, array('path'=>true));
    }

    public function laporankartupengawasan(){
        $data['bulan'] = $this->de->set_nm_bulan();
        $this->load_template('laporanpo/index_view',$data, array('path'=>true));
    }

    public function laporanpergantiannopol(){
        $data['bulan'] = $this->de->set_nm_bulan();
        $this->load_template('laporanpo/index_view',$data, array('path'=>true));
    }

    public function laporanperpanjanganstuk(){
        $data['bulan'] = $this->de->set_nm_bulan();
        $this->load_template('laporanpo/index_view',$data, array('path'=>true));
    }
} 