<?php

/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: awal.php
 */
class Awal extends T_Controller {

    var $stop = 0;

    function __construct() {
        parent::__construct();
        $this->stop = BATAS_REC;
    }

    public function access_rules() {
        return array(
            array('allow',
                'actions' => array('index'),
                'expression' => $this->session->userdata('login')
            ),
            array('allow',
                'actions' => array('login', 'dologin', 'logout'),
                'expression' => true
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }

    public function index() {
        $this->load->library('Doevent');
        $doe = new Doevent();
        $this->load->model('Po_model');
        $this->Po_model->set_null();
        $sql1 = 'SELECT 
                (select count(po_code) from tbl_po where year(po_tgl_akhir) =year (now()) and month(po_tgl_akhir)=1 ) as bln1,
                (select count(po_code) from tbl_po where year(po_tgl_akhir) =year (now()) and month(po_tgl_akhir)=2 ) as bln2,
                (select count(po_code) from tbl_po where year(po_tgl_akhir) =year (now()) and month(po_tgl_akhir)=3 ) as bln3,
                (select count(po_code) from tbl_po where year(po_tgl_akhir) =year (now()) and month(po_tgl_akhir)=4 ) as bln4,
                (select count(po_code) from tbl_po where year(po_tgl_akhir) =year (now()) and month(po_tgl_akhir)=5 ) as bln5,
                (select count(po_code) from tbl_po where year(po_tgl_akhir) =year (now()) and month(po_tgl_akhir)=6 ) as bln6,
                (select count(po_code) from tbl_po where year(po_tgl_akhir) =year (now()) and month(po_tgl_akhir)=7 ) as bln7,
                (select count(po_code) from tbl_po where year(po_tgl_akhir) =year (now()) and month(po_tgl_akhir)=8 ) as bln8,
                (select count(po_code) from tbl_po where year(po_tgl_akhir) =year (now()) and month(po_tgl_akhir)=9 ) as bln9,
                (select count(po_code) from tbl_po where year(po_tgl_akhir) =year (now()) and month(po_tgl_akhir)=10 ) as bln10,
                (select count(po_code) from tbl_po where year(po_tgl_akhir) =year (now()) and month(po_tgl_akhir)=11 ) as bln11,
                (select count(po_code) from tbl_po where year(po_tgl_akhir) =year (now()) and month(po_tgl_akhir)=12 ) as bln12';
        $hasil1 = array();
        foreach ($this->Po_model->exec_query($sql1, true) as $key => $value) {
            $hasil1[] = $value['bln1'];
            $hasil1[] = $value['bln2'];
            $hasil1[] = $value['bln3'];
            $hasil1[] = $value['bln4'];
            $hasil1[] = $value['bln5'];
            $hasil1[] = $value['bln6'];
            $hasil1[] = $value['bln7'];
            $hasil1[] = $value['bln8'];
            $hasil1[] = $value['bln9'];
            $hasil1[] = $value['bln10'];
            $hasil1[] = $value['bln11'];
            $hasil1[] = $value['bln12'];
        }

        $sql2 = 'SELECT 
                (select count(po_code) from tbl_po_kendaraan where year(pok_tgl_uji) =year (now()) and month(pok_tgl_uji)=1 ) as bln1,
                (select count(po_code) from tbl_po_kendaraan where year(pok_tgl_uji) =year (now()) and month(pok_tgl_uji)=2 ) as bln2,
                (select count(po_code) from tbl_po_kendaraan where year(pok_tgl_uji) =year (now()) and month(pok_tgl_uji)=3 ) as bln3,
                (select count(po_code) from tbl_po_kendaraan where year(pok_tgl_uji) =year (now()) and month(pok_tgl_uji)=4 ) as bln4,
                (select count(po_code) from tbl_po_kendaraan where year(pok_tgl_uji) =year (now()) and month(pok_tgl_uji)=5 ) as bln5,
                (select count(po_code) from tbl_po_kendaraan where year(pok_tgl_uji) =year (now()) and month(pok_tgl_uji)=6 ) as bln6,
                (select count(po_code) from tbl_po_kendaraan where year(pok_tgl_uji) =year (now()) and month(pok_tgl_uji)=7 ) as bln7,
                (select count(po_code) from tbl_po_kendaraan where year(pok_tgl_uji) =year (now()) and month(pok_tgl_uji)=8 ) as bln8,
                (select count(po_code) from tbl_po_kendaraan where year(pok_tgl_uji) =year (now()) and month(pok_tgl_uji)=9 ) as bln9,
                (select count(po_code) from tbl_po_kendaraan where year(pok_tgl_uji) =year (now()) and month(pok_tgl_uji)=10 ) as bln10,
                (select count(po_code) from tbl_po_kendaraan where year(pok_tgl_uji) =year (now()) and month(pok_tgl_uji)=11 ) as bln11,
                (select count(po_code) from tbl_po_kendaraan where year(pok_tgl_uji) =year (now()) and month(pok_tgl_uji)=12 ) as bln12';
        $hasil2 = array();
        $this->Po_model->set_null();
        foreach ($this->Po_model->exec_query($sql2, true) as $key => $value) {
            $hasil2[] = $value['bln1'];
            $hasil2[] = $value['bln2'];
            $hasil2[] = $value['bln3'];
            $hasil2[] = $value['bln4'];
            $hasil2[] = $value['bln5'];
            $hasil2[] = $value['bln6'];
            $hasil2[] = $value['bln7'];
            $hasil2[] = $value['bln8'];
            $hasil2[] = $value['bln9'];
            $hasil2[] = $value['bln10'];
            $hasil2[] = $value['bln11'];
            $hasil2[] = $value['bln12'];
        }
        $nm_bulan = array();
        foreach ($doe->set_nm_bulan() as $key => $value) {
            $nm_bulan[] = $value;
        }
        //$hasil = $this->Po_model->exec_query($sql,true);
        //Untuk PO
        $this->load->model('Kota_model');
        $this->load->model('Badan_usaha_model');
        $this->load->model('Po_kendaraan_model');
        $this->load->model('Po_kendaraan_trayek_model');
        $data['kota'] = $this->Kota_model->get_all();
        $data['badan_usaha'] = $this->Badan_usaha_model->get_all();
        $rec = array();
        foreach ($this->Po_model->search('po_name', '', 0, $this->stop) as $rows) {
            $recd = array();
            foreach ($this->Po_kendaraan_model->search('po_code', $rows['po_code'], 0, 10000) as $rdetail) {
                $rdetail['terminal'] = $this->Po_kendaraan_trayek_model->get_group_trayek($rdetail['pok_id']);
                $recd[] = $rdetail;
            }
            $rows['detail'] = $recd;
            $rec[] = $rows;
        }
        $data['rec'] = $rec; //$this->Po_model->search('po_name','',0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->Po_model->get_tot_rows(), $this->stop);

        $data['grafik_po'] = $hasil1;
        $data['grafik_kendaraan'] = $hasil2;
        $data['nm_bulan'] = $nm_bulan;
        $this->load_template('template/awal_view', $data);
    }

    public function login() {
        $this->load->view('template/login_view');
    }

    public function dologin() {
        $this->load->model('User_model', 'mdl');
        //$rec = $this->input->post('record',true);

        $this->mdl->set_null();
        $this->mdl->set_cetak_query(false);
        $this->mdl->set_table('tbl_user');
        $this->mdl->set_fields('count(*) as ada');
        $this->mdl->set_params(array(
            'u_name' => $this->input->post('nama', true),
            'u_password' => md5($this->input->post('pass', true))
        ));
        $dt = $this->mdl->get_data();
        if ($dt[0]['ada'] > 0) {
            //$program = "theme";
            if ($this->input->post('app', true) == '1') {
                $program = "payroll";
            } else if ($this->input->post('app', true) == '2') {
                $program = "keuangan";
            } else if ($this->input->post('app', true) == '3') {
                $program = "produksi";
            }
            $session['login'] = 1;
            $session['user_name'] = $this->input->post('nama', true);
            $this->load->model('User_menu_model');
            $session['user_menu'] = $this->User_menu_model->search('u_name', $this->input->post('nama', true), 0, 100);
            /*
             * $session['sess_expiration'] = 0;
             * */
            if($this->input->post('ingat',true)===true){
                $session['sess_expiration'] = 0;
            }
            $this->session->set_userdata($session);
            redirect('awal');
        } else {
            $this->load->view('template/login_view');
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url());
    }

}

/* End of file awal.php */
/* Location: ./application/controller/awal.php */