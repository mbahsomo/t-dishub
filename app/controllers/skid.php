<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: skid.php
 */
class Skid extends T_Controller {

	var $stop;
    var $de ; 
	public function __construct(){
		parent::__construct();
		$this->load->model('Skid_model', 'mdl');
		$this->stop = BATAS_REC;
        $this->load->library('Doevent');
        $this->de = new Doevent();
	}

	public function index()
	{
		$data['rec'] = $this->mdl->get_data();
		$this->load_template($this->cid . '/index_view',$data);
	}

	public function insert_data(){
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $_GET['skid_tgl'] =  $this->de->set_date($_GET['skid_tgl']);
        $_GET['skid_tgl_awal'] = $this->de->set_date($_GET['skid_tgl_awal']);
        $_GET['skid_tgl_akhir'] = $this->de->set_date($_GET['skid_tgl_akhir']);
        $_GET['skid_tgl_nosk'] = $this->de->set_date($_GET['skid_tgl_nosk']);
		$_POST=$_GET;
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
			//echo $error;
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode(array('success' => false, 'msg' => $error)));
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => $this->mdl->insert(),'key'=> $this->mdl->get_rec_id() )));
        }
    }

    public function edit_data(){
        $this->load->model('Setting_model','mdl');
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('form_validation'));
        $_GET['skid_tgl'] =  $this->de->set_date($_GET['skid_tgl']);
        $_GET['skid_tgl_awal'] = $this->de->set_date($_GET['skid_tgl_awal']);
        $_GET['skid_tgl_akhir'] = $this->de->set_date($_GET['skid_tgl_akhir']);
        $_GET['skid_tgl_nosk'] = $this->de->set_date($_GET['skid_tgl_nosk']);
		$_POST=$_GET;
        $this->form_validation->set_rules($this->mdl->get_rule());
        if ($this->form_validation->run() == FALSE) {
            $error = validation_errors();
                $this->output
                    ->set_content_type('application/json')
                    ->set_output(json_encode(array('success' => false, 'msg' => $error)));
        }else{
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('success' => $this->mdl->update( $this->input->get('skid_code',true) ))));
        }
    }

    
    public function delete_data(){
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
				array(
					'success' => $this->mdl->delete( $this->input->get('skid_code',true)),
					'max_page' => $this->get_max_page($this->mdl->get_tot_rows(),$this->stop) 
				)
			));
    }

    public function search(){
    	$this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
            	array(
            		'success' => true, 
            		'rec'=> $this->mdl->search( 
            			$this->input->get('field',true),
            			$this->input->get('value',true),
            			$this->input->get('stop',true),
            			$this->stop
        			),
        			'max_page' => $this->get_max_page($this->mdl->get_tot_rows(),$this->stop)
        		)
        	));	
    }

    public function get_skid_pertryake($po,$ty){
        $this->mdl->set_null();
        $this->mdl->set_fields('*');
        $this->mdl->set_params(array(
            'po_code'=>$po,
            'ty_code'=>$ty
        ));
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode(
                array(
                    'success' => true, 
                    'rec'=> $this->mdl->get_data()
                )
            )); 
        /*$this->mdl->set_join(array(
            array(
                'TABLE' => 'tbl_po',
                'FIELD' => 'tbl_po.po_code=tbl_skid.po_code',
                'JOIN'  => 'inner'
            ),array(
                'TABLE' => 'tbl_trayek',
                'FIELD' => 'tbl_po.po_code=tbl_skid.po_code',
                'JOIN'  => 'inner'
            )
        ));*/
    }

}

/* End of file skid.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/controllers/skid.php */