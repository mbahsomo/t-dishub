<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: entry.php
 */
class Entry extends T_Controller {

    var $stop = 0;

    function __construct() {
        parent::__construct();
        $this->stop = BATAS_REC;
    }

    public function access_rules() {
        return array(
            array('allow',
                'actions' => array('skid','tarif', 'trayek', 'po', 'kartupengawasan', 'pergantian_nomor_kendaraan', 'perpanjangan_stuk'),
                'expression' => get_user_akses($this->session->userdata('user_menu'), 'entry/' . $this->fn, 'um_status')
            ),
            array('deny',
                'expression' => false,
            ),
        );
    }

    public function skid(){
        $this->load->model('Skid_model', 'mdl');
        $data['rec'] = $this->mdl->search('skid_code', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('skid/index_view',$data,array('path' => true, 'info' => ''));
    }

    public function tarif() {
        $this->load->model('Tarif_model', 'mdl');
        $this->load->model('Terminal_model');
        $data['terminal'] = $this->Terminal_model->get_all();
        $data['rec'] = $this->mdl->search('ta_asal', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('tarif/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function trayek() {
        $this->load->model('Trayek_model', 'mdl');
        $this->load->model('Layanan_model');
        $this->load->model('Jenis_trayek_model');
        $this->load->model('Terminal_model');
        $this->load->model('Kota_model');
        $data['layanan'] = $this->Layanan_model->get_all();
        $data['terminal'] = $this->Terminal_model->get_all();
        $data['kota'] = $this->Kota_model->get_all();
        $data['jenis_trayek'] = $this->Jenis_trayek_model->get_all();
        $data['rec'] = $this->mdl->search('ty_name', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('trayek/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function po() {
        $this->load->model('Po_model', 'mdl');
        $this->load->model('Kota_model');
        $this->load->model('Badan_usaha_model');
        $data['kota'] = $this->Kota_model->get_all();
        $data['badan_usaha'] = $this->Badan_usaha_model->get_all();
        $data['rec'] = $this->mdl->search('po_name', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('po/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function kartupengawasan() {
        $this->load->model('Po_kendaraan_model', 'mdl');
        $this->load->model('Po_model');
        $this->load->model('Jenis_angkutan_model');
        $this->load->model('Trayek_model');
        $this->load->model('Terminal_model');
        $data['terminal'] = $this->Terminal_model->get_all();
        $data['ty'] = $this->Trayek_model->get_all();
        $data['ja'] = $this->Jenis_angkutan_model->get_all();
        $data['po'] = $this->Po_model->get_all('po_code', 'po_name', 'po_alamat');
        $data['rec'] = $this->mdl->search('', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('kartupengawasan/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function pergantian_nomor_kendaraan() {
        $this->load->model('Po_kendaraan_ubah_model', 'mdl');
        $this->load->model('Po_kendaraan_model');
//        $data['kendaraan'] = $this->Po_kendaraan_model->get_all('pok_nopol,pok_id, 
//			(select po_name from tbl_po where po_code = tbl_po_kendaraan.po_code) as nama, 
//			(select po_alamat from tbl_po where po_code = tbl_po_kendaraan.po_code) as alamat');
        $data['rec'] = $this->mdl->search('poku_nopol_lama', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
//        $data['max_page_kendaraan'] = $this->get_max_page($this->Po_kendaraan_model->get_tot_rows(), $this->stop);
        $this->load_template('kendaraan_ubah/index_view', $data, array('path' => true, 'info' => ''));
    }

    public function perpanjangan_stuk() {
        $this->load->model('Po_kendaraan_stuk_model', 'mdl');
        $this->load->model('Po_kendaraan_model');
        $data['kendaraan'] = $this->Po_kendaraan_model->get_all();
        $data['rec'] = $this->mdl->search('poks_id', '', 0, $this->stop);
        $data['stop'] = $this->stop;
        $data['max_page'] = $this->get_max_page($this->mdl->get_tot_rows(), $this->stop);
        $this->load_template('perpanjangan_stuk/index_view', $data, array('path' => true, 'info' => ''));
    }

}

/* End of file entry.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/controllers/entry.php */