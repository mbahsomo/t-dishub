<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	:
 * Nama File	: tarif_model.php
 */
class Tarif_model extends T_Model {

	private $field = array();

    function __construct() {
        parent::T_model();
        $this->set_table('tbl_tarif');
        $this->set_key_field( 'ta_id' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();

        for ($i=0; $i < count($this->field) ; $i++) {
            $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
        }
        $fields['user_entry'] = $this->session->userdata('user_name');
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }

    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function getAll() {
        $this->set_null();
        $this->set_cetak_query(false);
        return $this->get_data();
    }

    public function get_rule($insert=true) {
        return array(
            array(
                'field' => 'ta_asal',
                'label' => 'Treminal Asal',
                'rules' => 'xss_clean|max_length[3]|min_length[3]|required'
            ),array(
                'field' => 'ta_tujuan',
                'label' => 'Terminal  Tujuan',
                'rules' => 'xss_clean|max_length[3]|min_length[3]|required'
            ),array(
                'field' => 'ta_atas',
                'label' => 'Batas Atas',
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 'ta_bawah',
                'label' => 'Batas Bawah',
                'rules' => 'xss_clean|numeric|required'
            )
        );
    }

    public function search($field='ta_asal', $value='%', $start=0, $stop=5){
    	$this->set_null();
    	$this->set_cetak_query(false);
        $this->set_fields('tbl_tarif.*,tasal.t_name as asal, ttujuan.t_name as tujuan');
        $this->set_join(array(
           array(
               'TABLE'  => 'tbl_terminal as tasal',
               'FIELD'  => 'tasal.t_code=tbl_tarif.ta_asal',
               'JOIN'   => 'inner'
           ),array(
                'TABLE'  => 'tbl_terminal as ttujuan',
                'FIELD'  => 'ttujuan.t_code=tbl_tarif.ta_tujuan',
                'JOIN'   => 'inner'
            )
        ));
        $this->set_start($start);
        $this->set_stop($stop);
        $this->set_like(array(
        	$field=>$value
    	));
        $this->set_orderby('date_edit desc');
        return $this->get_data();
    }

	public function get_all($fields = '*', $params = ''){
        $this->set_null();
        $this->set_fields('*');
        return $this->get_data();
    }

}

/* End of file tarif_model.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/models/tarif_model.php */
