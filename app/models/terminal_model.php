<?php
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: terminal_model.php
 */
class Terminal_model extends T_Model{

    private $field = array();

    function __construct() {
        parent::T_model();
        $this->set_table('tbl_terminal');
        $this->set_key_field( 't_code' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();

        for ($i=0; $i < count($this->field) ; $i++) {
            $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
        }
        $fields['user_entry'] = $this->session->userdata('user_name');
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }

    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function getAll() {
        $this->set_null();
        $this->set_cetak_query(false);
        return $this->get_data();
    }

    public function get_rule($insert=true) {
        return array(
            array(
                'field' => 't_code',
                'label' => 'Kode terminal',
                'rules' => 'xss_clean|max_length[3]|min_length[3]|required'
            ),array(
                'field' => 'k_code',
                'label' => 'Kota',
                'rules' => 'xss_clean|max_length[3]|min_length[3]|required'
            ),array(
                'field' => 't_name',
                'label' => 'Nama',
                'rules' => 'xss_clean|max_length[45]|required'
            ),array(
                'field' => 't_alamat',
                'label' => 'Alamat',
                'rules' => 'xss_clean|max_length[100]'
            ),array(
                'field' => 't_type',
                'label' => 'Status',
                'rules' => 'xss_clean|max_length[1]'
            )
        );
    }

    public function search($field='t_code', $value='%', $start=0, $stop=5){
        $this->set_null();
        $this->set_cetak_query(false);
        $this->set_fields('tbl_terminal.*,k_name');
		$this->set_join(array(
			array(
				'TABLE'	=> 'tbl_kota',
				'FIELD'	=> 'tbl_kota.k_code=tbl_terminal.k_code',
				'JOIN'	=> 'inner'
			)
		));
        $this->set_start($start);
        $this->set_stop($stop);
        $this->set_like(array(
            $field=>$value
        ));
        $this->set_orderby('date_edit desc');
        return $this->get_data();
    }

    
}

/* End of file terminal.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/models/terminal.php */
