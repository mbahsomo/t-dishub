<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: trayek_model.php
 */
class Trayek_model extends T_Model {

    private $field = array();

    function __construct() {
        parent::T_model();
        $this->set_table('tbl_trayek');
        $this->set_key_field('ty_code');
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();

        for ($i = 0; $i < count($this->field); $i++) {
            $fields[$this->field[$i]] = $this->input->post($this->field[$i], true);
        }
        $fields['user_entry'] = $this->session->userdata('user_name');
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() => $id));
        return $this->update_data();
    }

    public function delete($id) {
        $this->set_null();
        $this->set_params(array($this->get_key_field() => $id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function getAll() {
        $this->set_null();
        $this->set_cetak_query(false);
        return $this->get_data();
    }

    public function get_rule($insert=true) {
        return array(
            array(
                'field' => 'ty_code',
                'label' => 'KD Trayek',
                'rules' => 'xss_clean|max_length[9]|required|min_length[9]'
            ),array(
                'field' => 'ty_name',
                'label' => 'Nama Trayek',
                'rules' => 'xss_clean|max_length[45]|required'
            ), array(
                'field' => 't_code_asal',
                'label' => 'Terminal Asal',
                'rules' => 'xss_clean|max_length[3]|min_length[3]'
            ), array(
                'field' => 't_code_tujuan',
                'label' => 'Terminal Tujuan',
                'rules' => 'xss_clean|max_length[3]|min_length[3]'
            ), array(
                'field' => 'ty_normal',
                'label' => 'Nilai Tarif',
                'rules' => 'xss_clean|numeric|required'
            ), array(
                'field' => 'jt_id',
                'label' => 'Jenis Trayek',
                'rules' => 'xss_clean|numeric|required'
            ), array(
                'field' => 'l_id',
                'label' => 'Pelayanan',
                'rules' => 'xss_clean|numeric|required'
            ), array(
                'field' => 'ty_type',
                'label' => 'Type Trayek',
                'rules' => 'xss_clean'
            )
        );
    }

    public function search($field = 'ty_name', $value = '%', $start = 0, $stop = 5) {
        $this->load->model('Trayek_jalan_model');
        $this->set_null();
        $this->set_fields('tbl_trayek.*, asal.t_name as asal, tujuan.t_name as tujuan');
        $this->set_join(array(
            array(
                'TABLE' => 'tbl_terminal as asal',
                'FIELD' => 'asal.t_code = tbl_trayek.t_code_asal',
                'JOIN' => 'inner'
            ), array(
                'TABLE' => 'tbl_terminal as tujuan',
                'FIELD' => 'tujuan.t_code = tbl_trayek.t_code_tujuan',
                'JOIN' => 'inner'
            )
        ));
        $this->set_start($start);
        $this->set_stop($stop);
        $this->set_like(array(
            $field => $value
        ));
        $this->set_orderby('date_edit desc');
        $rec = array();
        foreach ($this->get_data() as $rows) {
            $rows['detail'] = $this->Trayek_jalan_model->search( 'ty_code', $rows['ty_code'] ,100 );
            $rec[] = $rows;
        }
        return $rec;
    }

    public function get_all($fields = '*', $params = '') {
        $this->set_null();
        $this->set_fields('tbl_trayek.*, asal.t_name as tasal, tujuan.t_name as ttujuan');
        $this->set_join(array(
            array(
                'TABLE' => 'tbl_terminal as asal',
                'FIELD' => 'asal.t_code = tbl_trayek.t_code_asal',
                'JOIN' => 'inner'
            ), array(
                'TABLE' => 'tbl_terminal as tujuan',
                'FIELD' => 'tujuan.t_code = tbl_trayek.t_code_tujuan',
                'JOIN' => 'inner'
            )
        ));
        return $this->get_data();
    }

}

/* End of file trayek_model.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/models/trayek_model.php */
