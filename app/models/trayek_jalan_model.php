<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: trayek_jalan_model.php
 */
class Trayek_jalan_model extends T_Model {

    private $field = array();

    function __construct() {
        parent::T_model();
        $this->set_table('tbl_trayek_jalan');
        $this->set_key_field('tyj_id');
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();

        for ($i = 0; $i < count($this->field); $i++) {
            if ($this->field[$i] != 'tyj_id') {
                $fields[$this->field[$i]] = $this->input->post($this->field[$i], true);
            }
        }
        $fields['user_entry'] = $this->session->userdata('user_name');
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() => $id));
        return $this->update_data();
    }

    public function delete($id) {
        $this->set_null();
        $this->set_params(array($this->get_key_field() => $id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function getAll() {
        $this->set_null();
        $this->set_cetak_query(false);
        return $this->get_data();
    }

    public function get_rule($insert=true) {
        return array(
            array(
                'field' => 'ty_code',
                'label' => 'Trayek',
                'rules' => 'xss_clean|numeric|required'
            ), array(
                'field' => 'j_code',
                'label' => 'Jalan',
                'rules' => 'xss_clean|max_length[8]|min_length[8]|required'
            ), array(
                'field' => 'tyj_urut',
                'label' => 'No Urut',
                'rules' => 'xss_clean|numeric'
            )
        );
    }

    public function search($field = 'k_code', $value = '%', $start = 0, $stop = 5) {
        $this->set_null();
        $this->set_cetak_query(false);
        $this->set_fields('tbl_trayek_jalan.*, j_name');
        $this->set_join(array(
            array(
                'TABLE' => 'tbl_jalan',
                'FIELD' => 'tbl_jalan.j_code=tbl_trayek_jalan.j_code',
                'JOIN' => 'inner'
            )
        ));
        /* $this->set_start($start);
          $this->set_stop($stop); */
        $this->set_like(array(
            $field => $value
        ));
        $this->set_orderby('tyj_urut');
        return $this->get_data();
    }

}

/* End of file trayek_jalan_model.php */
/* Location: ./application/models/trayek_jalan_model.php */
