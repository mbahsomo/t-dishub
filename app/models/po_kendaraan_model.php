<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: po_kendaraan_model.php
 */
class Po_kendaraan_model extends T_Model {

    private $field = array();

    function __construct() {
        parent::T_model();
        $this->set_table('tbl_po_kendaraan');
        $this->set_key_field('pok_id');
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();

        for ($i = 0; $i < count($this->field); $i++) {
            if ($this->field[$i] != 'pok_id'){
                if ($this->field[$i]=== 'pok_tgl_kps'){
                    $fields[$this->field[$i]] = date('d-m-Y', strtotime($this->input->post($this->field[$i], true)));
                }else{
                    $fields[$this->field[$i]] = $this->input->post($this->field[$i], true);
                }
            }

        }
        $fields['user_entry'] = $this->session->userdata('user_name');
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() => $id));
        return $this->update_data();
    }

    public function delete($id) {
        $this->set_null();
        $this->set_params(array($this->get_key_field() => $id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function getAll() {
        $this->set_null();
        $this->set_cetak_query(false);
        return $this->get_data();
    }

    public function get_rule($insert = true) {
        $rl = array(
            array(
                'field' => 'pok_nopol',
                'label' => 'NOPOL',
                'width' => '100px',
                'rules' => 'xss_clean|max_length[20]|required'
            ), array(
                'field' => 'po_code',
                'label' => 'No PO',
                'width' => '150px',
                'fieldLabel' => 'po_name',
                'rules' => 'xss_clean|max_length[5]|min_lenght[5]|required'
            ), array(
                'field' => 'pok_nostuk',
                'label' => 'No STUK',
                'width' => '100px',
                'rules' => 'xss_clean|max_length[45]|required'
            ), array(
                'field' => 'pok_tgl_uji',
                'label' => 'Tgl Uji',
                'width' => '80px',
                'rules' => 'xss_clean'
            ), array(
                'field' => 'pok_merk',
                'label' => 'Merk',
                'rules' => 'xss_clean|max_length[45]',
                'width' => '120px'
            ), array(
                'field' => 'pok_type',
                'label' => 'Type',
                'rules' => 'xss_clean|max_length[45]',
                //'width' => '100px'
            ), array(
                'field' => 'pok_tahun_buat',
                'label' => 'Thn Buat',
                'grid' => false,
                'rules' => 'xss_clean|numeric'
            ), array(
                'field' => 'pok_nochasis',
                'label' => 'No Chasis',
                'grid' => false,
                'rules' => 'xss_clean|max_length[30]'
            ), array(
                'field' => 'pok_nomesin',
                'label' => 'No Mesin',
                'grid' => false,
                'rules' => 'xss_clean|max_length[30]'
            ), array(
                'field' => 'pok_nolambung',
                'label' => 'No Lambung',
                'grid' => false,
                'rules' => 'xss_clean|max_length[30]'
            ), array(
                'field' => 'pok_duduk',
                'label' => 'Tempat Duduk',
                'grid' => false,
                'rules' => 'xss_clean|numeric'
            ), array(
                'field' => 'pok_barang',
                'label' => 'Max Barang',
                'grid' => false,
                'rules' => 'xss_clean|numeric'
            ), array(
                'field' => 'ja_id',
                'label' => 'Jenis Angkutan',
                'fieldLabel' => 'ja_name',
                'rules' => 'xss_clean|numeric|required'
            ), array(
                'field' => 'ty_code',
                'label' => 'Trayek',
                'grid' => false,
                'rules' => 'xss_clean|numeric|required'
            ), array(
                'field' => 'pok_nokps',
                'label' => 'NO KPS',
                'grid' => false,
                'rules' => 'xss_clean|max_length[30]'
            ), array(
                'field' => 'pok_tgl_kps',
                'label' => 'Tgl KPS',
                'grid' => false,
                'rules' => 'xss_clean'
            ), array(
                'field' => 'pok_kps',
                'label' => 'KPS',
                'grid' => false,
                'rules' => 'xss_clean|max_length[1]'
            ), array(
                'field' => 'pok_spk',
                'label' => 'SPK',
                'grid' => false,
                'rules' => 'xss_clean|max_length[1]'
            ), array(
                'field' => 'pok_cetak',
                'label' => 'Cetak',
                'grid' => false,
                'rules' => 'xss_clean|max_length[1]'
            ), array(
                'field' => 'pok_status',
                'label' => 'Status',
                'grid' => false,
                'rules' => 'xss_clean|max_length[1]'
            )
        );
        if (!$insert) {
            return array_merge(
                array(
                array(
                    'field' => 'pok_id',
                    'label' => 'ID',
                    'rules' => 'required|numeric'
                )
                ), $rl
            );
        } else {
            return $rl;
        }
    }

    public function search($field = 'pok_id', $value = '%', $start = 0, $stop = 5, $terminal = '') {
        $this->set_null();
        //$this->set_cetak_query(true);
        $this->set_fields('tbl_po_kendaraan.*,ty_name, ja_name, po_name, po_alamat, po_nosk, po_tgl_sk');
        $this->set_join(array(
            array(
                'TABLE' => 'tbl_jenis_angkutan',
                'FIELD' => 'tbl_jenis_angkutan.ja_id=tbl_po_kendaraan.ja_id',
                'JOIN' => 'inner'
            ), array(
                'TABLE' => 'tbl_po',
                'FIELD' => 'tbl_po.po_code=tbl_po_kendaraan.po_code',
                'JOIN' => 'inner'
            ), array(
                'TABLE' => 'tbl_trayek',
                'FIELD' => 'tbl_trayek.ty_code=tbl_po_kendaraan.ty_code',
                'JOIN' => 'left'
            )
        ));
        $this->set_start($start);
        $this->set_stop($stop);
        if($terminal!==''){
            $this->set_where_in('pok_id', $this->_get_po($terminal) );
        }
        $fieldv = explode(";", $field);
        $valuev = explode(";", $value);
        if (count($valuev) > 0) {
            for ($a = 0; $a < count($fieldv); $a++) {
                if ($valuev[$a] !== '') {
                    $this->set_like(array(
                        'tbl_po_kendaraan.' . $fieldv[$a] => $valuev[$a]
                    ));
                }
            }
        }
        //$this->set_cetak_query(true);
        $this->set_orderby('date_edit desc');
        $arr = array();
        foreach ($this->get_data() as $key =>  $row) {
            $row['pok_tgl_kps'] = date ('d-m-Y', strtotime($row['pok_tgl_kps']) );
            $arr[] = $row;
        }
        return $arr;
    }

    private function _get_po($terminal){
        $this->load->model('Po_kendaraan_trayek_model');
        $this->Po_kendaraan_trayek_model->set_null();
        $this->Po_kendaraan_trayek_model->set_fields('pok_id');
        $this->Po_kendaraan_trayek_model->set_params(array('t_code'=>$terminal));
        $hasil = array();
        foreach ($this->Po_kendaraan_trayek_model->get_data() as $row) {
            $hasil[] = $row['pok_id'];
        }
        
        return $hasil;
    }

}

/* End of file po_kendaraan_model.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/models/po_kendaraan_model.php */