<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: skid_model.php
 */
class Skid_model extends T_Model {

	private $field = array();

    function __construct() {
        parent::T_model();
        $this->set_table('tbl_skid');
        $this->set_key_field( 'skid_code' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();

        for ($i=0; $i < count($this->field) ; $i++) {
            $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
        }
        $fields['user_entry'] = $this->session->userdata('user_name');
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }

    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function getAll() {
        $this->set_null();
        $this->set_cetak_query(false);
        return $this->get_data();
    }

    public function get_rule($insert=true) {
        return array(
            array(
                'field' => 'skid_code',
                'label' => 'Kode',
                'rules' => 'xss_clean|max_length[45]|required'
            ),array(
                'field' => 'skid_tgl',
                'label' => 'Tgl Penetapan',
                'rules' => 'xss_clean|required'
            ),array(
                'field' => 'skid_tgl_awal',
                'label' => 'Tgl Mulai',
                'grid'  => false,
                'rules' => 'xss_clean|required'
            ),array(
                'field' => 'skid_tgl_akhir',
                'label' => 'Tgl Selesai',
                'grid'  => false,
                'rules' => 'xss_clean|required'
            ),array(
                'field' => 'skid_nosk',
                'label' => 'No P2K',
                'rules' => 'xss_clean|max_length[45]|required'
            ),array(
                'field' => 'skid_tgl_nosk',
                'label' => 'Tgl Penetapan SK',
                'grid'  => false,
                'rules' => 'xss_clean|required'
            ),array(
                'field' => 'po_code',
                'label' => 'PO',
                'rules' => 'xss_clean|required|max_length[4]'
            ),array(
                'field' => 'ty_code',
                'label' => 'TRayek',
                'rules' => 'xss_clean|required|max_length[9]'
            ),array(
                'field' => 'skid_jumlah',
                'label' => 'Jml Kendaraan',
                'rules' => 'xss_clean|numeric'
            )
        );
    }

    public function get_all($fields = '*', $params = ''){
        $this->set_null();
        //$this->set_cetak_query(true);
        $this->set_fields('*');
        return $this->get_data();
    }

    public function search($field='skid_code', $value='%', $start=0, $stop=5){
    	$this->set_null();
    	$this->set_cetak_query(false);
        $this->set_fields('tbl_skid.*,po_name, ty_name');
        $this->set_join(array(
            array(
                'TABLE' => 'tbl_po',
                'FIELD' => 'tbl_po.po_code=tbl_skid.po_code',
                'JOIN'  => 'inner'
            ),array(
                'TABLE' => 'tbl_trayek',
                'FIELD' => 'tbl_trayek.ty_code=tbl_skid.ty_code',
                'JOIN'  => 'inner'
            )
        ));
        $this->set_start($start);
        $this->set_stop($stop);
        $this->set_like(array(
        	'tbl_skid.'.$field=>$value
    	));
        $this->set_orderby('date_edit desc');
        return $this->get_data();
    }

}

/* End of file skid_model.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/models/skid_model.php */
