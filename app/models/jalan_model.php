<?php

/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */


/**
 * Description of jalan_model
 *
 * @author mbahsomo
 */
class Jalan_model extends T_Model{
    //put your code here
    function __construct() {
        parent::T_Model();
        $this->set_table('tbl_jalan');
        $this->set_key_field('j_code');
    }

}
