<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: user_menu_model.php
 */
class User_menu_model extends T_Model {
    private $field = array();

    function __construct() {
        parent::T_model();
        $this->set_table('tbl_user_menu');
        $this->set_key_field( 'um_id' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();

        for ($i=0; $i < count($this->field) ; $i++) {
            $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
        }
        /*$fields['user_entry'] = $this->session->userdata('user_name');
        $fields['date_edit'] = date('Y-m-d H:i:s');*/
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        //$fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }

    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function getAll() {
        $this->set_null();
        $this->set_cetak_query(false);
        return $this->get_data();
    }

    public function get_rule($insert=true) {
        return array(
            /*array(
                'field' => 'u_name',
                'label' => 'Nama Menu',
                'rules' => 'xss_clean|max_length[15]|required'
            ),array(
                'field' => 'mn_id',
                'label' => 'Menu',
                'rules' => 'xss_clean|numeric'
            ),*/array(
                'field' => 'um_status',
                'label' => 'Status',
                'rules' => 'xss_clean|max_length[1]|min_length[1]'
            ),array(
                'field' => 'um_insert',
                'label' => 'Insert',
                'rules' => 'xss_clean|max_length[1]|min_length[1]'
            ),array(
                'field' => 'um_update',
                'label' => 'Update',
                'rules' => 'xss_clean|max_length[1]|min_length[1]'
            ),array(
                'field' => 'um_delete',
                'label' => 'Delete',
                'rules' => 'xss_clean|max_length[1]|min_length[1]'
            ),array(
                'field' => 'um_cetak',
                'label' => 'Cetak',
                'rules' => 'xss_clean|max_length[1]|min_length[1]'
            )
        );
    }

    public function search($field='mn_name', $value='%', $start=0, $stop=5){
        $this->set_null();
        $this->set_cetak_query(false);
        $this->set_fields('tbl_user_menu.*, mn_name, mn_link');
        $this->set_join(array(
            array(
                'TABLE'  => 'tbl_menu',
                'FIELD'  => 'tbl_menu.mn_id = tbl_user_menu.mn_id',
                'JOIN'   => 'inner'
            )
        ));
        $this->set_start($start);
        $this->set_stop($stop);
        $this->set_params(array(
            $field=>$value
        ));
        //$this->set_orderby('date_edit desc');
        return $this->get_data();
    }
    
}

/* End of file user_menu_model.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/models/user_menu_model.php */
