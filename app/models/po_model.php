<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: po_model.php
 */
class Po_model extends T_Model {

	private $field = array();

    function __construct() {
        parent::T_model();
        $this->set_table('tbl_po');
        $this->set_key_field( 'po_code' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();
        $this->load->library('Doevent');
        $do = new Doevent();
        for ($i=0; $i < count($this->field) ; $i++) {
            if($this->field[$i]!=='po_tgl_berdiri' &&  
                    $this->field[$i]!=='po_tgl_kps' && 
                    $this->field[$i]!=='po_tgl_akhir' && 
                    $this->field[$i]!=='po_tgl_sk'){
                $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
            }else{ 
                $fields[$this->field[$i]] = $do->set_tanggal($this->input->post($this->field[$i] , true)) ;
            }
        }
        $fields['user_entry'] = $this->session->userdata('user_name');
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }
    
    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function getAll() {
        $this->set_null();
        $this->set_cetak_query(false);
        return $this->get_data();
    }

    public function get_rule($insert=true) {
        return array(
            array(
                'field' => 'po_code',
                'label' => 'ID',
                'rules' => 'xss_clean|max_length[4]|min_length[4]|required'
            ),array(
                'field' => 'po_name',
                'label' => 'Nama PO',
                'rules' => 'xss_clean|max_length[45]|required'
            ),array(
                'field' => 'po_alamat',
                'label' => 'ALamat',
                'rules' => 'xss_clean|max_length[255]'
            ),array(
                'field' => 'po_status',
                'label' => 'Status',
                'grid'  => false,
                'rules' => 'xss_clean'
            ),array(
                'field' => 'k_code',
                'label' => 'Kota',
                'fieldLabel' => 'k_name',
                'rules' => 'xss_clean|max_length[3]|min_length[3]'
            ),array(
                'field' => 'po_telp',
                'label' => 'No Telp',
                'rules' => 'xss_clean|max_length[45]'
            ),array(
                'field' => 'po_tgl_berdiri',
                'label' => 'Tgl Berdiri',
                'grid'  => false,
                'rules' => 'xss_clean'
            ),array(
                'field' => 'po_pemilik',
                'label' => 'Pemilik',
                'rules' => 'xss_clean|max_length[45]'
            ),array(
                'field' => 'po_nowni',
                'label' => 'No WNI',
                'grid'  => false,
                'rules' => 'xss_clean|max_length[45]'
            ),array(
                'field' => 'po_kota_wni',
                'label' => 'Kota WNI',
                'grid'  => false,
                'rules' => 'xss_clean|max_length[45]'
            ),array(
                'field' => 'po_nogama',
                'label' => 'No Gama',
                'grid'  => false,
                'rules' => 'xss_clean|max_length[45]'
            ),array(
                'field' => 'po_kgnama',
                'label' => 'KG Nama',
                'grid'  => false,
                'rules' => 'xss_clean|max_length[45]'
            ),array(
                'field' => 'bu_id',
                'label' => 'BU',
                'fieldLabel' => 'bu_name',
                'rules' => 'xss_clean|numeric'
            ),array(
                'field' => 'po_no_notaris',
                'label' => 'No Notaris',
                'grid'  => false,
                'rules' => 'xss_clean|max_length[45]'
            ),array(
                'field' => 'po_nama_notaris',
                'label' => 'Nama Notaris',
                'grid'  => false,
                'rules' => 'xss_clean|max_length[45]'
            ),array(
                'field' => 'po_tgl_kps',
                'label' => 'Tgl KPS',
                'grid'  => false,
                'rules' => 'xss_clean'
            ),array(
                'field' => 'po_tgl_akhir',
                'label' => 'Tgl Akhir',
                'grid'  => false,
                'rules' => 'xss_clean'
            ),array(
                'field' => 'po_nosk',
                'label' => 'No SK',
                'grid'  => false,
                'rules' => 'xss_clean|max_length[45]'
            ),array(
                'field' => 'po_tgl_sk',
                'label' => 'Tgl SK',
                'grid'  => false,
                'rules' => 'xss_clean'
            )
        );
    }
    
    public function search($field='po_code', $value='%', $start=0, $stop=5){
        $this->load->library('Doevent');
        $do = new Doevent();
    	$this->set_null();
    	$this->set_fields($this->get_table() .'.*, bu_name, k_name');
        //$this->set_cetak_query(true);
        $this->set_join(array(
            array(
               'TABLE'  => 'tbl_badan_usaha',
               'FIELD'  => 'tbl_badan_usaha.bu_id='. $this->get_table() . '.bu_id',
               'JOIN'   => 'inner'
            ),
            array(
                'TABLE'  => 'tbl_kota',
                'FIELD'  => 'tbl_kota.k_code='. $this->get_table() . '.k_code',
                'JOIN'   => 'inner'
            )
        ));
        $this->set_start($start);
        $this->set_stop($stop);
        if ($field==='terminal'){
            $this->set_where_in('po_code', $this->_get_po($value) );
        }else{
            $this->set_like(array(
                $field=>$value
            ));
        }
        $this->set_orderby('date_edit desc');
        $hasil = array();
        foreach ($this->get_data() as $row){
            $row['po_tgl_berdiri']  =  $do->get_date($row['po_tgl_berdiri']);
            $row['po_tgl_kps']  =  $do->get_date($row['po_tgl_kps']);
            $row['po_tgl_akhir']  =  $do->get_date($row['po_tgl_akhir']);
            $row['po_tgl_sk']  =  $do->get_date($row['po_tgl_sk']);
            $hasil[] = $row;
        }
        return $hasil;
    }

    private function _get_po($terminal){
        $this->load->model('Po_kendaraan_trayek_model');
        $this->Po_kendaraan_trayek_model->set_null();
        //$this->Po_kendaraan_trayek_model->set_cetak_query(true);
        $this->Po_kendaraan_trayek_model->set_fields('po_code');
        $this->Po_kendaraan_trayek_model->set_join(array(
            array(
                'TABLE' => 'tbl_po_kendaraan',
                'FIELD' => 'tbl_po_kendaraan.pok_id = tbl_po_kendaraan_trayek.pok_id',
                'JOIN'  => 'inner'
            )
        ));
        $this->Po_kendaraan_trayek_model->set_params(array('t_code'=>$terminal));
        $hasil = array();
        foreach ($this->Po_kendaraan_trayek_model->get_data() as $row) {
            /*'po_tgl_berdiri' &&  
                    $this->field[$a]!=='po_tgl_kps' && 
                    $this->field[$a]!=='po_tgl_akhir' && 
                    $this->field[$a]!=='po_tgl_sk'*/
            $hasil[] = $row['po_code'];
        }
        //$hasil = $this->Po_kendaraan_trayek_model->get_data();
        //print_r ($hasil);
        return $hasil;
    }
	
	public function get_all($fields='*', $params=''){
        $this->set_null();
        //$this->set_fields($fields);
        $this->set_fields($this->get_table() . '.*,bu_name, k_name');
        $this->set_join(array(
            array(
               'TABLE'  => 'tbl_badan_usaha',
               'FIELD'  => 'tbl_badan_usaha.bu_id='. $this->get_table() . '.bu_id',
               'JOIN'   => 'inner'
            ),
            array(
                'TABLE'  => 'tbl_kota',
                'FIELD'  => 'tbl_kota.k_code='. $this->get_table() . '.k_code',
                'JOIN'   => 'inner'
            )
        ));
        //$this->set_params($params);
        return $this->get_data();
    }

    public function get_lastpo(){
        $this->set_null();
        $this->set_fields('max(po_code)+1 as hasil');
        $hasil = $this->get_data();
        if($hasil[0]['hasil']==0){
            return '1';
        }else{
            return $hasil[0]['hasil'];
        }
    }

}

/* End of file po_model.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/models/po_model.php */
