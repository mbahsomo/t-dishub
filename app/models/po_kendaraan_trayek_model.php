<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom 
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: po_kendaraan_trayek_model.php
 */
class Po_kendaraan_trayek_model extends T_Model {

	private $field = array();

    function __construct() {
        parent::T_model();
        $this->set_table('tbl_po_kendaraan_trayek');
        $this->set_key_field( 'pokt_id' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();
        
        for ($i=0; $i < count($this->field) ; $i++) { 
            if ($this->field[$i]!='pokt_id'){
                $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
            }
        }
        $fields['user_entry'] = $this->session->userdata('user_name');
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }
    
    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function getAll() {
        $this->set_null();
        $this->set_cetak_query(false);
        return $this->get_data();
    }

    public function get_rule($insert=true) {
        return array(
            array(
                'field' => 'pok_id',
                'label' => 'Kendaraan',
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 't_code',
                'label' => 'Terminal',
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 'pokt_status',
                'label' => 'Status',
                'rules' => 'xss_clean'
            ),array(
                'field' => 'pokt_rit1',
                'label' => 'Rit 1',
                'rules' => 'xss_clean|max_length[5]'
            ),array(
                'field' => 'pokt_rit2',
                'label' => 'Rit 2',
                'rules' => 'xss_clean|max_length[5]'
            ),array(
                'field' => 'pokt_rit3',
                'label' => 'Rit 3',
                'rules' => 'xss_clean|max_length[5]'
            ),array(
                'field' => 'pokt_rit4',
                'label' => 'Rit 4',
                'rules' => 'xss_clean|max_length[5]'
            ),array(
                'field' => 'pokt_rit5',
                'label' => 'Rit 5',
                'rules' => 'xss_clean|max_length[5]'
            ),array(
                'field' => 'pokt_rit6',
                'label' => 'Rit 6',
                'rules' => 'xss_clean|max_length[5]'
            ),array(
                'field' => 'pokt_rit7',
                'label' => 'Rit 7',
                'rules' => 'xss_clean|max_length[5]'
            ),array(
                'field' => 'pokt_rit8',
                'label' => 'Rit 8',
                'rules' => 'xss_clean|max_length[5]'
            )
        );
    }
    
    public function search($field='k_code', $value='%', $start=0, $stop=5){
    	$this->set_null();
    	$this->set_cetak_query(false);
        $this->set_fields('tbl_po_kendaraan_trayek.*, t_name');
        $this->set_join(array(
           array(
               'TABLE'  => 'tbl_terminal',
               'FIELD'  => 'tbl_terminal.t_code=tbl_po_kendaraan_trayek.t_code',
               'JOIN'   => 'inner'
           )
        ));
        /*$this->set_start($start);
        $this->set_stop($stop);*/
        /*$this->set_like(array(
        	$field=>$value
    	));*/
        $this->set_params(array($field=>$value));
        $this->set_orderby('date_edit desc');
        return $this->get_data();
    }

    public function get_group_trayek($pok){
        $this->set_null();
        $this->set_cetak_query(false);
        $this->set_fields('GROUP_CONCAT(t_name) as t_name');
        $this->set_join(array(
            array(
                'TABLE'  => 'tbl_terminal',
                'FIELD'  => 'tbl_terminal.t_code=tbl_po_kendaraan_trayek.t_code',
                'JOIN'   => 'inner'
            )
        ));
        $this->set_params(array('pok_id'=>$pok));
        $rec = $this->get_data();
        return $rec[0]['t_name'];
    }
		
}

/* End of file po_kendaraan_trayek_model.php */
/* Location: .//home/alif/project/public_html/t-dishub/app/models/po_kendaraan_trayek_model.php */
