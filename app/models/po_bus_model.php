<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: po_bus_model.php
 */
class Po_bus_model extends T_Model {

	private $field = array();

    function __construct() {
        parent::T_model();
        $this->set_table('tbl_po_bus');
        $this->set_key_field('pob_nopol');
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();

        for ($i = 0; $i < count($this->field); $i++) {
            if ($this->field[$i] != 'pob_nopol'){
                $fields[$this->field[$i]] = $this->input->post($this->field[$i], true);
            }

        }
        $fields['user_entry'] = $this->session->userdata('user_name');
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() => $id));
        return $this->update_data();
    }

    public function delete($id) {
        $this->set_null();
        $this->set_params(array($this->get_key_field() => $id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function getAll() {
        $this->set_null();
        $this->set_cetak_query(false);
        return $this->get_data();
    }

    public function get_rule($insert = true) {
        $rl = array(
            array(
                'field' => 'pob_nopol',
                'label' => 'NOPOL',
                'width' => '100px',
                'rules' => 'xss_clean|max_length[20]|required'
            ), array(
                'field' => 'po_code',
                'label' => 'No PO',
                'width' => '100px',
                'fieldLabel' => 'po_name',
                'rules' => 'xss_clean|max_length[5]|min_lenght[5]|required'
            )
        );
        return $rl;
        /*if (!$insert) {
            return array_merge(
                array(
                array(
                    'field' => 'pob_nopol',
                    'label' => 'ID',
                    'rules' => 'required|numeric'
                )
                ), $rl
            );
        } else {
            return $rl;
        }*/
    }

    public function search($field = 'pob_nopol', $value = '%', $start = 0, $stop = 5, $terminal = '') {
        $this->set_null();
        //$this->set_cetak_query(true);
        $this->set_fields('tbl_po_bus.*, po_name, po_alamat, po_nosk, po_tgl_sk');
        $this->set_join(array(
            array(
                'TABLE' => 'tbl_po',
                'FIELD' => 'tbl_po.po_code=tbl_po_bus.po_code',
                'JOIN' => 'inner'
            )
        ));
        $this->set_start($start);
        $this->set_stop($stop);
        $fieldv = explode(";", $field);
        $valuev = explode(";", $value);
        if (count($valuev) > 0) {
            for ($a = 0; $a < count($fieldv); $a++) {
                if ($valuev[$a] !== '') {
                    $this->set_like(array(
                         'tbl_po_bus.'. $fieldv[$a] => $valuev[$a]
                    ));
                }
            }
        }
        //$this->set_cetak_query(true);
        $this->set_orderby('date_edit desc');
        return $this->get_data();
    }

}

/* End of file po_bus_model.php */
/* Location: .//home/mbahsomo/Documents/project/public_html/t-dishub/app/models/po_bus_model.php */