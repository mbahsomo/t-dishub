<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

/**
 * User Create	: mbahsomo
 * Nama File	: po_kendaraan_ubah_model.php
 */
class Po_kendaraan_ubah_model extends T_Model {

	private $field = array();

    function __construct() {
        parent::T_model();
        $this->set_table('tbl_po_kendaraan_ubah');
        $this->set_key_field( 'poku_id' );
        $this->field = $this->get_field_array();
    }

    private function set_init() {
        $fields = array();

        for ($i=0; $i < count($this->field) ; $i++) {
            $fields[$this->field[$i]] = $this->input->post($this->field[$i] , true);
        }
        $fields['user_entry'] = $this->session->userdata('user_name');
        $fields['date_edit'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
    }

    public function insert() {
        $this->set_null();
        $this->set_init();
        $fields['date_entry'] = date('Y-m-d H:i:s');
        $this->set_fields($fields);
        return $this->save_data();
    }

    public function update($id) {
        $this->set_null();
        $this->set_init();
        $this->set_params(array($this->get_key_field() =>$id));
        return $this->update_data();
    }

    public function delete($id){
        $this->set_null();
        $this->set_params(array($this->get_key_field() =>$id));
        $this->set_cetak_query(false);
        return $this->delete_data();
    }

    public function getAll() {
        $this->set_null();
        $this->set_cetak_query(false);
        return $this->get_data();
    }

    public function get_rule($insert=true) {
        return array(
            array(
                'field' => 'pok_id',
                'label' => 'Kendaraan',
                'rules' => 'xss_clean|numeric|required'
            ),array(
                'field' => 'poku_date',
                'label' => 'Tanggal',
                'rules' => 'xss_clean|required'
            ),array(
                'field' => 'poku_nopol_lama',
                'label' => 'NOPOL Lama',
                'rules' => 'xss_clean|max_length[45]|required'
            ),array(
                'field' => 'poku_nopol_baru',
                'label' => 'NOPOL Baru',
                'rules' => 'xss_clean|max_length[45]|required'
            )
        );
    }

    public function search($field='k_code', $value='%', $start=0, $stop=5){
    	$this->set_null();
    	$this->set_cetak_query(false);
        $this->set_fields('tbl_po_kendaraan_ubah.*');
        $this->set_join(array(
            array(
                'TABLE' => 'tbl_po_kendaraan' ,
                'FIELD' => 'tbl_po_kendaraan.pok_id = tbl_po_kendaraan_ubah.pok_id' ,
                'JOIN'  => 'inner'
            )
        ));
        $this->set_start($start);
        $this->set_stop($stop);
        //$fl = explode(";", $field);
        $val = explode(";", $value);
        if ($val[0]!=='' && $val[0]!=='undefined' ){
            $this->set_like(
                array(
                    'tbl_po_kendaraan.ty_code'=>$val[0]
                )
            );
        }
        if(count($val)>1){
            if ($val[1]!=='' && $val[1]!=='undefined'){
                $this->set_like(
                    array(
                        'tbl_po_kendaraan.po_code'=>$val[1]
                    )
                );
            }
        }
//        $this->set_like(array(
//        	$field=>$value
//    	));
        $this->set_orderby('date_edit desc');
//        $this->set_cetak_query(true);
        return $this->get_data();
    }

	public function get_all($fields = '*', $params = ''){
        $this->set_null();
        //$this->set_cetak_query(true);
        $this->set_fields('*');
        return $this->get_data();
    }

}

/* End of file po_kendaraan_ubah_model.php */
/* Location: ./application/models/po_kendaraan_ubah_model.php */
