<script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.grid.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/mask/mask.js"></script>
<script type="text/javascript">
    'use strict';
    var ngDishub = angular.module('ngDishub', ['ngCsv','ngDoeventrow', 'ui.mask']);
    ngDishub.controller('PoCtrl', function($scope, $http, $sce, $document, $filter) {
        $scope.errorshow = false;
        $scope.pesanerror = "";
        $scope.orderByField = 'po_code';
        $scope.reverseSort = false;

        $scope.stop = <?php echo isset($stop) ? $stop : 5; ?>;
        $scope.max_page = <?php echo isset($max_page) ? $max_page : 5; ?>;
        $scope.nextpage = 0;

        $scope.edit = -1;
        //$scope.statuss = [{id:0, value:0},{id:1, value:1}];
        $scope.kotas = <?php echo count($kota) > 0 ? json_encode($kota) : "[]"; ?>;
        $scope.statuss = [{id:0,value:0},{id:1,value:1}];
        $scope.badanUsahas = <?php echo count($badan_usaha) > 0 ? json_encode($badan_usaha) : "[]"; ?>;
        $scope.fieldcaris = [{name: 'Po Name', value: 'po_name'}];
        $scope.fieldcari = $scope.fieldcaris[0];
        $scope.txtcari = '';
        $scope.obj = <?php echo json_encode($this->mdl->get_field_names()); ?>;
        $scope.po = {items: <?php echo count($rec) > 0 ? json_encode($rec) : "[]"; ?>};

        $scope.showErrorMessage = function() {
            return $sce.trustAsHtml($scope.pesanerror);
        };
        //Add data
        $scope.showAdd = function() {
            $scope.edit = -1;
            $scope.obj = <?php echo json_encode($this->mdl->get_field_names()); ?>;
            $http.get('<?php echo site_url() . '/po/get_lastpo/'; ?>').
                    success(function(data) {
                        if (data != undefined) {
                            $scope.obj.po_code = data.kode;
                        }
                    });
            $scope.errorshow = false;
            $('#win-po').modal('show');
        }

        $scope.addItem = function() {
            var evt = "";
            if ($scope.edit == -1) {
                evt = "insert_data";
            } else {
                evt = "edit_data";
            }
            var vparams = [];
            angular.forEach($scope.obj, function(value, key){
                switch (key){
                    case 'bu_id':
                        vparams['bu_id'] = value.bu_id
                        break;
                    case 'k_code':
                        vparams['k_code'] = value.k_code;
                        break;
                    default :
                        if(key != '$$hashKey')
                            vparams[key] = value;
                        break;
                }
            });
            $http.get('<?php echo site_url() . '/po/'; ?>' + evt, {
                params: vparams
            }).
            success(function(data) {
                if (data != undefined) {
                    if (data.success == true) {
                        var dtedit = $scope.obj;
                        dtedit.bu_id = $scope.obj.bu_id.bu_id;
                        dtedit.k_code = $scope.obj.k_code.k_code;
                        dtedit.po_status = $scope.obj.po_status;
                        dtedit.user_entry = '<?php echo $this->session->userdata('user_name'); ?>';
                        dtedit.date_edit = '<?php echo date('d-m-Y H:m:s') ?>';
                        if ($scope.edit === -1 ){
                            dtedit.date_entry = '<?php echo date('d-m-Y H:m:s') ?>';
                            $scope.po.items.push(dtedit);
                        } else {
                            $scope.po.items[$scope.edit] = dtedit;
                        }
                        $scope.errorshow = false;
                        $('#win-po').modal('hide');
                    } else {
                        //$('#msg-error').html(data.msg);
                        $scope.pesanerror = data.msg;
                        $scope.errorshow = true;
                    }
                }
            });

        }
        //Edit data
        $scope.editItem = function(index, item) {
            $scope.edit = index;
            $scope.obj = item;
            
            var buid = $filter('filter')($scope.badanUsahas , {bu_id: item.bu_id}, true);
            $scope.obj.bu_id = buid[0];
            var buid = $filter('filter')($scope.kotas , {k_code: item.k_code}, true);
            $scope.obj.k_code = buid[0];
            angular.element('select').trigger('liszt:updated');
            angular.element('select').trigger("chosen:updated");
            $('#win-po').modal('show');
        }

        //Hapus data
        $scope.removeItem = function(index, item) {

            if (confirm('Anda yakin HAPUS data ini? ')) {
                $http.get('<?php echo site_url() . '/po/delete_data/'; ?>', {
                    params: {
                        'po_code': item.po_code
                    }
                }).
                success(function(data) {
                    if (data != undefined) {
                        $scope.po.items.splice(index, 1);
                        //$scope.max_page = data.max_page;
                    }
                });
            }
        }

        $scope.search = function() {
            $http.get('<?php echo site_url() . '/po/search/'; ?>', {
                params: {
                    'field': $scope.fieldcari.value,
                    'value': $scope.txtcari,
                    'stop': $scope.nextpage
                }
            }).
                    success(function(data) {
                        if (data != undefined) {
                            $scope.po = {items: data.rec};
                            $scope.max_page = data.max_page;
                            if ($scope.max_page < $scope.nextpage) {
                                $scope.nextpage = 0;
                            }
                        }
                    })
        }

        $scope.onKeydown = function(ev) {
            $scope.pressed = ev.which;
            if (ev.which == 13 || ev.which == 9)
                $scope.search();
        }

        $scope.onFirstPage = function() {
            if ($scope.nextpage > 0) {
                $scope.nextpage = 0;
                $scope.search();
            }
        }

        $scope.onNextPage = function() {
            if (($scope.nextpage + $scope.stop) <= $scope.max_page) {
                $scope.nextpage = $scope.nextpage + $scope.stop;
                //console.log($scope.nextpage);
                $scope.search();
            }
        }

        $scope.onPrevPage = function() {
            if ($scope.nextpage > 0) {
                $scope.nextpage = $scope.nextpage - $scope.stop;
                $scope.search();
            }
        }

        $scope.onLastPage = function() {
            if ($scope.nextpage < $scope.max_page) {
                $scope.nextpage = $scope.max_page;
                $scope.search();
            }
        }

        $scope.selectRow = function(row) {
            $scope.selectedRow = row;
        };

        $scope.cetak = function (){
            print_preview('<?php echo site_url(). '/'. $this->fn .'/cetak/'; ?>','');
        }

        $document.bind('keydown', function(event) {
            console.debug(event.which);
            switch (event.which)
            {
                case 65:
                    if(event.altKey){
                        $scope.showAdd();
                        return false;
                    }
                    break;
                case 80 :
                    if(event.altKey){
                        $scope.cetak();
                        return false;
                    }
                    break;               
            }
        });

    }).directive('deFocus', deEvent);
</script>