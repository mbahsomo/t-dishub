<?php
$data['rec'] = $rec;
$data['stop'] = $stop;
$data['max_page'] = $max_page;
$this->load->view('po/controller', $data);
?>
<div ng-controller="PoCtrl">
    <!--Windows Modal-->
    <div class="modal fade" id="win-po" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog"  style="width:90%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add / Edit Data</h4>
                </div>
                <div class="modal-body">

                    <div ng-show="errorshow" class="alert alert-danger" ng-bind-html='showErrorMessage()'></div>

                    <div class="form-horizontal" role="form">
                        <!-- Kolom -->
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="po_code" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_code'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.po_code" type="text" class="form-control input-sm" id="po_code" placeholder="<?php echo $this->mdl->get_label('po_code'); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="po_name" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_name'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.po_name" type="text" class="form-control input-sm" id="po_name" placeholder="<?php echo $this->mdl->get_label('po_name'); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="po_alamat" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_alamat'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.po_alamat" type="text" class="form-control input-sm" id="po_alamat" placeholder="<?php echo $this->mdl->get_label('po_alamat'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="po_status" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_status'); ?></label>
                                    <div class="col-sm-4">
                                        <select de-focus ng-model="obj.po_status" class="form-control input-sm">
                                            <option ng-repeat="status in statuss" value="{{status.id}}">{{status.value}}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="k_code" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('k_code'); ?></label>
                                    <div class="col-sm-9">
                                        <select style="width:200px;" de-focus ng-model="obj.k_code" class="form-control
                                        input-sm" ng-options="kota.k_name for kota in kotas" id="k_code">
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="po_telp" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_telp'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.po_telp" type="text" class="form-control input-sm" id="po_telp" placeholder="<?php echo $this->mdl->get_label('po_telp'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="po_tgl_berdiri" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_tgl_berdiri'); ?></label>
                                    <div class="col-sm-4">
                                        <input ui-mask="99-99-9999" de-focus ng-model="obj.po_tgl_berdiri" type="text" class="form-control input-sm" id="po_tgl_berdiri">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="po_pemilik" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_pemilik'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.po_pemilik" type="text" class="form-control input-sm" id="po_pemilik" placeholder="<?php echo $this->mdl->get_label('po_pemilik'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="po_nowni" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_nowni'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.po_nowni" type="text" class="form-control input-sm" id="po_nowni" placeholder="<?php echo $this->mdl->get_label('po_nowni'); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="po_kota_wni" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_kota_wni'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.po_kota_wni" type="text" class="form-control input-sm" id="po_kota_wni" placeholder="<?php echo $this->mdl->get_label('po_kota_wni'); ?>">
                                    </div>
                                </div>							

                                <div class="form-group">
                                    <label for="po_nogama" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_nogama'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.po_nogama" type="text" class="form-control input-sm" id="po_nogama" placeholder="<?php echo $this->mdl->get_label('po_nogama'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="po_kgnama" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_kgnama'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.po_kgnama" type="text" class="form-control input-sm" id="po_kgnama" placeholder="<?php echo $this->mdl->get_label('po_kgnama'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="bu_id" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('bu_id'); ?></label>
                                    <div class="col-sm-9">
                                        <select style="width:200px;" de-focus ng-model="obj.bu_id" class="form-control
                                                input-sm" ng-options="badanUsaha.bu_name for badanUsaha in badanUsahas" >
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="po_no_notaris" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_no_notaris'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.po_no_notaris" type="text" class="form-control input-sm" id="po_no_notaris" placeholder="<?php echo $this->mdl->get_label('po_no_notaris'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="po_nama_notaris" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_nama_notaris'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.po_nama_notaris" type="text" class="form-control input-sm" id="po_nama_notaris" placeholder="<?php echo $this->mdl->get_label('po_nama_notaris'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="po_tgl_kps" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_tgl_kps'); ?></label>
                                    <div class="col-sm-4">
                                        <input ui-mask="99-99-9999" de-focus ng-model="obj.po_tgl_kps" type="text" class="form-control input-sm" id="po_tgl_kps">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="po_tgl_akhir" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_tgl_akhir'); ?></label>
                                    <div class="col-sm-4">
                                        <input  ui-mask="99-99-9999" de-focus ng-model="obj.po_tgl_akhir" type="text" class="form-control input-sm" id="po_tgl_akhir">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="po_nosk" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_nosk'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.po_nosk" type="text" class="form-control input-sm" id="po_nosk" placeholder="<?php echo $this->mdl->get_label('po_nosk'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="po_tgl_sk" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_tgl_sk'); ?></label>
                                    <div class="col-sm-4">
                                        <input  ui-mask="99-99-9999" de-focus ng-model="obj.po_tgl_sk" type="text" class="form-control input-sm" id="po_tgl_sk">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End Kolom-->
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" ng-click="addItem()" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

   <?php $this->load->view('template/toolbar_grid'); ?>

    <form class="navbar-form" style="padding-left:0;">
        <div class="form-group">
            <select style="width:150px;" class="form-control input-sm" ng-model="fieldcari" ng-options="c.name for c in fieldcaris"></select>
        </div>
        <div class="form-group">
            <input type="text" placeholder="Data yang dicari" class="form-control input-sm" ng-model="txtcari" ng-keydown="onKeydown($event)"/>
        </div>
        <button type="button" class="btn btn-success btn-sm" ng-click="search()"><span class="glyphicon glyphicon-search"></span> Cari</button>
    </form>

    <!-- id="tbl-scroll" -->
    <table id="tbl-scroll" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <td colspan="20" style="text-align:right;">
                    <ul class="pagination pagination-sm" style="margin:0 0 0 0;">
                        <li><a   ng-click="onFirstPage()">&#8249;&#8249;</a></li>
                        <li><a   ng-click="onPrevPage()">&#8249;</a></li>
                        <li><span>Page {{nextpage / stop + 1}} of {{max_page / stop + 1}}</span></li>
                        <li><a   ng-click="onNextPage()">&#8250;</a></li>
                        <li><a   ng-click="onLastPage()">&#8250;&#8250;</a></li>
                    </ul>
                </td>
            </tr>
            <tr>
                <?php
                foreach ($this->mdl->get_rule() as $val) {
                    if (isset($val['grid'])) {
                        if ($val['grid']){ ?>
                            <th>
                                <a   ng-click="orderByField='<?php echo $val['field']; ?>'; reverseSort = !reverseSort">
                                    <?php echo $this->mdl->get_label($val['field']); ?>
                                    <span ng-show="orderByField == '<?php echo $val['field']; ?>'">
                                        <span ng-show="!reverseSort" class="glyphicon glyphicon-chevron-up"></span>
                                        <span ng-show="reverseSort" class="glyphicon glyphicon-chevron-down"></span>
                                    </span>
                                </a>
                            </th>                            
                        <?php }
                    }else{?>
                        <th>
                            <a   ng-click="orderByField='<?php echo $val['field']; ?>'; reverseSort = !reverseSort">
                                <?php echo $this->mdl->get_label($val['field']); ?>
                                <span ng-show="orderByField == '<?php echo $val['field']; ?>'">
                                    <span ng-show="!reverseSort" class="glyphicon glyphicon-chevron-up"></span>
                                    <span ng-show="reverseSort" class="glyphicon glyphicon-chevron-down"></span>
                                </span>
                            </a>
                        </th>                        
                    <?php }
                }
                ?>
                <td width="100px">User Entry</td>
                <td width="155px">Tgl Entry</td>
                <td width="155px">Tgl Edit</td>
                <th width="70px;">Event</th>
            </tr>
        </thead>
        <tbody>
            <tr ng-doeventrow="item" tabindex="{{$index}}" ng-repeat="item in po.items | orderBy:orderByField:reverseSort" ng-class='{"success":$index == selectedRow}' ng-click='selectRow($index)'>
                <?php
                foreach ($this->mdl->get_rule() as $val) {
                    if (isset($val['grid'])) {
                        if ($val['grid']){
                            echo "<td>{{item." . (isset($val['fieldLabel']) ? $val['fieldLabel']: $val['field']) .
                                "}}</td>";
                        }
                    }else {
                        echo "<td>{{item." . (isset($val['fieldLabel'])? $val['fieldLabel']: $val['field']) .
                            "}}</td>";
                    }
                }
                ?>
                <td>{{item.user_entry}}</td>
                <td>{{item.date_entry | date: 'dd-MM-yyyy H:m:s'}}</td>
                <td>{{item.date_edit | date:'dd-MM-yyyy H:m:s'}}</td>
                <td>
                    <a   class="btn btn-success btn-xs" ng-click="editItem($index, item)" ><span class="glyphicon glyphicon-edit"></span></a>
                    <a   class="btn btn-danger btn-xs" ng-click="removeItem($index, item)" ><span class="glyphicon glyphicon-floppy-remove"></span></a>
                </td>
            </tr>			
        </tbody>
        <tfoot>
            <tr>
                <td colspan="20" style="text-align:right;">
                    <ul class="pagination pagination-sm" style="margin:0 0 0 0;">
                        <li><a   ng-click="onFirstPage()">&#8249;&#8249;</a></li>
                        <li><a   ng-click="onPrevPage()">&#8249;</a></li>
                        <li><span>Page {{nextpage / stop + 1}} of {{max_page / stop + 1}}</span></li>
                        <li><a   ng-click="onNextPage()">&#8250;</a></li>
                        <li><a   ng-click="onLastPage()">&#8250;&#8250;</a></li>
                    </ul>
                </td>
            </tr>
        </tfoot>
    </table>

</div>
<script src="<?php echo base_url(); ?>assets/js/md5.js"></script>
<link href="<?php echo base_url(); ?>assets/lib/date-picker/date.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/lib/date-picker/date.js"></script>
<link href="<?php echo base_url(); ?>assets/css/t-table.css" rel="stylesheet">

