<?php  
	$data['rec'] = $rec;
	$data['stop'] = $stop;
	$data['max_page'] = $max_page;
	$this->load->view('user/controller', $data);
?>
<div ng-controller="UserCtrl">
    <!--Windows Modal-->
    <div class="modal fade" id="win-user-detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Detail Menu</h4>
                </div>
                <div class="modal-body">
                	<table id="tbl-scroll" class="table table-bordered table-striped table-hover">
                		<thead>
                			<tr>
                				<th>Menu</th>
                				<th>Status</th>
                				<th>Insert</th>
                				<th>Update</th>
                				<th>Delete</th>
                				<th>Cetak</th>
                			</tr>
                		</thead>
                		<tbody>
                			<tr ng-repeat="item in userDetailMenu" ng-class='{"success":$index==selectedRow}' ng-click='selectRow($index)' ng-doeventrow="item" set-obj-rows="setObjRows(item)" tabindex="{{$index}}">
                				<td>{{item.mn_name}}</td>
                				<td style="text-align:center;">
                					<input ng-change="userDetailSave(item)" ng-model="item.um_status" type="checkbox" ng-true-value="T" ng-false-value="F" />
                				</td>
                				<td style="text-align:center;">
                					<input ng-change="userDetailSave(item)" ng-model="item.um_insert" type="checkbox" ng-true-value="T" ng-false-value="F" />
                				</td>
                				<td style="text-align:center;">
                					<input ng-change="userDetailSave(item)" ng-model="item.um_update" type="checkbox" ng-true-value="T" ng-false-value="F" />
                				</td>
                				<td style="text-align:center;">
                					<input ng-change="userDetailSave(item)" ng-model="item.um_delete" type="checkbox" ng-true-value="T" ng-false-value="F" />
                				</td>
                				<td style="text-align:center;">
                					<input ng-change="userDetailSave(item)" ng-model="item.um_cetak" type="checkbox" ng-true-value="T" ng-false-value="F" />
                				</td>
                			</tr>
                		</tbody>
                	</table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" ng-click="addItem()" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

	<!--Windows Modal-->
	<div class="modal fade" id="win-user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add / Edit Data</h4>
				</div>
				<div class="modal-body">

					<div ng-show="errorshow" class="alert alert-danger" ng-bind-html='showErrorMessage()'></div>

					<div class="form-horizontal" role="form">
						<div class="form-group">
						   	<label for="u_name" class="col-sm-3 control-label">User Name</label>
						    <div class="col-sm-9 input-group input-group-sm">
								<span class="input-group-addon">@</span>
						      	<input de-focus ng-model="obj.u_name" type="text" class="form-control input-sm" id="u_name" placeholder="User Name">
						    </div>
					  	</div>

					  	<div class="form-group">
						   	<label for="u_email" class="col-sm-3 control-label">Email</label>
						    <div class="col-sm-9">
								<input de-focus ng-model="obj.u_email" type="text" class="form-control input-sm" id="u_email" placeholder="Email">								
						    </div>
					  	</div>

					  	<div class="form-group">
						   	<label for="u_password" class="col-sm-3 control-label">Password</label>
						    <div class="col-sm-9">
						      	<input de-focus ng-model="obj.u_password" type="password" class="form-control input-sm" id="u_password" placeholder="Masukkan password">
						    </div>
					  	</div>

					  	<div class="form-group">
						   	<label for="u_level" class="col-sm-3 control-label">Level</label>
						    <div class="col-sm-9">
								<!--name="u_level" ng-options="c.name for c in levels"-->
						    	<select de-focus ng-model="obj.u_level"  class="form-control">
									<option ng-repeat="level in levels" value="{{level.name}}">{{level.name}}</option>
								</select>
						    </div>
					  	</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary btn-sm" ng-click="addItem()" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<?php $this->load->view('template/toolbar_grid'); ?>
	


    <form class="navbar-form" style="padding-left:0;">
		<div class="form-group">
			<select class="form-control input-sm" ng-model="fieldcari" ng-options="c.name for c in fieldcaris"></select>
		</div>
			<div class="form-group">
				<input type="text" placeholder="Data yang dicari" class="form-control input-sm" ng-model="txtcari" ng-keydown="onKeydown($event)"/>
			</div>
		<button type="button" class="btn btn-success btn-sm" ng-click="search()"><span class="glyphicon glyphicon-search"></span> Cari</button>
	</form>
        
	<!-- id="tbl-scroll" -->
	<table id="tbl-scroll" class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th colspan="5" style="text-align:right;">
					<ul class="pagination pagination-sm" style="margin:0 0 0 0;">
						<li><a   ng-click="onFirstPage()">&#8249;&#8249;</a></li>
						<li><a   ng-click="onPrevPage()">&#8249;</a></li>
						<li><span>Page {{nextpage/stop+1}} of {{max_page/stop+1}}</span></li>
						<li><a   ng-click="onNextPage()">&#8250;</a></li>
						<li><a   ng-click="onLastPage()">&#8250;&#8250;</a></li>
					</ul>
				</th>
			</tr>
			<tr>
				<td>No</td>
				<td>User Name</td>
				<td>Password</td>
				<td>Email</td>
				<td width="200px">Event</td>
			</tr>
		</thead>
		<tbody>
			<tr  ng-repeat="item in user.items" ng-class='{"success":$index==selectedRow}' ng-click='selectRow($index)' ng-doeventrow="item" set-obj-rows="setObjRows(item)" tabindex="{{$index}}">
				<td>{{$index+1}}</td>
				<td>{{item.u_name}}</td>
				<td>{{item.u_password}}</td>
				<td>{{item.u_email}}</td>
				<td>
					<a class="btn btn-success btn-xs" ng-click="editItem($index,item)" ><span class="glyphicon glyphicon-edit"></span>  Edit</a>
                    <a class="btn btn-primary btn-xs" ng-click="detailItem(item)" ><span class="glyphicon
                    glyphicon-plus"></span>Detail</a>
                    <a class="btn btn-danger btn-xs" ng-click="removeItem($index,
                    item)" ><span class="glyphicon glyphicon-floppy-remove"></span> Delete</a>
				</td>
			</tr>			
		</tbody>
		<tfoot>
			<tr>
				<td colspan="5" style="text-align:right;">
					<ul class="pagination pagination-sm" style="margin:0 0 0 0;">
						<li><a   ng-click="onFirstPage()">&#8249;&#8249;</a></li>
						<li><a   ng-click="onPrevPage()">&#8249;</a></li>
						<li><span>Page {{nextpage/stop+1}} of {{max_page/stop+1}}</span></li>
						<li><a   ng-click="onNextPage()">&#8250;</a></li>
						<li><a   ng-click="onLastPage()">&#8250;&#8250;</a></li>
					</ul>
				</td>
			</tr>
		</tfoot>
	</table>
</div>
<script src="<?php echo base_url(); ?>assets/js/md5.js"></script>
