<script src="<?php echo base_url(); ?>assets/lib/doevent/do-event-input.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.grid.js"></script>
<script type="text/javascript">
	'use strict';
	var ngDishub = angular.module('ngDishub', ['ngCsv','ngDoeventrow']);
	ngDishub.controller('UserCtrl', function($scope, $http, $sce, $document) {
		$scope.errorshow=false;
		$scope.pesanerror="";
		
		$scope.stop = <?php echo isset($stop)?$stop:5; ?>;
		$scope.max_page = <?php echo isset($max_page)?$max_page:5; ?>;
		$scope.nextpage = 0;

		$scope.edit = -1 ;
		$scope.levels = [
				{name:1},{name:2},{name:3},{name:4},{name:5},{name:6},{name:7},{name:8},{name:9}
			];
		$scope.fieldcaris = [{name:'User Name',value:'u_name'},{name:'Email',value:'u_email'}];
		$scope.fieldcari = $scope.fieldcaris[0];
		$scope.txtcari = '';
		$scope.obj = {u_name:'',u_password:'',u_email:'',u_level:$scope.levels[0]};
		$scope.user = { items : <?php echo count($rec)>0? json_encode($rec):"[]";?>};
		
		$scope.showErrorMessage = function() {
			return $sce.trustAsHtml($scope.pesanerror);
		};
		//Add data
		$scope.showAdd = function (){
        	$scope.edit = -1 ;
        	$scope.obj = {u_name:'',u_password:'',u_email:'',u_level:''};
        	$scope.errorshow=false;
        	$('#win-user').modal('show');
        }

        $scope.addItem = function() {
        	$scope.obj.u_password = $.md5($scope.obj.u_password);
        	var evt = "";
        	if ($scope.edit == -1 ){
		        evt = "insert_data";
		    }else{
		    	evt = "edit_data" ;
		    }
			$http.get('<?php echo site_url() .'/user/'; ?>' + evt, {
				params: {
					u_email : $scope.obj.u_email,
                    u_name : $scope.obj.u_name,
					u_password : $scope.obj.u_password,
					u_level : $scope.obj.u_level
				}
			}).
			success(function(data) {
				if(data!=undefined){
					if(data.success==true){
						if ($scope.edit == -1 ){
							$scope.user.items.push($scope.obj);
						}else{
							$scope.user.items[$scope.edit] = $scope.obj;
						}
               			$scope.errorshow=false;
						$('#win-user').modal('hide');
                	}else{
                		//$('#msg-error').html(data.msg);
						$scope.pesanerror=data.msg;
                		$scope.errorshow=true;                		
                	}
				}
			});	    		
	    	
	    }
	    //Edit data
	    $scope.editItem = function(index, item){
	    	$scope.edit = index ;
	    	$scope.obj = item;
			//$scope.obj.level = item.u_level;
	    	$('#win-user').modal('show');
	    }

		//Hapus data
		$scope.removeItem = function(index, item) {

	    	if(confirm('Anda yakin HAPUS data ini? ')){
				$http.get('<?php echo site_url() .'/user/delete_data/'; ?>', {
					params: {
						'u_name' : item.u_name
					}
				}).
				success(function(data) {
					if(data!=undefined){
						$scope.user.items.splice(index, 1);
						//$scope.max_page = data.max_page;
					}
				});	    		
	    	}
	    }
		
		$scope.search = function (){
			$http.get('<?php echo site_url() .'/user/search/'; ?>', {
				params: {
					'field' : $scope.fieldcari.value,
					'value' : $scope.txtcari,
					'stop'  : $scope.nextpage
				}
			}).
			success(function(data) {
				if(data!=undefined){
					$scope.user = { items : data.rec};
					$scope.max_page = data.max_page;
					if($scope.max_page<$scope.nextpage){
						$scope.nextpage=0;
					}
				}
			});
		}

		$scope.onKeydown= function(ev){
			$scope.pressed = ev.which;
			if (ev.which==13 || ev.which==9)
	            $scope.search();
		}

		$scope.onFirstPage = function(){
			if ($scope.nextpage  > 0 ){
				$scope.nextpage = 0;
				$scope.search();
			}
		}

		$scope.onNextPage = function(){
			if (($scope.nextpage + $scope.stop) <= $scope.max_page ){
				$scope.nextpage = $scope.nextpage+$scope.stop;
				//console.log($scope.nextpage);
				$scope.search();
			}
		}

		$scope.onPrevPage = function(){
			if ($scope.nextpage  >0 ){
				$scope.nextpage = $scope.nextpage-$scope.stop;
				$scope.search();
			}
		}		

		$scope.onLastPage = function(){
			if ($scope.nextpage  < $scope.max_page ){
				$scope.nextpage = $scope.max_page;
				$scope.search();
			}
		}

		$scope.selectRow = function(row) {
          $scope.selectedRow = row;
        };

        $scope.cetak = function (){
			print_preview('<?php echo site_url(). '/'. $this->fn .'/cetak/'; ?>','');
		}

		$document.bind('keydown', function(event) {
			switch (event.which)
            {
                case 65:
                    if(event.altKey){
                        $scope.showAdd();
                        return false;
                    }
                    break;
                case 80 :
                	if(event.altKey){
                        $scope.cetak();
                        return false;
                    }
                	break;
            }
    	});

        $scope.userDetailMenu = {};
        $scope.detailItem = function (item){
            $http.get('<?php echo site_url() .'/user_menu/search/'; ?>', {
                params: {
                    'field' : 'u_name',
                    'value' : item.u_name,
                    'stop'  : 0
                }
            }).
                success(function(data) {
                    if(data!=undefined){
                    	$scope.userDetailMenu = data.rec;
                        /*$scope.user = { items : data.rec};
                        $scope.max_page = data.max_page;
                        if($scope.max_page<$scope.nextpage){
                            $scope.nextpage=0;
                        }*/
                    }
                });
            $('#win-user-detail').modal('show');
        };

        $scope.userDetailSave = function (item){
        	$http.get('<?php echo site_url() .'/user_menu/edit_data/'; ?>', {
                params: {
                    'um_id' : item.um_id,
                    'um_status' : item.um_status,
                    'um_insert' : item.um_insert,
                    'um_update' : item.um_update,
                    'um_delete' : item.um_delete,
                    'um_cetak' : item.um_cetak
                }
            })
        }
		
	}).directive('deFocus', deEvent);
</script>