<?php

/* 
 * ** Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
    </head>
    <body>
        <table>
        	<tr>
        		<td width="100px"></td>
        		<td>Lampiran 1 &nbsp;&nbsp;&nbsp;</td>
        		<td colspan="3">KEPUTUSAN KEPALA DINAS PERHUBUNGAN DAN LLAJ</td>
        	</tr>
        	<tr>
        		<td></td>
        		<td></td>
        		<td colspan="3">Propinsi Jawa Timur</td>
        	</tr>
        	<tr>
        		<td></td>
        		<td></td>
        		<td>Nomor</td>
        		<td>:</td>
        		<td>551.21/0010/104.113/2011</td>
        	</tr>
        	<tr>
        		<td></td>
        		<td></td>
        		<td>Tanggal</td>
        		<td>:</td>
        		<td>10 FEB 2011</td>
        	</tr>
        </table>
        <hr />
        DAFTAR KENDARAAN <BR />
        <?php 
        if (count($rec)>0){
        	echo $rec[0]['po_name']; 
    	?>
    	<table border='1'>
    		<thead>
    			<tr>
    				<th>No Urut</th>
    				<th>Nomor Kendaraan</th>
    				<th>Nomor Stuk</th>
    				<th>Merk Kendaraan</th>
    				<th>Tahun Pembuatan</th>
    				<th>Jumlah Tempat Duduk</th>
    				<th>Kode Trayek</th>
    				<th>Keterangan</th>
    			</tr>
    		</thead>
    		<tbody>
    			<?php $bil=1 ; foreach ($rec as $rows) { ?>
    			<tr>
    				<td><?php echo $bil++; ?></td>
    				<td><?php echo $rows['pok_nopol']; ?></td>
    				<td><?php echo $rows['pok_nostuk']; ?></td>
    				<td><?php echo $rows['pok_merk']; ?></td>
    				<td><?php echo $rows['pok_tahun_buat']; ?></td>
    				<td><?php echo $rows['pok_duduk']; ?></td>
    				<td><?php echo $rows['ty_code']; ?></td>
    				<td><?php echo $rows['ja_name']; ?></td>
    			</tr>
    			<?php } ?>
    		</tbody>
    	</table>
    	<?php 
    	}else{
    		echo "Data Kosong";
    	}
    	?>
    </body>
</html>