<?php

/* 
 * ** Desain by t-team
 * name    : Sugik Puja Kusuma, S.Kom
 * email   : mbahsomo@do-event.com
 * website : http://www.do-event.com.com
 * ===============================================================
 *     Semua source dalam program ini dibawah license [GPL]
 *     Silakan Untuk mengembangkan dan memperbanyak source ini
 *     Dengan tidak menghilangkan nama pembuat
 * ===============================================================
 */

?>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <style> 

	        @page { size: A4 portrait }
	        
		</style>
    </head>
    <body>
    	<table>
        	<tr>
        		<td width="100px"></td>
        		<td>Lampiran 1 &nbsp;&nbsp;&nbsp;</td>
        		<td colspan="3">KEPUTUSAN KEPALA DINAS PERHUBUNGAN DAN LLAJ</td>
        	</tr>
        	<tr>
        		<td></td>
        		<td></td>
        		<td colspan="3">Propinsi Jawa Timur</td>
        	</tr>
        	<tr>
        		<td></td>
        		<td></td>
        		<td>Nomor</td>
        		<td>:</td>
        		<td>551.21/0103/104.111/2010</td>
        	</tr>
        	<tr>
        		<td></td>
        		<td></td>
        		<td>Tanggal</td>
        		<td>:</td>
        		<td>23 JUN 2010</td>
        	</tr>
        </table>
        <hr />
        <?php 
        if (count($rec)>0){
        	echo $rec[0]['po_name']; 
    	?>
    	<div align="center">
        	DAFTAR WAKTU PERJALANAN<BR />
        	PERUSAHAAN OTOBIS <?php echo $rec[0]['po_name']; ?><BR />
        	DI <?php echo $rec[0]['po_alamat']; ?><BR /> 
        </div>
    	=========================================================================<br>
    	<?php foreach ($rec as $rows) { ?>
    	&nbsp;&nbsp;&nbsp;<?php echo $rows['asal']; ?>&nbsp;&nbsp;&nbsp;<?php echo $rows['pok_status']; ?>;&nbsp;&nbsp;<?php echo $rows['pokt_rit1']; ?>;&nbsp;&nbsp;<?php echo $rows['pokt_rit2'];  ?>
    	<?php } ?>
    	=========================================================================
    	Keterangan : T = Terminal ; B = Berangkat ; D = Datang ; <br >
    	<table style="margin : 300px;" border="0" width="100%">
    		<tr style="text-align:center;">
    			<td>
    			Surabaya , <?php echo date('d MMMM yyyy'); ?><br>
    			KEPALA DINAS PERHUBUNGAN DAN LLAJ<br>
    			PROPINSI JAWA TIMUR<br>
    			<br>
    			<br>
    			<br>
				<br>
    			<br>
    			Ir. WAHID WAHYUDI, MT<br>
    			---------------------------------------<br>
    			Pembina Utama Muda<br>
    			NIP. 196330127 198903 1 005
    			</td>
    		</tr>
    		
    	</table>
    	<?php     	
    	}else{
    		echo "Data Kosong <br>";
    	}
    	?>
    	
    </body>
</html>