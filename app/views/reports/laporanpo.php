<!DOCTYPE html>
<html>
<head>	
	<title>Laporan PO</title>
	<link href="<?php echo base_url(); ?>assets/css/report.css" rel="stylesheet">
</head>
<body>
	<!--detail-->
	<table class="detail">
		<caption>Laporan Po <?php echo $title; ?></caption>
		<thead>
			<tr>
				<th>No</th>
				<th>ID</th>
                <th>Nama PO</th>                        
                <th>ALamat</th>                        
                <th>Kota</th>                        
                <th>No Telp</th>
                <th>Pemilik</th>                        
                <th>BU</th> 
			</tr>
		</thead>
		<tbody>
			<?php $bil = 1; foreach ($rec as $key => $value) {?>
			<tr>
				<td><?php echo $bil++; ?></td>
				<td><?php echo $value['po_code']; ?></td>
				<td><?php echo $value['po_name']; ?></td>
				<td><?php echo $value['po_alamat']; ?></td>
				<td><?php echo $value['k_name']; ?></td>
				<td><?php echo $value['po_telp']; ?></td>
				<td><?php echo $value['po_pemilik']; ?></td>
				<td><?php echo $value['bu_name']; ?></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<!--detail-->
</body>
</html>