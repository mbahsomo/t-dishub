<?php
$data['rec'] = $rec;
$data['stop'] = $stop;
$data['max_page'] = $max_page;
$this->load->view('kendaraan/controller', $data);
?>
<div ng-controller="KendaraanCtrl">
    <!--Windows Modal-->
    <div class="modal fade" id="win-kendaraan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add / Edit Data</h4>
                </div>
                <div class="modal-body">

                    <div ng-show="errorshow" class="alert alert-danger" ng-bind-html='showErrorMessage()'></div>

                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="po_code" class="col-sm-3 control-label">Nama PO</label>
                            <div class="col-sm-9 input-group input-group-sm">
                                <span class="input-group-addon">@</span>
                                <select ng-model="obj.po_code" class="form-control input-sm" >
                                    <option ng-repeat="item in pos" value="{{item.po_code}}">{{item.po_name}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pob_nopol" class="col-sm-3 control-label">NOPOL</label>
                            <div class="col-sm-9 input-group input-group-sm">
                                <span class="input-group-addon">@</span>
                                <input de-focus ng-model="obj.pob_nopol" type="text" class="form-control input-sm" id="pob_nopol" placeholder="NOPOL">
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" ng-click="addItem()" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <?php $this->load->view('template/toolbar_grid'); ?>

<!--    <form class="navbar-form" style="padding-left:0;">
        <div class="form-group">
            <select class="form-control input-sm" ng-model="fieldcari" ng-options="c.name for c in fieldcaris"></select>
        </div>
        <div class="form-group">
            <input type="text" placeholder="Data yang dicari" class="form-control input-sm" ng-model="txtcari" ng-keydown="onKeydown($event)"/>
        </div>
        <button type="button" class="btn btn-success btn-sm" ng-click="search()"><span class="glyphicon glyphicon-search"></span> Cari</button>
    </form>-->

    <!-- id="tbl-scroll" -->
    <table id="tbl-scroll" class="table table-bordered table-striped table-hover detail">
        <thead>
            <tr>
                <th width="15px;">No</th>
                <?php
                foreach ($this->mdl->get_rule() as $val) {
                    if (isset($val['grid'])) {
                        if ($val['grid']){ ?>
                            <th <?php echo isset($val['width'])?'style="width:' .$val['width'] .'"':''; ?>>
                                <a   ng-click="orderByField='<?php echo $val['field']; ?>'; reverseSort = !reverseSort">
                                    <?php echo $this->mdl->get_label($val['field']); ?>
                                    <span ng-show="orderByField == '<?php echo $val['field']; ?>'">
                                        <span ng-show="!reverseSort" class="glyphicon glyphicon-chevron-up"></span>
                                        <span ng-show="reverseSort" class="glyphicon glyphicon-chevron-down"></span>
                                    </span>
                                </a>
                            </th>                            
                        <?php }
                    }else{?>
                        <th <?php echo isset($val['width'])?'style="width:' .$val['width'] .'"':''; ?>>
                            <a   ng-click="orderByField='<?php echo $val['field']; ?>'; reverseSort = !reverseSort">
                                <?php echo $this->mdl->get_label($val['field']); ?>
                                <span ng-show="orderByField == '<?php echo $val['field']; ?>'">
                                    <span ng-show="!reverseSort" class="glyphicon glyphicon-chevron-up"></span>
                                    <span ng-show="reverseSort" class="glyphicon glyphicon-chevron-down"></span>
                                </span>
                            </a>
                        </th>                        
                    <?php }
                }
                ?>
                <td width="100px">User Entry</td>
                <td width="155px">Tgl Entry</td>
                <td width="155px">Tgl Edit</td>
                <th width="150px" >Event</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td>
                    <input ng-change="search(find)" class="form-control input-sm" ng-model="find.pob_nopol" />
                </td>
                <td>
                    <select ng-change="search(find)" ng-model="find.po_code" class="form-control input-sm" >
                        <option value="">All</option>
                        <option ng-repeat="item in pos" value="{{item.po_code}}">{{item.po_name}}</option>
                    </select>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr ng-doeventrow="item" set-obj-rows="setObjRows(item)" tabindex="{{$index}}" ng-repeat="item in kendaraan.items | orderBy:orderByField:reverseSort" ng-class='{"success":$index == selectedRow}' ng-click='selectRow($index)'>
                <td >{{$index + 1}}</td>
                <td >{{item.pob_nopol}}</td>
                <td >{{item.po_name}}</td>
                <td >{{item.user_entry}}</td>
                <td>{{item.date_entry | date: 'dd-MM-yyyy H:m:s'}}</td>
                <td>{{item.date_edit | date:'dd-MM-yyyy H:m:s'}}</td>
                <td width="150px">
                    <a   class="btn btn-success btn-xs" ng-click="editItem($index, item)" ><span class="glyphicon glyphicon-edit"></span>  Edit</a>
                    <a   class="btn btn-danger btn-xs" ng-click="removeItem($index, item)" ><span class="glyphicon glyphicon-floppy-remove"></span> Delete</a>
                </td>
            </tr>			
        </tbody>
        <tfoot>
            <tr>
                <td colspan="10" style="text-align:right;">
                    <ul class="pagination pagination-sm" style="margin:0 0 0 0;">
                        <li><a   ng-click="onFirstPage()">&#8249;&#8249;</a></li>
                        <li><a   ng-click="onPrevPage()">&#8249;</a></li>
                        <li><span>Page {{nextpage / stop + 1}} of {{max_page / stop + 1}}</span></li>
                        <li><a   ng-click="onNextPage()">&#8250;</a></li>
                        <li><a   ng-click="onLastPage()">&#8250;&#8250;</a></li>
                    </ul>
                </td>
            </tr>
        </tfoot>
    </table>

</div>
<script src="<?php echo base_url(); ?>assets/js/md5.js"></script>

