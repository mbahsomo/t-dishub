<script src="<?php echo base_url(); ?>assets/lib/doevent/do-event-input.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.grid.js"></script>
<script type="text/javascript">
	'use strict';
	var ngDishub = angular.module('ngDishub', ['ngCsv','ngDoeventrow']);
	ngDishub.directive('focus', function($timeout, $parse) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				scope.$watch(attrs.focus, function(newValue, oldValue) {
					if (newValue) { element[0].focus(); }
				});
				element.bind("blur", function(e) {
					$timeout(function() {
						scope.$apply(attrs.focus + "=false"); 
					}, 0);
				});
				element.bind("focus", function(e) {
					$timeout(function() {
						scope.$apply(attrs.focus + "=true");
					}, 0);
				})
			}
		}
	});
	ngDishub.controller('KendaraanCtrl', function($scope, $http, $sce, $document) {
		$scope.errorshow=false;
		$scope.pesanerror="";
		$scope.orderByField = 'pob_nopol';
        $scope.reverseSort = false;
        
        $scope.find = {};
		
		$scope.stop = <?php echo isset($stop)?$stop:5; ?>;
		$scope.max_page = <?php echo isset($max_page)?$max_page:5; ?>;
		$scope.nextpage = 0;

		$scope.edit = -1 ;
		$scope.fieldcaris = [{name:'NOPOL',value:'pob_nopol'},{name:'PO',value:'po_code'}];
		$scope.fieldcari = $scope.fieldcaris[0];
		$scope.txtcari = '';
		
		$scope.obj = {pob_nopol:'',pob_nopolname:'',k_keterangan:'',k_region:0,k_status:true, p_id : 0};
		$scope.kendaraan = { items : <?php echo count($rec)>0? json_encode($rec):"[]";?>};
		

		$scope.pos = [];
		$scope.getPo = function(){
            $http.get('<?php echo site_url() . '/po/get_all/' . $this->session->userdata('hashkey'); ?>' ).
            success(function(data) {
                if (data != undefined) {
                    $scope.pos = data.rec;
                }
            });
        };
        $scope.getPo();

		$scope.showErrorMessage = function() {
			return $sce.trustAsHtml($scope.pesanerror);
		};
		//Add data
		$scope.showAdd = function (){
        	$scope.edit = -1 ;
        	$scope.obj = {pob_nopol:'',po_code:''};
        	$scope.errorshow=false;
        	$('#win-kendaraan').modal('show');
        }

        $scope.addItem = function() {
        	var evt = "";
        	if ($scope.edit == -1 ){
		        evt = "insert_data";
		    }else{
		    	evt = "edit_data" ;
		    }
			$http.get('<?php echo site_url() .'/kendaraan/'; ?>' + evt, {
				params: {
					pob_nopol : $scope.obj.pob_nopol,
                    po_code : $scope.obj.po_code
				}
			}).
			success(function(data) {
				if(data!=undefined){
					if(data.success==true){
						$scope.obj.user_entry = '<?php echo $this->session->userdata('user_name'); ?>';
                        $scope.obj.date_edit = new Date();
						if ($scope.edit == -1 ){
                            $scope.obj.date_entry = new Date();
							$scope.kendaraan.items.push($scope.obj);
						}else{
							$scope.kendaraan.items[$scope.edit] = $scope.obj;
						}
               			$scope.errorshow=false;
						$('#win-kendaraan').modal('hide');
                	}else{
                		//$('#msg-error').html(data.msg);
						$scope.pesanerror=data.msg;
                		$scope.errorshow=true;                		
                	}
				}
			});	    		
	    	
	    }
	    //Edit data
	    $scope.editItem = function(index, item){
	    	$scope.edit = index ;
	    	$scope.obj = item;
			//$scope.obj.level = item.u_level;
	    	$('#win-kendaraan').modal('show');
	    }

		//Hapus data
		$scope.removeItem = function(index, item) {

	    	if(confirm('Anda yakin HAPUS data ini? ')){
				$http.get('<?php echo site_url() .'/kendaraan/delete_data/'; ?>', {
					params: {
						'pob_nopol' :  item.pob_nopol
					}
				}).
				success(function(data) {
					if(data!=undefined){
						$scope.kendaraan.items.splice(index, 1);
						//$scope.max_page = data.max_page;
					}
				});	    		
	    	}
	    };
        
        $scope.search = function(varFind){
            var fieldcari = '';
            var fieldvalue = '';
            angular.forEach(varFind, function(value, key){
                if (value!=='') fieldcari += ((fieldcari!=='')?';':'')+ key ;
                if(value!=='') fieldvalue += ((fieldvalue!=='')?';':'')+ value ;
            });
            
            $http.get('<?php echo site_url() .'/kendaraan/search/'; ?>', {
                params: {
                    'field': (varFind!=='')?fieldcari:'',
                    'value': (varFind!=='')?fieldvalue:'',
                    'stop'  : $scope.nextpage
                }
            }).
            success(function(data) {
                if (data !== undefined) {
                    $scope.kendaraan = { items : data.rec};
                    $scope.max_page = data.max_page;
                    if($scope.max_page<$scope.nextpage){
						$scope.nextpage=0;
					}
                }
            });
        };

		$scope.onKeydown= function(ev){
			$scope.pressed = ev.which;
			if (ev.which===13 || ev.which===9)
	            $scope.search($scope.find);

		}

		$scope.onFirstPage = function(){
			if ($scope.nextpage  > 0 ){
				$scope.nextpage = 0;
				$scope.search($scope.find);
			}
		}

		$scope.onNextPage = function(){
			if (($scope.nextpage + $scope.stop) <= $scope.max_page ){
				$scope.nextpage = $scope.nextpage+$scope.stop;
				//console.log($scope.nextpage);
				$scope.search($scope.find);
			}
		}

		$scope.onPrevPage = function(){
			if ($scope.nextpage  >0 ){
				$scope.nextpage = $scope.nextpage-$scope.stop;
				$scope.search($scope.find);
			}
		}		

		$scope.onLastPage = function(){
			if ($scope.nextpage  < $scope.max_page ){
				$scope.nextpage = $scope.max_page;
				$scope.search($scope.find);
			}
		}

		$scope.selectRow = function(row) {
          $scope.selectedRow = row;
        };

        $scope.cetak = function (){
			print_preview('<?php echo site_url(). '/'. $this->fn .'/cetak/'; ?>','');
		}

		$document.bind('keydown', function(event) {
			console.debug(event.which);
			switch (event.which)
            {
                case 65:
                    if(event.altKey){
                        $scope.showAdd();
                        return false;
                    }
                    break;
                case 80 :
                	if(event.altKey){
                        $scope.cetak();
                        return false;
                    }
                	break;
            }
    	});
		
	}).directive('deFocus', deEvent);
</script>