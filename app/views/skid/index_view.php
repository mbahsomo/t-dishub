
<?php  
	$data['rec'] = $rec;
	$data['stop'] = $stop;
	$data['max_page'] = $max_page;
	$this->load->view('skid/controller', $data);
?>
<div ng-controller="SkidCtrl">
	<script type="text/ng-template" id="customTemplate.html">
	  <a>
	      <span bind-html-unsafe="match.label | typeaheadHighlight:query"></span>
	      <i>({{match.model.po_name}})</i>
	  </a>
	</script>

	<script type="text/ng-template" id="customTemplateTrayek.html">
	  <a>
	      <span bind-html-unsafe="match.label | typeaheadHighlight:query"></span>
	      <i>({{match.model.ty_name}})</i>
	  </a>
	</script>
	<!--Windows Modal-->
	<div class="modal fade" id="win-skid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add / Edit Data</h4>
				</div>
				<div class="modal-body">

					<div ng-show="errorshow" class="alert alert-danger" ng-bind-html='showErrorMessage()'></div>
					<div class="form-horizontal" role="form">
						<div class="form-group">
						   	<label for="po_code" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_code'); ?></label>
						    <div class="col-sm-9 input-group input-group-sm">
								<input type="text" class="form-control input-sm" placeholder="Cari PO"
	                            ng-model="dataPO"
	                            typeahead="c as c.po_code for c in pos | filter:$viewValue | limitTo:10"
	                            typeahead-min-length='1'
	                            ng-change='onChangePart(dataPO)'
	                            typeahead-on-select='onSelectPart($item, $model, $label)'
	                            typeahead-template-url="customTemplate.html" >
	                            <span class="input-group-addon">{{dataPO.po_name}}</span>
						    </div>
					  	</div>
					  	<div class="form-group">
						   	<label for="ty_code" class="col-sm-3 control-label">Trayek</label>
						    <div class="col-sm-9 input-group input-group-sm">
								<input type="text" class="form-control input-sm" placeholder="Cari PO"
	                            ng-model="dataTrayek"
	                            typeahead="c as c.ty_code for c in trayeks | filter:$viewValue | limitTo:10"
	                            typeahead-min-length='1'
	                            ng-change='onChangePart(dataTrayek)'
	                            typeahead-on-select='onSelectPart($item, $model, $label)'
	                            typeahead-template-url="customTemplateTrayek.html" >
	                            <span class="input-group-addon">{{dataTrayek.ty_name}}</span>
						    </div>
					  	</div>
						<div class="form-group">
						   	<label for="skid_code" class="col-sm-3 control-label">Nomer SKID</label>
						    <div class="col-sm-6 input-group input-group-sm">
								<input de-focus ng-model="obj.skid_code" type="text" class="form-control input-sm" id="skid_code" placeholder="SKID">
						    </div>
					  	</div>
					  	<div class="form-group">
						   	<label for="skid_tgl" class="col-sm-3 control-label">Tgl Penetapan</label>
						    <div class="col-sm-4 input-group input-group-sm">
								<input  ui-mask="99/99/9999"  de-focus ng-model="obj.skid_tgl" type="text" class="form-control input-sm" id="skid_tgl" >
						    </div>
					  	</div>
					  	<div class="form-group">
						   	<label for="skid_tgl_awal" class="col-sm-3 control-label">Tgl Mulai</label>
						    <div class="col-sm-4 input-group input-group-sm">
								<input  ui-mask="99/99/9999"  de-focus ng-model="obj.skid_tgl_awal" type="text" class="form-control input-sm" id="skid_tgl_awal" >
						    </div>
					  	</div>
					  	<div class="form-group">
						   	<label for="skid_tgl_akhir" class="col-sm-3 control-label">Tgl Selesai</label>
						    <div class="col-sm-4 input-group input-group-sm">
								<input  ui-mask="99/99/9999"  de-focus ng-model="obj.skid_tgl_akhir" type="text" class="form-control input-sm" id="skid_tgl_akhir" >
						    </div>
					  	</div>
					  	<div class="form-group">
						   	<label for="skid_nosk" class="col-sm-3 control-label">Nomer SK</label>
						    <div class="col-sm-6 input-group input-group-sm">
								<input de-focus ng-model="obj.skid_nosk" type="text" class="form-control input-sm" id="skid_nosk" placeholder="SKID">
						    </div>
					  	</div>
					  	<div class="form-group">
						   	<label for="skid_tgl_nosk" class="col-sm-3 control-label">Tgl Penetapan</label>
						    <div class="col-sm-4 input-group input-group-sm">
								<input  ui-mask="99/99/9999"  de-focus ng-model="obj.skid_tgl_nosk" type="text" class="form-control input-sm" id="skid_tgl_nosk" >
						    </div>
					  	</div>
					  	<div class="form-group">
						   	<label for="skid_jumlah" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('skid_jumlah'); ?></label>
						    <div class="col-sm-3 input-group input-group-sm">
						    	<input  de-focus ng-model="obj.skid_jumlah" class="form-control input-sm" id="skid_jumlah" >
						    </div>
					   	</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary btn-sm" ng-click="addItem()" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<?php $this->load->view('template/toolbar_grid'); ?>
	
	<form class="navbar-form" style="padding-left:0;">
		<div class="form-group">
			<select class="form-control input-sm" ng-model="fieldcari" ng-options="c.name for c in fieldcaris"></select>
		</div>
			<div class="form-group">
				<input type="text" placeholder="Data yang dicari" class="form-control input-sm" ng-model="txtcari" ng-keydown="onKeydown($event)"/>
			</div>
		<button type="button" class="btn btn-success btn-sm" ng-click="search()"><span class="glyphicon glyphicon-search"></span> Cari</button>
	</form>
        
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<td>No</td>
				<?php
	            foreach ($this->mdl->get_rule() as $val) {
	                if (isset($val['grid'])) {
	                    if ($val['grid']){ ?>
	                        <th>
	                            <a   ng-click="orderByField='<?php echo $val['field']; ?>'; reverseSort = !reverseSort">
	                                <?php echo $this->mdl->get_label($val['field']); ?>
	                                <span ng-show="orderByField == '<?php echo $val['field']; ?>'">
	                                    <span ng-show="!reverseSort" class="glyphicon glyphicon-chevron-up"></span>
	                                    <span ng-show="reverseSort" class="glyphicon glyphicon-chevron-down"></span>
	                                </span>
	                            </a>
	                        </th>                            
	                    <?php }
	                }else{?>
	                    <th>
	                        <a   ng-click="orderByField='<?php echo $val['field']; ?>'; reverseSort = !reverseSort">
	                            <?php echo $this->mdl->get_label($val['field']); ?>
	                            <span ng-show="orderByField == '<?php echo $val['field']; ?>'">
	                                <span ng-show="!reverseSort" class="glyphicon glyphicon-chevron-up"></span>
	                                <span ng-show="reverseSort" class="glyphicon glyphicon-chevron-down"></span>
	                            </span>
	                        </a>
	                    </th>                        
	                <?php }
	            }
	            ?>
				<td width="100px">User Entry</td>
	            <td width="155px">Tgl Entry</td>
	            <td width="155px">Tgl Edit</td>
				<td width="150px">Event</td>
			</tr>
		</thead>
		<tbody>
			<tr ng-doeventrow="item" set-obj-rows="setObjRows(item)" tabindex="{{$index}}" ng-repeat="item in skid.items | orderBy:orderByField:reverseSort" class="first" ng-class='{"success":$index==selectedRow}' ng-click='selectRow($index)'>
				<td>{{$index+1}}</td>
				<td>{{item.skid_code}}</td>
				<td>{{item.skid_tgl}}</td>
				<td>{{item.skid_nosk}}</td>
				<td>{{item.po_name}}</td>
				<td>{{item.ty_name}}</td>
				<td>{{item.skid_jumlah}}</td>
				<td >{{item.user_entry}}</td>
                <td>{{item.date_entry | date: 'dd-MM-yyyy H:m:s'}}</td>
				<td>{{item.date_edit | date:'dd-MM-yyyy H:m:s'}}</td>
				<td>
					<a   class="btn btn-success btn-xs" ng-click="editItem($index,item)" ><span class="glyphicon glyphicon-edit"></span>  Edit</a>
					<a   class="btn btn-danger btn-xs" ng-click="removeItem($index, item)" ><span class="glyphicon glyphicon-floppy-remove"></span> Delete</a>
				</td>
			</tr>			
		</tbody>
		<tfoot>
			<tr>
				<td colspan="11" style="text-align:right;">
					<ul class="pagination pagination-sm" style="margin:0 0 0 0;">
						<li><a   ng-click="onFirstPage()">&#8249;&#8249;</a></li>
						<li><a   ng-click="onPrevPage()">&#8249;</a></li>
						<li><span>Page {{nextpage/stop+1}} of {{max_page/stop+1}}</span></li>
						<li><a   ng-click="onNextPage()">&#8250;</a></li>
						<li><a   ng-click="onLastPage()">&#8250;&#8250;</a></li>
					</ul>
				</td>
			</tr>
		</tfoot>
	</table>
</div>