<link href="<?php echo base_url(); ?>assets/lib/date-picker/date.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/lib/date-picker/date.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/doevent/do-event-input.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.grid.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/auto-complite/ui-bootstrap-tpls-0.9.0.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/mask/mask.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/date-picker/m-date.js"></script>
<script type="text/javascript">
	'use strict';
	var ngDishub = angular.module('ngDishub', ['ngCsv','ngDoeventrow','ui.bootstrap','ui.mask']);
	ngDishub.controller('SkidCtrl', function($scope, $http, $sce, $document, $filter) {
		$scope.errorshow=false;
		$scope.pesanerror="";
		
		$scope.stop = <?php echo isset($stop)?$stop:5; ?>;
		$scope.max_page = <?php echo isset($max_page)?$max_page:5; ?>;
		$scope.nextpage = 0;

		$scope.edit = -1 ;
		
		$scope.fieldcaris = [{name:'PO',value:'po_code'}];
		$scope.fieldcari = $scope.fieldcaris[0];
		$scope.txtcari = '';
		$scope.obj = {
			skid_code : '',
			skid_tgl : '<?php echo date('d-M-Y');?>',
			skid_tgl_awal : '',
			skid_tgl_akhir : '',
			skid_nosk :'',
			skid_tgl_nosk:'',
			skid_tgl_awal_nosk: '',
			skid_tgl_akhir_nosk: '',
			po_code : '',
			ty_code : ''
		};
		
		$scope.showErrorMessage = function() {
			return $sce.trustAsHtml($scope.pesanerror);
		};
		$scope.dataPO = {};
		$scope.dataTrayek = {};
		$scope.pos = [] ;
		$scope.loadPo = function(){
			$http.get('<?php echo site_url() .'/po/get_all'; ?>' ).success(function(data) {
				if(data!=undefined){
					$scope.pos = data.rec;
				}
			});
		};
		$scope.loadPo();

		$scope.trayeks = [] ;
		$scope.loadTrayek = function(po){
            $http.get('<?php echo site_url() .'/po_kendaraan/get_perpo/'; ?>' + po ).success(function(data) {
                if(data!=undefined){
                    $scope.trayeks = data.rec;
                }
            });
        };
        $scope.$watch('dataPO.po_code', function (newVal, oldVal) {
            if (newVal !== oldVal ) {
                $scope.loadTrayek(newVal);
            }
        }, true);

		/*$scope.loadTrayek = function(){
			$http.get('<?php echo site_url() .'/trayek/get_all'; ?>' ).success(function(data) {
				if(data!=undefined){
					$scope.trayeks = data.rec;
				}
			});
		};
		$scope.loadTrayek();*/
		//Add data
		$scope.showAdd = function (){
        	$scope.edit = -1 ;
        	$scope.obj = {
        		skid_code : '',
				skid_tgl : '<?php echo date('d-M-Y');?>',
				skid_tgl_awal : '',
				skid_tgl_akhir : '',
				skid_nosk :'',
				skid_tgl_nosk:'',
				skid_tgl_awal_nosk: '',
				skid_tgl_akhir_nosk: '',
				po_code : '',
				ty_code : ''
			};
			$scope.dataPO = {po_code:'',po_name :''};
			$scope.dataTrayek = {ty_code:'',ty_name :''};
        	$scope.errorshow=false;
        	$('#win-skid').modal('show');
        }

        $scope.addItem = function() {
        	var evt = "";
        	if ($scope.edit == -1 ){
		        evt = "insert_data";
		    }else{
		    	evt = "edit_data" ;
		    }
		    var vparams = {};
		    angular.forEach($scope.obj, function(value, key){
	            if(key !== '$$hashKey')
	                vparams[key] = value;
	        });
		    vparams['po_code'] = $scope.dataPO.po_code;
		    vparams['ty_code'] = $scope.dataTrayek.ty_code;
			$http.get('<?php echo site_url() .'/skid/'; ?>' + evt, {
				params: vparams
			}).
			success(function(data) {
				if(data!=undefined){
					if(data.success==true){
						$scope.obj.user_entry = '<?php echo $this->session->userdata('user_name'); ?>';
                        $scope.obj.date_edit = new Date();
						if ($scope.edit == -1 ){
                            $scope.obj.date_entry = new Date();
                            $scope.skid.items.push($scope.obj);
						}else{
							$scope.skid.items[$scope.edit] = $scope.obj;
						}
               			$scope.errorshow=false;
						$('#win-skid').modal('hide');
                	}else{
                		//$('#msg-error').html(data.msg);
						$scope.pesanerror=data.msg;
                		$scope.errorshow=true;                		
                	}
				}
			});	    		
	    	
	    }
	    //Edit data
	    $scope.editItem = function(index, item){
	    	console.log(item);
	    	$scope.edit = index ;
	    	$scope.obj = item;
	    	$scope.dataPO = {po_code:item.po_code,po_name :item.po_name};
			$scope.dataTrayek = {ty_code: item.ty_code ,ty_name : item.ty_name};
		    $scope.obj.skid_tgl = $filter('date')(item.skid_tgl, 'dd/MM/yyyy');
	  	    $scope.obj.skid_tgl_awal = $filter('date')(item.skid_tgl_awal, 'dd/MM/yyyy');
	  	    $scope.obj.skid_tgl_akhir = $filter('date')(item.skid_tgl_akhir, 'dd/MM/yyyy');
	  	    $scope.obj.skid_tgl_nosk = $filter('date')(item.skid_tgl_nosk, 'dd/MM/yyyy');
		  	$('#win-skid').modal('show');
	    }

		//Hapus data
		$scope.removeItem = function(index, item) {

	    	if(confirm('Anda yakin HAPUS data ini? ')){
				$http.get('<?php echo site_url() .'/skid/delete_data/'; ?>', {
					params: {
						'skid_code' :  item.skid_code
					}
				}).
				success(function(data) {
					if(data!=undefined){
						$scope.skid.items.splice(index, 1);
					}
				});	    		
	    	}
	    }
		
		$scope.search = function (){
			$http.get('<?php echo site_url() .'/skid/search/'; ?>', {
				params: {
					'field' : $scope.fieldcari.value,
					'value' : $scope.txtcari,
					'stop'  : $scope.nextpage
				}
			}).
			success(function(data) {
				if(data!=undefined){
					$scope.skid = { items : data.rec};
					$scope.max_page = data.max_page;
					if($scope.max_page<$scope.nextpage){
						$scope.nextpage=0;
					}
				}
			})
		}
		$scope.search();

		$scope.onKeydown= function(ev){
			$scope.pressed = ev.which;
			if (ev.which==13 || ev.which==9)
	            $scope.search();
		}

		$scope.onFirstPage = function(){
			if ($scope.nextpage  > 0 ){
				$scope.nextpage = 0;
				$scope.search();
			}
		}

		$scope.onNextPage = function(){
			if (($scope.nextpage + $scope.stop) <= $scope.max_page ){
				$scope.nextpage = $scope.nextpage+$scope.stop;
				//console.log($scope.nextpage);
				$scope.search();
			}
		}

		$scope.onPrevPage = function(){
			if ($scope.nextpage  >0 ){
				$scope.nextpage = $scope.nextpage-$scope.stop;
				$scope.search();
			}
		}		

		$scope.onLastPage = function(){
			if ($scope.nextpage  < $scope.max_page ){
				$scope.nextpage = $scope.max_page;
				$scope.search();
			}
		}

		$scope.selectRow = function(row) {
          $scope.selectedRow = row;
        };
		
		$scope.cetak = function (){
			print_preview('<?php echo site_url(). '/'. $this->fn .'/cetak/'; ?>','');
		}

		$document.bind('keydown', function(event) {
			console.debug(event.which);
			switch (event.which)
            {
                case 65:
                    if(event.altKey){
                        $scope.showAdd();
                        return false;
                    }
                    break;
                case 80 :
                	if(event.altKey){
                        $scope.cetak();
                        return false;
                    }
                	break;
            }
    	});    	

	}).directive('deFocus', deEvent).directive('mDatepicker', mDate);
</script>