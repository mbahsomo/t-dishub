<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="<?php echo base_url('assets/css/report.css'); ?>" title="no title" charset="utf-8">
    </head>
    <body><!--detail-->
        <table class="detail">
            <caption>Data Tarif Trayek</caption>
            <thead>
                <tr>
                    <th>No</th>
                    <th>Asal</th>
                    <th>Tujuan</th>
                </tr>
            </thead>
            <tbody>
                <?php $no=1; foreach ($rec as $rows) {?>
                <tr>
                    <td class="clstdcenter"><?php echo $no++; ?></td>
                    <td><?php echo $rows['asal']; ?></td>
                    <td><?php echo $rows['tujuan']; ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
        <!--detail-->
    </body>
</html>
