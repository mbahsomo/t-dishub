<script type="text/javascript">
    'use strict';
    var ngDishub = angular.module('ngDishub', ['ngCsv', 'ngDoeventrow','directives']);
    ngDishub.controller('TarifCtrl', function($scope, $http, $sce, $document) {
        $scope.errorshow = false;
        $scope.pesanerror = "";

        $scope.stop = <?php echo isset($stop) ? $stop : 5; ?>;
        $scope.max_page = <?php echo isset($max_page) ? $max_page : 5; ?>;
        $scope.nextpage = 0;

        $scope.edit = -1;
        $scope.terminals = <?php echo count($terminal) > 0 ? json_encode($terminal) : "[]"; ?>;
        $scope.fieldcaris = [{name: 'Tarif Name', value: 'u_name'}, {name: 'Email', value: 'u_email'}];
        $scope.fieldcari = $scope.fieldcaris[0];
        $scope.txtcari = '';
        $scope.obj = {ta_id: 0, ta_asal: '', ta_tujuan: '', ta_atas: 0, ta_bawah: 0};
        $scope.tarif = {items: <?php echo count($rec) > 0 ? json_encode($rec) : "[]"; ?>};

        $scope.showErrorMessage = function() {
            return $sce.trustAsHtml($scope.pesanerror);
        };
        //Add data
        $scope.showAdd = function() {
            $scope.edit = -1;
            $scope.obj = {ta_id: 0, ta_asal: '', ta_tujuan: '', ta_atas: 0, ta_bawah: 0};
            $scope.errorshow = false;
            $('#win-tarif').modal('show');
        };

        $scope.addItem = function() {
            var evt = "";
            if ($scope.edit === -1) {
                evt = "insert_data";
            } else {
                evt = "edit_data";
            }
            $http.get('<?php echo site_url() . '/tarif/'; ?>' + evt, {
                params: {
                    ta_id: $scope.obj.ta_id,
                    ta_asal: $scope.obj.tasal.t_code,
                    ta_tujuan: $scope.obj.ttujuan.t_code,
                    ta_atas: $scope.obj.ta_atas,
                    ta_bawah: $scope.obj.ta_bawah
                }
            }).
            success(function(data) {
                if (data !== undefined) {
                    if (data.success === true) {
                        $scope.obj.user_entry = '<?php echo $this->session->userdata('user_name'); ?>';
                        $scope.obj.date_edit = new Date();
                        $scope.obj.ta_asal = $scope.obj.tasal.t_code;
                        $scope.obj.ta_tujuan = $scope.obj.ttujuan.t_code;
                        $scope.obj.asal = $scope.obj.tasal.t_name;
                        $scope.obj.tujuan = $scope.obj.ttujuan.t_name;
                        if ($scope.edit === -1) {
                            $scope.obj.date_entry = new Date();
                            $scope.obj.ta_id = data.key;
                            $scope.tarif.items.push($scope.obj);
                        } else {
                            $scope.tarif.items[$scope.edit] = $scope.obj;
                        }
                        $scope.errorshow = false;
                        $('#win-tarif').modal('hide');
                    } else {
                        //$('#msg-error').html(data.msg);
                        $scope.pesanerror = data.msg;
                        $scope.errorshow = true;
                    }
                }
            });

        };
        //Edit data
        $scope.editItem = function(index, item) {
            $scope.edit = index;
            item.tasal = $scope.SearchTerminalObj(item.ta_asal);
            item.ttujuan = $scope.SearchTerminalObj(item.ta_tujuan);
            angular.element('#asal input').val( item.tasal.t_code );
			angular.element('#tujuan input').val( item.ttujuan.t_code );
            $scope.obj = item;
            console.log($scope.obj);
            $('#win-tarif').modal('show');
        };

        //Hapus data
        $scope.removeItem = function(index, item) {

            if (confirm('Anda yakin HAPUS data ini? ')) {
                $http.get('<?php echo site_url() . '/tarif/delete_data/'; ?>', {
                    params: {
                        'ta_id': item.ta_id
                    }
                }).
                success(function(data) {
                    if (data !== undefined) {
                        $scope.tarif.items.splice(index, 1);
                        //$scope.max_page = data.max_page;
                    }
                });
            }
        };

        $scope.search = function() {
            $http.get('<?php echo site_url() . '/tarif/search/'; ?>', {
                params: {
                    'field': $scope.fieldcari.value,
                    'value': $scope.txtcari,
                    'stop': $scope.nextpage
                }
            }).
            success(function(data) {
                if (data !== undefined) {
                    $scope.tarif = {items: data.rec};
                    $scope.max_page = data.max_page;
                    if ($scope.max_page < $scope.nextpage) {
                        $scope.nextpage = 0;
                    }
                }
            });
        };

        $scope.print = function() {
            print_preview('<?php echo site_url() . '/tarif/cetak/'; ?>', '', 'pos_print');
        };

        $scope.onKeydown = function(ev) {
            $scope.pressed = ev.which;
            if (ev.which === 13 || ev.which === 9)
                $scope.search();
        };

        $scope.onFirstPage = function() {
            if ($scope.nextpage > 0) {
                $scope.nextpage = 0;
                $scope.search();
            }
        };

        $scope.onNextPage = function() {
            if (($scope.nextpage + $scope.stop) <= $scope.max_page) {
                $scope.nextpage = $scope.nextpage + $scope.stop;
                //console.log($scope.nextpage);
                $scope.search();
            }
        };

        $scope.onPrevPage = function() {
            if ($scope.nextpage > 0) {
                $scope.nextpage = $scope.nextpage - $scope.stop;
                $scope.search();
            }
        };

        $scope.onLastPage = function() {
            if ($scope.nextpage < $scope.max_page) {
                $scope.nextpage = $scope.max_page;
                $scope.search();
            }
        };

        $scope.selectRow = function(row) {
            $scope.selectedRow = row;
        };

        $scope.cetak = function() {
            print_preview('<?php echo site_url() . '/' . $this->fn . '/cetak/'; ?>', '');
        };

        $document.bind('keydown', function(event) {
            switch (event.which)
            {
                case 65:
                    if (event.altKey) {
                        $scope.showAdd();
                        return false;
                    }
                    break;
                case 80 :
                    if (event.altKey) {
                        $scope.cetak();
                        return false;
                    }
                    break;
            }
        });

        $scope.SearchTerminal = function(id) {
            var hasil = "";
            angular.forEach($scope.terminals, function(value, key) {
                if (id === value.t_code) {
                    hasil = value.t_name;
                    return false;
                }
            });
            return hasil;
        };

        $scope.SearchTerminalObj = function(id) {
            var hasil = [];
            angular.forEach($scope.terminals, function(value, key) {
                if (id === value.t_code) {
                    hasil = value;
                    return false;
                }
            });
            return hasil;
        };

        $scope.exportExcel = function(){
            var urlprint = "<?php echo site_url(). '/'. $this->fn .'/exportxls/'; ?>";
            var win = window.open(urlprint,'Export','height=255,width=250,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,top=20,left=20');
            setInterval(
                function(){
                    win.close();
                    return false;
                },
                7000
            );
        }

    }).directive('deFocus', deEvent);
</script>
