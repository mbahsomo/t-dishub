<script src="<?php echo base_url(); ?>assets/lib/doevent/do-event-input.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.grid.js"></script>
<link href="<?php echo base_url(); ?>assets/lib/auto-complete/auto-complete.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/lib/auto-complete/auto-complete.js"></script>
<?php
$data['rec'] = $rec;
$data['stop'] = $stop;
$data['max_page'] = $max_page;
$this->load->view('tarif/controller', $data);
?>
<div id="pos_print"></div>
<div ng-controller="TarifCtrl">
    <!--Windows Modal-->
    <div class="modal fade" id="win-tarif" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add / Edit Data</h4>
                </div>
                <div class="modal-body">

                    <div ng-show="errorshow" class="alert alert-danger" ng-bind-html='showErrorMessage()'></div>

                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="ta_asal" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('ta_asal'); ?></label>
                            <div class="col-sm-8 input-group input-group-sm">
                                <autocomplete id="asal" ng-model="obj.tasal"
                                    items="terminals"
                                    key-field="t_code"
                                    text-field="t_code"
                                    no-item-text="">
                                    {{item.t_code}}
                                </autocomplete>
                                <span id="sasal" class="input-group-addon">{{obj.tasal.t_name}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ta_tujuan" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('ta_tujuan'); ?></label>
                            <div class="col-sm-8 input-group input-group-sm">
                                <autocomplete id="tujuan" ng-model="obj.ttujuan"
                                    items="terminals"
                                    key-field="t_code"
                                    text-field="t_code"
                                    no-item-text="">
                                    {{item.t_code}}
                                </autocomplete>
                                <span id="stujuan" class="input-group-addon">{{obj.ttujuan.t_name}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ta_atas" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('ta_atas'); ?></label>
                            <div class="col-sm-3">
                                <input de-focus ng-model="obj.ta_atas" class="form-control input-sm" id="ta_atas" placeholder="<?php echo $this->mdl->get_label('ta_atas'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ta_bawah" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('ta_bawah'); ?></label>
                            <div class="col-sm-3">
                                <input de-focus ng-model="obj.ta_bawah" class="form-control input-sm" id="ta_bawah" placeholder="<?php echo $this->mdl->get_label('ta_bawah'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" ng-click="addItem()" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="btn-group toolbar navbar-right">
        <button type="button" class="btn btn-default btn-sm"  ng-click="showAdd()"><span class="glyphicon glyphicon-new-window"></span> Add</button>
        <button type="button" class="btn btn-default btn-sm" ng-click="print();"><span class="glyphicon glyphicon-print"></span> Print</button>
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown"><span class="glyphicon glyphicon-export"></span> Export <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a href="#" ng-click="exportExcel()" >Excel</a></li>
                <!-- <li><a  >PDF</a></li> -->
                <li><a  >Word</a></li>
            </ul>
        </div>
    </div>

    <form class="navbar-form" style="padding-left:0;">
        <div class="form-group">
            <select class="form-control input-sm" ng-model="fieldcari" ng-options="c.name for c in fieldcaris"></select>
        </div>
        <div class="form-group">
            <input type="text" placeholder="Data yang dicari" class="form-control input-sm" ng-model="txtcari" ng-keydown="onKeydown($event)"/>
        </div>
        <button type="button" class="btn btn-success btn-sm" ng-click="search()"><span class="glyphicon glyphicon-search"></span> Cari</button>
    </form>

    <!-- id="tbl-scroll" -->
    <div class="table-responsive">
        <table id="tbl-scroll" class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <td colspan="9" style="text-align:right;">
                        <ul class="pagination pagination-sm" style="margin:0 0 0 0;">
                            <li><a   ng-click="onFirstPage()">&#8249;&#8249;</a></li>
                            <li><a   ng-click="onPrevPage()">&#8249;</a></li>
                            <li><span>Page {{nextpage / stop + 1}} of {{max_page / stop + 1}}</span></li>
                            <li><a   ng-click="onNextPage()">&#8250;</a></li>
                            <li><a   ng-click="onLastPage()">&#8250;&#8250;</a></li>
                        </ul>
                    </td>
                </tr>
                <tr>
                    <th>No</th>
                    <th><?php echo $this->mdl->get_label('ta_asal'); ?></th>
                    <th><?php echo $this->mdl->get_label('ta_tujuan'); ?></th>
                    <th><?php echo $this->mdl->get_label('ta_atas'); ?></th>
                    <th><?php echo $this->mdl->get_label('ta_bawah'); ?></th>
                    <td width="100px">User Entry</td>
                    <td width="155px">Tgl Entry</td>
                    <td width="155px">Tgl Edit</td>
                    <th width="150px">Event</th>
                </tr>
            </thead>
            <tbody>
                <tr ng-doeventrow="item" set-obj-rows="setObjRows(item)" tabindex="{{$index}}"  ng-repeat="item in tarif.items" class="first" ng-class='{"success":$index == selectedRow}' ng-click='selectRow($index)'>
                    <td>{{$index + 1}}</td>
                    <td>{{item.asal}}</td>
                    <td>{{item.tujuan}}</td>
                    <td>{{item.ta_atas}}</td>
                    <td>{{item.ta_bawah}}</td>
                    <td>{{item.user_entry}}</td>
                    <td>{{item.date_entry| date: 'dd-MM-yyyy H:m:s'}}</td>
                    <td>{{item.date_edit| date:'dd-MM-yyyy H:m:s'}}</td>
                    <td>
                        <a   class="btn btn-success btn-xs" ng-click="editItem($index, item)" ><span class="glyphicon glyphicon-edit"></span>  Edit</a>
                        <a   class="btn btn-danger btn-xs" ng-click="removeItem($index, item)" ><span class="glyphicon glyphicon-floppy-remove"></span> Delete</a>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="9" style="text-align:right;">
                        <ul class="pagination pagination-sm" style="margin:0 0 0 0;">
                            <li><a   ng-click="onFirstPage()">&#8249;&#8249;</a></li>
                            <li><a   ng-click="onPrevPage()">&#8249;</a></li>
                            <li><span>Page {{nextpage / stop + 1}} of {{max_page / stop + 1}}</span></li>
                            <li><a   ng-click="onNextPage()">&#8250;</a></li>
                            <li><a   ng-click="onLastPage()">&#8250;&#8250;</a></li>
                        </ul>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<script src="<?php echo base_url(); ?>assets/js/md5.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.print.js"></script>
