<?php
$data['rec'] = $rec;
$data['stop'] = $stop;
$data['max_page'] = $max_page;
$this->load->view('trayekkendaraan/controller', $data);
?>
<div ng-controller="KendaraanCtrl">
    <script type="text/ng-template" id="customTemplate.html">
      <a>
          <span bind-html-unsafe="match.label | typeaheadHighlight:query"></span>
          <i>({{match.model.po_name}})</i>
      </a>
    </script>

    <script type="text/ng-template" id="customTemplateTrayek.html">
      <a>
          <span bind-html-unsafe="match.label | typeaheadHighlight:query"></span>
          <i>({{match.model.ty_name}})</i>
      </a>
    </script>
    <!--Windows Modal-->
    <div class="modal fade" id="win-kendaraan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog"  style="width:90%;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add / Edit Data</h4>
                </div>
                <div class="modal-body">

                    <div ng-show="errorshow" class="alert alert-danger" ng-bind-html='showErrorMessage()'></div>

                    <div class="form-horizontal" role="form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="po_code" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_code'); ?></label>
                                    <div class="col-sm-9 input-group input-group-sm">
                                        <input type="text" class="form-control input-sm" placeholder="Cari PO"
                                        ng-model="dataPO"
                                        typeahead="c as c.po_code for c in pos | filter:$viewValue | limitTo:10"
                                        typeahead-min-length='1'
                                        ng-change='onChangePart(dataPO)'
                                        typeahead-on-select='onSelectPart($item, $model, $label)'
                                        typeahead-template-url="customTemplate.html" >
                                        <span class="input-group-addon">{{dataPO.po_name}}</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="pok_nopol" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_nopol'); ?></label>
                                    <div class="col-sm-4">
                                        <input de-focus ng-model="obj.pok_nopol" type="text" class="form-control input-sm" id="pok_nopol" placeholder="<?php echo $this->mdl->get_label('pok_nopol'); ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="pok_nostuk" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_nostuk'); ?></label>
                                    <div class="col-sm-4">
                                        <input de-focus ng-model="obj.pok_nostuk" type="text" class="form-control input-sm" id="pok_nostuk" placeholder="<?php echo $this->mdl->get_label('pok_nostuk'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_tgl_uji" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_tgl_uji'); ?></label>
                                    <div class="col-sm-4 input-group input-group-sm">
                                        <input m-datepicker ui-mask="9999-99-99" de-focus ng-model="obj.pok_tgl_uji" type="text" class="form-control input-sm" >
                                        <a  href="" class="input-group-addon btn btn-default btn-sm" data-toggle="datepicker"><span class="glyphicon glyphicon-calendar"></span></a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_merk" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_merk'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.pok_merk" type="text" class="form-control input-sm" id="pok_merk" placeholder="<?php echo $this->mdl->get_label('pok_merk'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_type" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_type'); ?></label>
                                    <div class="col-sm-9">
                                        <input de-focus ng-model="obj.pok_type" type="text" class="form-control input-sm" id="pok_type" placeholder="<?php echo $this->mdl->get_label('pok_type'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_tahun_buat" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_tahun_buat'); ?></label>
                                    <div class="col-sm-3">
                                        <input de-focus ng-model="obj.pok_tahun_buat" type="number" class="form-control input-sm" id="pok_tahun_buat" placeholder="<?php echo $this->mdl->get_label('pok_tahun_buat'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_nochasis" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_nochasis'); ?></label>
                                    <div class="col-sm-5">
                                        <input de-focus ng-model="obj.pok_nochasis" type="text" class="form-control input-sm" id="pok_nochasis" placeholder="<?php echo $this->mdl->get_label('pok_nochasis'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_nomesin" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_nomesin'); ?></label>
                                    <div class="col-sm-5">
                                        <input de-focus ng-model="obj.pok_nomesin" type="text" class="form-control input-sm" id="pok_nomesin" placeholder="<?php echo $this->mdl->get_label('pok_nomesin'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_nolambung" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_nolambung'); ?></label>
                                    <div class="col-sm-5">
                                        <input de-focus ng-model="obj.pok_nolambung" type="text" class="form-control input-sm" id="pok_nolambung" placeholder="<?php echo $this->mdl->get_label('pok_nolambung'); ?>">
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="pok_duduk" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_duduk'); ?></label>
                                    <div class="col-sm-3">
                                        <input de-focus ng-model="obj.pok_duduk" type="number" class="form-control input-sm" id="pok_duduk" placeholder="<?php echo $this->mdl->get_label('pok_duduk'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_barang" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_barang'); ?></label>
                                    <div class="col-sm-3">
                                        <input de-focus ng-model="obj.pok_barang" type="number" class="form-control input-sm" id="pok_barang" placeholder="<?php echo $this->mdl->get_label('pok_barang'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ja_id" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('ja_id'); ?></label>
                                    <div class="col-sm-9">
                                        <select style="width:200px;" chosen de-focus ng-model="dataJa" class="form-control
                                                input-sm" ng-options="ja.ja_name for ja in jas" >
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ty_code" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('ty_code'); ?></label>
                                    <div class="col-sm-9 input-group input-group-sm">
                                        <input type="text" class="form-control input-sm" placeholder="Cari Trayek"
                                        ng-model="dataTrayek"
                                        typeahead="c as c.ty_code for c in trayeks | filter:$viewValue | limitTo:10"
                                        typeahead-min-length='1'
                                        ng-change='onChangePart(dataTrayek)'
                                        typeahead-on-select='onSelectPart($item, $model, $label)'
                                        typeahead-template-url="customTemplateTrayek.html" >
                                        <span class="input-group-addon">{{dataTrayek.ty_name}}</span>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_nokps" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_nokps'); ?></label>
                                    <div class="col-sm-5">
                                        <input de-focus ng-model="obj.pok_nokps" type="text" class="form-control input-sm" id="pok_nokps" placeholder="<?php echo $this->mdl->get_label('pok_nokps'); ?>">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_tgl_kps" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_tgl_kps'); ?></label>
                                    <div class="col-sm-4 input-group input-group-sm">
                                        <input m-datepicker ui-mask="9999-99-99"  de-focus ng-model="obj.pok_tgl_kps" type="text" class="form-control input-sm" id="pok_tgl_kps" >
                                        <a  href="" class="input-group-addon btn btn-default btn-sm" data-toggle="datepicker"><span class="glyphicon glyphicon-calendar"></span></a>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_kps" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_kps'); ?></label>
                                    <div class="col-sm-1">
                                        <input de-focus ng-model="obj.pok_kps" type="checkbox" class="form-control input-sm" id="pok_kps" placeholder="<?php echo $this->mdl->get_label('pok_kps'); ?>" ng-true-value="Y" ng-false-value="N" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_spk" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_spk'); ?></label>
                                    <div class="col-sm-1">
                                        <input de-focus ng-model="obj.pok_spk" type="checkbox" class="form-control" id="pok_spk" placeholder="<?php echo $this->mdl->get_label('pok_spk'); ?>" ng-true-value="Y" ng-false-value="N" >
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_cetak" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_cetak'); ?></label>
                                    <div class="col-sm-1">
                                        <input de-focus ng-model="obj.pok_cetak" type="checkbox" class="form-control input-sm" id="pok_cetak" placeholder="<?php echo $this->mdl->get_label('pok_cetak'); ?>" ng-true-value="Y" ng-false-value="N" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="pok_status" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_status'); ?></label>
                                    <div class="col-sm-1">
                                        <input de-focus ng-model="obj.pok_status" type="checkbox" class="form-control input-sm" id="pok_status" placeholder="<?php echo $this->mdl->get_label('pok_status'); ?>" ng-true-value="A" ng-false-value="N" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" ng-click="addItem()" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <?php $this->load->view('template/toolbar_grid'); ?>

<!--    <form class="navbar-form" style="padding-left:0;">
        <div class="form-group">
            <select class="form-control input-sm" ng-model="fieldcari" ng-options="c.name for c in fieldcaris"></select>
        </div>
        <div class="form-group">
            <input type="text" placeholder="Data yang dicari" class="form-control input-sm" ng-model="txtcari" ng-keydown="onKeydown($event)"/>
        </div>
        <button type="button" class="btn btn-success btn-sm" ng-click="search()"><span class="glyphicon glyphicon-search"></span> Cari</button>
    </form>-->

    <!-- id="tbl-scroll" -->
    <table id="tbl-scroll" class="table table-bordered table-striped table-hover detail">
        <thead>
            <tr>
                <th width="15px;">No</th>
                <?php
                foreach ($this->mdl->get_rule() as $val) {
                    if (isset($val['grid'])) {
                        if ($val['grid']){ ?>
                            <th <?php echo isset($val['width'])?'style="width:' .$val['width'] .'"':''; ?>>
                                <a   ng-click="orderByField='<?php echo $val['field']; ?>'; reverseSort = !reverseSort">
                                    <?php echo $this->mdl->get_label($val['field']); ?>
                                    <span ng-show="orderByField == '<?php echo $val['field']; ?>'">
                                        <span ng-show="!reverseSort" class="glyphicon glyphicon-chevron-up"></span>
                                        <span ng-show="reverseSort" class="glyphicon glyphicon-chevron-down"></span>
                                    </span>
                                </a>
                            </th>                            
                        <?php }
                    }else{?>
                        <th <?php echo isset($val['width'])?'style="width:' .$val['width'] .'"':''; ?>>
                            <a   ng-click="orderByField='<?php echo $val['field']; ?>'; reverseSort = !reverseSort">
                                <?php echo $this->mdl->get_label($val['field']); ?>
                                <span ng-show="orderByField == '<?php echo $val['field']; ?>'">
                                    <span ng-show="!reverseSort" class="glyphicon glyphicon-chevron-up"></span>
                                    <span ng-show="reverseSort" class="glyphicon glyphicon-chevron-down"></span>
                                </span>
                            </a>
                        </th>                        
                    <?php }
                }
                ?>
                <td width="100px">User Entry</td>
                <td width="155px">Tgl Entry</td>
                <td width="155px">Tgl Edit</td>
                <th width="150px" >Event</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td>
                    <input ng-change="search(find)" class="form-control input-sm" ng-model="find.pob_nopol" />
                </td>
                <td>
                    <select ng-change="search(find)" ng-model="find.po_code" class="form-control input-sm" >
                        <option value="">All</option>
                        <option ng-repeat="item in pos" value="{{item.po_code}}">{{item.po_name}}</option>
                    </select>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr ng-doeventrow="item" set-obj-rows="setObjRows(item)" tabindex="{{$index}}" ng-repeat="item in kendaraan.items | orderBy:orderByField:reverseSort" ng-class='{"success":$index == selectedRow}' ng-click='selectRow($index)'>
                <td >{{$index + 1}}</td>
                <td >{{item.pok_nopol}}</td>
                <td >{{item.po_name}}</td>
                <td >{{item.pok_nostuk}}</td>
                <td >{{item.pok_tgl_uji}}</td>
                <td >{{item.pok_merk}}</td>
                <td >{{item.pok_type}}</td>
                <td >{{item.ja_name}}</td>
                <td >{{item.user_entry}}</td>
                <td>{{item.date_entry | date: 'dd-MM-yyyy H:m:s'}}</td>
                <td>{{item.date_edit | date:'dd-MM-yyyy H:m:s'}}</td>
                <td width="150px">
                    <a   class="btn btn-success btn-xs" ng-click="editItem($index, item)" ><span class="glyphicon glyphicon-edit"></span>  Edit</a>
                    <a   class="btn btn-danger btn-xs" ng-click="removeItem($index, item)" ><span class="glyphicon glyphicon-floppy-remove"></span> Delete</a>
                </td>
            </tr>			
        </tbody>
        <tfoot>
            <tr>
                <td colspan="12" style="text-align:right;">
                    <ul class="pagination pagination-sm" style="margin:0 0 0 0;">
                        <li><a   ng-click="onFirstPage()">&#8249;&#8249;</a></li>
                        <li><a   ng-click="onPrevPage()">&#8249;</a></li>
                        <li><span>Page {{nextpage / stop + 1}} of {{max_page / stop + 1}}</span></li>
                        <li><a   ng-click="onNextPage()">&#8250;</a></li>
                        <li><a   ng-click="onLastPage()">&#8250;&#8250;</a></li>
                    </ul>
                </td>
            </tr>
        </tfoot>
    </table>

</div>
<script src="<?php echo base_url(); ?>assets/js/md5.js"></script>

