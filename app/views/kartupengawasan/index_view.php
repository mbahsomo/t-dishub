
<?php
$data['rec'] = $rec;
$data['stop'] = $stop;
$data['max_page'] = $max_page;
$this->load->view('kartupengawasan/controller', $data);
?>
<div id="pos_print"></div>
<div ng-controller="KartupengawasanCtrl">
    <script type="text/ng-template" id="customTemplate.html">
      <a>
          <span bind-html-unsafe="match.label | typeaheadHighlight:query"></span>
          <i>({{match.model.po_name}})</i>
      </a>
    </script>

    <script type="text/ng-template" id="customTemplateTrayek.html">
      <a>
          <span bind-html-unsafe="match.label | typeaheadHighlight:query"></span>
          <i>({{match.model.ty_name}})</i>
      </a>
    </script>

    <script type="text/ng-template" id="customTemplateSkid.html">
      <a>
          <span bind-html-unsafe="match.label | typeaheadHighlight:query"></span>
          <i>({{match.model.skid_code}})</i>
      </a>
    </script>

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Perpanjangan Kartu pengawasan</h3>
        </div>
        <div class="panel-body">
            <form action="" method="POST" class="form-horizontal" role="form">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="po_code" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('po_code'); ?></label>
                            <div class="col-sm-9 input-group input-group-sm">
                                <input type="text" class="form-control input-sm" placeholder="<?php echo $this->mdl->get_label('po_code'); ?>"
                                ng-model="dataPO"
                                typeahead="c as c.po_code for c in pos | filter:$viewValue | limitTo:10"
                                typeahead-min-length='1'
                                ng-change='onChangePart(dataPO)'
                                typeahead-on-select='onSelectPart($item, $model, $label)'
                                typeahead-template-url="customTemplate.html" >
                                <span class="input-group-addon">{{dataPO.po_name}}</span>
                            </div>

                        </div>
                        <div class="form-group">
                            <label for="po_code" class="col-sm-3 control-label">Alamat PO</label>
                            <div class="col-sm-9 input-group input-group-sm">
                                <span>{{dataPO.po_alamat}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ty_code" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('ty_code'); ?></label>
                            <div class="col-sm-9 input-group input-group-sm">
                                <input type="text" class="form-control input-sm" placeholder="<?php echo $this->mdl->get_label('ty_code'); ?>"
                                ng-model="dataTrayek"
                                typeahead="c as c.ty_code for c in trayeks | filter:$viewValue | limitTo:10"
                                typeahead-min-length='1'
                                ng-change='onChangePart(dataTrayek)'
                                typeahead-on-select='onSelectPart($item, $model, $label)'
                                typeahead-template-url="customTemplateTrayek.html" >
                                <span class="input-group-addon">{{dataTrayek.ty_name}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ty_code" class="col-sm-3 control-label">NO SKID</label>
                            <div class="col-sm-9 input-group input-group-sm">
                                <input type="text" class="form-control input-sm" placeholder="Cari PO"
                                ng-model="dataSkid"
                                typeahead="c as c.skid_code for c in skids | filter:$viewValue | limitTo:10"
                                typeahead-min-length='1'
                                ng-change='onChangePart(dataTrayek)'
                                typeahead-on-select='onSelectPart($item, $model, $label)'
                                typeahead-template-url="customTemplateSkid.html" >
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ty_code" class="col-sm-3 control-label">Periode</label>
                            <div class="col-sm-9 input-group input-group-sm">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <input readonly ui-mask="99/99/9999"  de-focus ng-model="dataSkid.skid_tgl_awal" type="text" class="form-control input-sm" id="skid_tgl" >        
                                    </div>
                                    <div class="col-sm-1">s/d</div>
                                    <div class="col-sm-4">
                                        <input readonly ui-mask="99/99/9999"  de-focus ng-model="dataSkid.skid_tgl_akhir" type="text" class="form-control input-sm" id="skid_tgl" >        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ty_code" class="col-sm-3 control-label">P2K</label>
                            <div class="col-sm-9 input-group input-group-sm">
                                <input readonly de-focus ng-model="dataSkid.skid_nosk" type="text" class="form-control input-sm" id="skid_tgl" >        
                                
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <table class="table table-bordered table-condensed table-hover">
                <thead>
                    <tr>
                        <th style="width:40px;text-align:center;"><input type="checkbox" ng-model="pilih"/></th>
                        <th style="width:40px;text-align:center;">#</th>
                        <th >No POL</th>
                        <th >KD Trayek</th>
                        <th >Nama Trayek</th>
                        <th>Titip</th>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="item in kendaraans">
                        <td class="kolom-kiri">
                            <input type="checkbox" ng-model="item.pilih" />
                        </td>
                        <td class="kolom-kiri"><b>{{$index+1}}</b></td>
                        <td><b>{{item.pok_nopol}}</b></td>
                        <td>{{item.ty_code}}</td>
                        <td>{{item.ty_name}}</td>
                        <td class="kolom-kiri" >
                            {{item.proses}}
                            <input type="checkbox" ng-model="item.titip" ng-true-value="T" ng-false-value="F"/>                            
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="panel-footer">
            <div class="alert alert-info" ng-if="prosesstatus">
                Sedang Proses .....
            </div>

            <button ng-click="Proses()" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-cog"></span> Proses</button>
            <button class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-list-alt"></span> Print Preview</button>
            <button class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-print"></span> Print</button>
        </div>
    </div>
    
</div>
