
<script src="<?php echo base_url(); ?>assets/lib/mask/mask.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/auto-complite/ui-bootstrap-tpls-0.9.0.js"></script>

<script type="text/javascript">
    'use strict';
    var ngDishub = angular.module('ngDishub', [ 'ui.mask','ui.bootstrap']);
    ngDishub.controller('KartupengawasanCtrl', function($scope, $http, $sce, $document, $filter) {
        
        $scope.dataPO = {po_code:'',po_name :'',po_alamat:''};        
        $scope.pos = [] ;
        $scope.loadPo = function(){
            $http.get('<?php echo site_url() .'/po/get_all'; ?>' ).success(function(data) {
                if(data!=undefined){
                    $scope.pos = data.rec;
                }
            });
        };
        $scope.loadPo();

        $scope.dataTrayek = {ty_code:'',ty_name :''};
        $scope.trayeks = [] ;
        $scope.loadTrayek = function(po){
            $http.get('<?php echo site_url() .'/po_kendaraan/get_perpo/'; ?>' + po ).success(function(data) {
                if(data!=undefined){
                    $scope.trayeks = data.rec;
                }
            });
        };

        $scope.dataSkid = {skid_code:'',ty_name :'', skid_tgl_awal : '', skid_tgl_akhir : '', skid_nosk:''};
        $scope.skids = [] ;
        $scope.loadSkid = function(po,ty){
            $http.get('<?php echo site_url() .'/skid/get_skid_pertryake'; ?>/' + po + '/' +ty).success(function(data) {
                if(data!=undefined){
                    $scope.skids = data.rec;
                }
            });
        };
        
        $scope.kendaraans = [];
        $scope.loadKendaraan = function(po,ty){
            $http.get('<?php echo site_url() .'/po_kendaraan/get_perpoty'; ?>/' + po + '/' +ty).success(function(data) {
                if(data!=undefined){
                    $scope.kendaraans = data.rec;
                }
            });
        };

        //Data PO
        $scope.$watch('dataPO.po_code', function (newVal, oldVal) {
            if (newVal !== oldVal ) {
                $scope.loadTrayek(newVal);
            }
        }, true);

        $scope.$watch('dataTrayek.ty_code', function (newVal, oldVal) {
            if (newVal !== oldVal ) {
                $scope.loadSkid($scope.dataPO.po_code,newVal);
                $scope.loadKendaraan($scope.dataPO.po_code,newVal);
            }
        }, true);

        $scope.$watch('dataSkid', function (newVal, oldVal) {
            if (newVal !== oldVal ) {
                $scope.dataSkid.skid_tgl_awal = $filter('date')($scope.dataSkid.skid_tgl_awal, 'dd/MM/yyyy');
                $scope.dataSkid.skid_tgl_akhir = $filter('date')($scope.dataSkid.skid_tgl_akhir, 'dd/MM/yyyy');
            }
        }, true);

        $scope.$watch(
            "pilih",
            function( newValue, oldValue ) {
                if (newValue!==oldValue){
                    angular.forEach($scope.kendaraans, function(value, key){
                        value.pilih = newValue;
                    });
                }
            }
        );

        $scope.prosesstatus = false;
        //Proses simpan data
        $scope.Proses = function(){
            $scope.prosesstatus = true;
            angular.forEach($scope.kendaraans, function(value, key){
                console.log(value);
                value.proses = "Proses...";
            });
            $scope.prosesstatus = false;
        }
    });
</script>