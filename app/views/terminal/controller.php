<script src="<?php echo base_url(); ?>assets/lib/doevent/do-event-input.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.grid.js"></script>
<script type="text/javascript">
	'use strict';
	var ngDishub = angular.module('ngDishub', ['ngCsv','ngDoeventrow']);
	ngDishub.controller('TerminalCtrl', function($scope, $http, $sce, $document) {
		$scope.errorshow=false;
		$scope.pesanerror="";
		$scope.orderByField = 't_code';
        $scope.reverseSort = false;
		
		$scope.stop = <?php echo isset($stop)?$stop:5; ?>;
		$scope.max_page = <?php echo isset($max_page)?$max_page:5; ?>;
		$scope.nextpage = 0;
		$scope.statuss = [
			{value:'A',name:'Antar Kota'},
			{value:'D',name:'Dalam Kota'}
		];
		

		$scope.edit = -1 ;
		$scope.fieldcaris = [{name:'Kode',value:'t_code'},{name:'Nama',value:'t_name'},{name:'Alamat',value:'a_alamat'}];
		$scope.fieldcari = $scope.fieldcaris[0];
		$scope.txtcari = '';
		$scope.kotas = <?php echo count($kota)>0? json_encode($kota):"[]";?>;
		$scope.obj = {t_code:'',k_code:'',t_name:'',t_alamat:'',t_type:'A'};
		$scope.Terminal = { items : <?php echo count($rec)>0? json_encode($rec):"[]";?>};
		
		$scope.showErrorMessage = function() {
			return $sce.trustAsHtml($scope.pesanerror);
		};
		//Add data
		$scope.showAdd = function (){
        	$scope.edit = -1 ;
        	$scope.obj = {t_code:'',k_code:'',t_name:'',t_alamat:'',t_type:'A'};
        	$scope.errorshow=false;
        	$('#win-Terminal').modal('show');
        }

        $scope.addItem = function() {
        	var evt = "";
        	if ($scope.edit == -1 ){
		        evt = "insert_data";
		    }else{
		    	evt = "edit_data" ;
		    }
			$http.get('<?php echo site_url() .'/terminal/'; ?>' + evt, {
				params: {
					t_code : $scope.obj.t_code,
					k_code : $scope.obj.k_code,
                    t_name : $scope.obj.t_name,
                    t_alamat : $scope.obj.t_alamat,
					t_type : $scope.obj.t_type
				}
			}).
			success(function(data) {
				if(data!=undefined){
					if(data.success==true){
						$scope.obj.user_entry = '<?php echo $this->session->userdata('user_name'); ?>';
                        $scope.obj.date_edit = new Date();
						if ($scope.edit == -1 ){
                            $scope.obj.date_entry = new Date();
							$scope.Terminal.items.push($scope.obj);
						}else{
							$scope.Terminal.items[$scope.edit] = $scope.obj;
						}
               			$scope.errorshow=false;
						$('#win-Terminal').modal('hide');
                	}else{
                		//$('#msg-error').html(data.msg);
						$scope.pesanerror=data.msg;
                		$scope.errorshow=true;                		
                	}
				}
			});	    		
	    	
	    }
	    //Edit data
	    $scope.editItem = function(index, item){
	    	$scope.edit = index ;
	    	$scope.obj = item;
			//$scope.obj.level = item.u_level;
	    	$('#win-Terminal').modal('show');
	    }

		//Hapus data
		$scope.removeItem = function(index, item) {

	    	if(confirm('Anda yakin HAPUS data ini? ')){
				$http.get('<?php echo site_url() .'/terminal/delete_data/'; ?>', {
					params: {
						't_code' : item.t_code
					}
				}).
				success(function(data) {
					if(data!=undefined){
						$scope.Terminal.items.splice(index, 1);
						//$scope.max_page = data.max_page;
					}
				});	    		
	    	}
	    }
		
		$scope.search = function (){
			$http.get('<?php echo site_url() .'/terminal/search/'; ?>', {
				params: {
					'field' : $scope.fieldcari.value,
					'value' : $scope.txtcari,
					'stop'  : $scope.nextpage
				}
			}).
			success(function(data) {
				if(data!=undefined){
					$scope.Terminal = { items : data.rec};
					$scope.max_page = data.max_page;
					if($scope.max_page<$scope.nextpage){
						$scope.nextpage=0;
					}
				}
			})
		}

		$scope.onKeydown= function(ev){
			$scope.pressed = ev.which;
			if (ev.which==13 || ev.which==9)
	            $scope.search();
		}

		$scope.onFirstPage = function(){
			if ($scope.nextpage  > 0 ){
				$scope.nextpage = 0;
				$scope.search();
			}
		}

		$scope.onNextPage = function(){
			if (($scope.nextpage + $scope.stop) <= $scope.max_page ){
				$scope.nextpage = $scope.nextpage+$scope.stop;
				//console.log($scope.nextpage);
				$scope.search();
			}
		}

		$scope.onPrevPage = function(){
			if ($scope.nextpage  >0 ){
				$scope.nextpage = $scope.nextpage-$scope.stop;
				$scope.search();
			}
		}		

		$scope.onLastPage = function(){
			if ($scope.nextpage  < $scope.max_page ){
				$scope.nextpage = $scope.max_page;
				$scope.search();
			}
		}

		$scope.selectRow = function(row) {
          $scope.selectedRow = row;
        };

        $scope.cetak = function (){
			print_preview('<?php echo site_url(). '/'. $this->fn .'/cetak/'; ?>','');
		}

		$document.bind('keydown', function(event) {
			console.debug(event.which);
			switch (event.which)
            {
                case 65:
                    if(event.altKey){
                        $scope.showAdd();
                        return false;
                    }
                    break;
                case 80 :
                	if(event.altKey){
                        $scope.cetak();
                        return false;
                    }
                	break;
            }
    	});
		
	}).directive('deFocus', deEvent);
</script>