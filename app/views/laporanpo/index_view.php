<link href="<?php echo base_url(); ?>assets/lib/date-picker/date.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/lib/date-picker/date.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/date-picker/m-date.js"></script>
<script>
    'use strict';
    var ngDishub = angular.module('ngDishub', ['ngCsv']);
    ngDishub.controller('LaporanPOCtrl', function($scope, $http, $sce, $document, $filter) {
        $scope.tgl1='<?php echo date('Y-m-d');?>';
        $scope.tgl2='<?php echo date('Y-m-d');?>';
        $scope.bln='<?php echo date('m');?>';
        $scope.bln_tahun="<?php echo date('Y');?>";
        $scope.tahun="<?php echo date('Y');?>";
        //$scope.tahuns = [{id:2012,val:2012},{id:2013,val:2013},{id:2014,val:2014},{id:2015,val:2015},{id:2016,val:2016},{id:2017,val:2017}];
        $scope.cetakPerTanggal = function(){
            if ($scope.tgl1!='' && $scope.tgl2!=''){
                print_preview('<?php echo site_url(). '/'. $this->fn .'/cetakpertanggal/'; ?>'+$scope.tgl1+'/'+$scope.tgl2,'');
            }else{
                alert('Isi Tanggal Dulu');
            }
        }

        $scope.exportPerTanggal = function(){
            var urlprint = "<?php echo site_url(). '/'. $this->fn .'/exportpertanggal/'; ?>" +$scope.tgl1+'/'+$scope.tgl2;
            var win = window.open(urlprint,'Export','height=255,width=250,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,top=20,left=20');
            setInterval(
                function(){
                    win.close();
                    return false;
                },
                5000
            );
        }

        $scope.cetakPerBulan = function(){
            if ($scope.bln!='' && $scope.blntahun!=''){
                print_preview('<?php echo site_url(). '/'. $this->fn .'/cetakperbulan/'; ?>'+$scope.bln+'/'+$scope.bln_tahun,'');
            }else{
                alert('Isi Bulan Dulu');
            }
        }

        $scope.exportPerBulan = function(){
            var urlprint = "<?php echo site_url(). '/'. $this->fn .'/exportperbulan/'; ?>" +$scope.bln+'/'+$scope.bln_tahun;
            var win = window.open(urlprint,'Export','height=255,width=250,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,top=20,left=20');
            setInterval(
                function(){
                    win.close();
                    return false;
                },
                7000
            );
        }

        $scope.cetakPerTahun = function(){
            if ($scope.tahun!=''){
                print_preview('<?php echo site_url(). '/'. $this->fn .'/cetakpertahun/'; ?>'+$scope.tahun ,'');
            }else{
                alert('Isi tahun Dulu');
            }
        }

        $scope.exportPerTahun = function(){
            var urlprint = "<?php echo site_url(). '/'. $this->fn .'/exportpertahun/'; ?>" +$scope.tahun;
            var win = window.open(urlprint,'Export','height=255,width=250,toolbar=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,top=20,left=20');
            setInterval(
                function(){
                    win.close();
                    return false;
                },
                10000
            );
        }

    }).directive('mDatepicker', mDate)
    .directive('deFocus', deEvent);


    $('.nav-tabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

</script>
<ul class="nav nav-tabs" >
    <li class="active"><a href="#pertanggal" data-toggle="tab">Pertanggal</a></li>
    <li class=""><a href="#perbulan" data-toggle="tab">Perbulan</a></li>
    <li class=""><a href="#pertahun" data-toggle="tab">Pertahun</a></li>
</ul>
<div ng-controller="LaporanPOCtrl">
    <div id='content' class="tab-content" style="padding: 10px 10px 10px 10px; border-left: 1px solid #ddd; border-right:
    1px
    solid #ddd;
    border-bottom: 1px solid #ddd;
    margin-bottom: 4px; ">
        <div class="tab-pane active" id="pertanggal">
            <div class="row">
                <div class="col-sm-3  input-group input-group-sm">
                    <input m-datepicker de-focus ng-model="tgl1" type="text" class="form-control input-sm" id="po_tgl_berdiri" placeholder="Pilih tanggal 1">
                    <a  href="" class="input-group-addon btn btn-default btn-sm" data-toggle="datepicker"><span class="glyphicon glyphicon-calendar"></span></a>
                </div>
                <div  style="padding:0 0 0 0; float:left;">s/d</div>
                <div class="col-sm-3  input-group input-group-sm">
                    <input m-datepicker de-focus ng-model="tgl2" type="text" class="form-control input-sm" id="po_tgl_berdiri" placeholder="Pilih tanggal 1">
                    <a  href="" class="input-group-addon btn btn-default btn-sm" data-toggle="datepicker"><span class="glyphicon glyphicon-calendar"></span></a>
                </div>                
            </div>
            <div class="row" style="margin-top:10px;">
                <div class="col-sm-6">
                    <a href="" ng-click="cetakPerTanggal();" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print</a>
                    <a href="" ng-click="exportPerTanggal();" class="btn btn-default"><span class="glyphicon glyphicon-export"></span> Export xls</a>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="perbulan">
            <div class="row">
                <div class="col-sm-4 ">
                    <select ng-model="bln"  class="form-control input-sm" style="width: 70%; float:left;">
                        <?php foreach ($bulan as $key => $value) {?>
                        <option value="<?php echo $key ;?>"><?php echo $value ;?></option >
                        <?php } ?>
                    </select>
                    <select ng-model="bln_tahun" class="form-control input-sm"  style="width: 30%;">
                        <?php for ($a = 2011 ; $a<2050; $a++) {?>
                        <option value="<?php echo $a; ?>"><?php echo $a; ?></option>
                        <?php } ?>
                    </select>
                </div>                
            </div>
            <div class="row" style="margin-top:10px;">
                <div class="col-sm-6">
                    <a href="" ng-click="cetakPerBulan();" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print</a>
                    <a href="" ng-click="exportPerBulan();" class="btn btn-default"><span class="glyphicon glyphicon-export"></span> Export xls</a>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="pertahun">
            <div class="row">
                <div class="col-sm-2 ">
                    <select ng-model="tahun" class="form-control input-sm">
                        <?php for ($a = 2011 ; $a<2050; $a++) {?>
                        <option value="<?php echo $a; ?>"><?php echo $a; ?></option>
                        <?php } ?>
                    </select>
                </div>                
            </div>
            <div class="row" style="margin-top:10px;">
                <div class="col-sm-6">
                    <a href="" ng-click="cetakPerTahun();" class="btn btn-default"><span class="glyphicon glyphicon-print"></span> Print</a>
                    <a href="" ng-click="exportPerTahun();" class="btn btn-default"><span class="glyphicon glyphicon-export"></span> Export xls</a>
                </div>
            </div>
        </div>
    </div>    
</div>