<script src="<?php echo base_url(); ?>assets/lib/doevent/do-event-input.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.grid.js"></script>
<script type="text/javascript">
	'use strict';
	var ngDishub = angular.module('ngDishub', ['ngCsv','ngDoeventrow']);
	ngDishub.directive('focus', function($timeout, $parse) {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				scope.$watch(attrs.focus, function(newValue, oldValue) {
					if (newValue) { element[0].focus(); }
				});
				element.bind("blur", function(e) {
					$timeout(function() {
						scope.$apply(attrs.focus + "=false"); 
					}, 0);
				});
				element.bind("focus", function(e) {
					$timeout(function() {
						scope.$apply(attrs.focus + "=true");
					}, 0);
				})
			}
		}
	});
	ngDishub.controller('KotaCtrl', function($scope, $http, $sce, $document) {
		$scope.errorshow=false;
		$scope.pesanerror="";
		$scope.orderByField = 'k_code';
        $scope.reverseSort = false;
		
		$scope.stop = <?php echo isset($stop)?$stop:5; ?>;
		$scope.max_page = <?php echo isset($max_page)?$max_page:5; ?>;
		$scope.nextpage = 0;

		$scope.edit = -1 ;
		$scope.fieldcaris = [{name:'Kode',value:'k_code'},{name:'Nama',value:'k_name'},{name:'Keterangan',value:'k_keterangan'},{name:'Region',value:'k_region'}];
		$scope.fieldcari = $scope.fieldcaris[0];
		$scope.txtcari = '';
		$scope.propinsis = <?php echo count($propinsi)>0? json_encode($propinsi):"[]";?>;
		$scope.obj = {k_code:'',k_codename:'',k_keterangan:'',k_region:0,k_status:true, p_id : 0};
		$scope.kota = { items : <?php echo count($rec)>0? json_encode($rec):"[]";?>};
		
		$scope.showErrorMessage = function() {
			return $sce.trustAsHtml($scope.pesanerror);
		};
		//Add data
		$scope.showAdd = function (){
        	$scope.edit = -1 ;
        	$scope.obj = {k_code:'',k_codename:'',k_keterangan:'',k_region:0,k_status:true};
        	$scope.errorshow=false;
        	$('#win-kota').modal('show');
        }

        $scope.addItem = function() {
        	var evt = "";
        	if ($scope.edit == -1 ){
		        evt = "insert_data";
		    }else{
		    	evt = "edit_data" ;
		    }
			$http.get('<?php echo site_url() .'/kota/'; ?>' + evt, {
				params: {
					k_code : $scope.obj.k_code,
                    k_name : $scope.obj.k_name,
                    p_id : $scope.obj.p_id,
					k_keterangan : $scope.obj.k_keterangan,
					k_region : $scope.obj.k_region,
					k_status : $scope.obj.k_status
				}
			}).
			success(function(data) {
				if(data!=undefined){
					if(data.success==true){
						$scope.obj.user_entry = '<?php echo $this->session->userdata('user_name'); ?>';
                        $scope.obj.date_edit = new Date();
						if ($scope.edit == -1 ){
                            $scope.obj.date_entry = new Date();
							$scope.kota.items.push($scope.obj);
						}else{
							$scope.kota.items[$scope.edit] = $scope.obj;
						}
               			$scope.errorshow=false;
						$('#win-kota').modal('hide');
                	}else{
                		//$('#msg-error').html(data.msg);
						$scope.pesanerror=data.msg;
                		$scope.errorshow=true;                		
                	}
				}
			});	    		
	    	
	    }
	    //Edit data
	    $scope.editItem = function(index, item){
	    	$scope.edit = index ;
	    	$scope.obj = item;
			//$scope.obj.level = item.u_level;
	    	$('#win-kota').modal('show');
	    }

		//Hapus data
		$scope.removeItem = function(index, item) {

	    	if(confirm('Anda yakin HAPUS data ini? ')){
				$http.get('<?php echo site_url() .'/kota/delete_data/'; ?>', {
					params: {
						'k_code' :  item.k_code
					}
				}).
				success(function(data) {
					if(data!=undefined){
						$scope.kota.items.splice(index, 1);
						//$scope.max_page = data.max_page;
					}
				});	    		
	    	}
	    }
		
		$scope.search = function (){
			$http.get('<?php echo site_url() .'/kota/search/'; ?>', {
				params: {
					'field' : $scope.fieldcari.value,
					'value' : $scope.txtcari,
					'stop'  : $scope.nextpage
				}
			}).
			success(function(data) {
				if(data!=undefined){
					$scope.kota = { items : data.rec};
					$scope.max_page = data.max_page;
					if($scope.max_page<$scope.nextpage){
						$scope.nextpage=0;
					}
				}
			})
		}

		$scope.setStatus = function(sts){
			if (sts==0){
				return 'NA';
			}else{
				return 'Aktif';
			}
		}

		$scope.onKeydown= function(ev){
			$scope.pressed = ev.which;
			if (ev.which==13 || ev.which==9)
	            $scope.search();

		}

		$scope.onFirstPage = function(){
			if ($scope.nextpage  > 0 ){
				$scope.nextpage = 0;
				$scope.search();
			}
		}

		$scope.onNextPage = function(){
			if (($scope.nextpage + $scope.stop) <= $scope.max_page ){
				$scope.nextpage = $scope.nextpage+$scope.stop;
				//console.log($scope.nextpage);
				$scope.search();
			}
		}

		$scope.onPrevPage = function(){
			if ($scope.nextpage  >0 ){
				$scope.nextpage = $scope.nextpage-$scope.stop;
				$scope.search();
			}
		}		

		$scope.onLastPage = function(){
			if ($scope.nextpage  < $scope.max_page ){
				$scope.nextpage = $scope.max_page;
				$scope.search();
			}
		}

		$scope.selectRow = function(row) {
          $scope.selectedRow = row;
        };

        $scope.cetak = function (){
			print_preview('<?php echo site_url(). '/'. $this->fn .'/cetak/'; ?>','');
		}

		$document.bind('keydown', function(event) {
			console.debug(event.which);
			switch (event.which)
            {
                case 65:
                    if(event.altKey){
                        $scope.showAdd();
                        return false;
                    }
                    break;
                case 80 :
                	if(event.altKey){
                        $scope.cetak();
                        return false;
                    }
                	break;
            }
    	});
		
	}).directive('deFocus', deEvent);
</script>