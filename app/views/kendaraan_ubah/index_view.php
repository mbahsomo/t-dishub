<script src="<?php echo base_url(); ?>assets/lib/date-picker/m-date.js"></script>
<?php
$data['rec'] = $rec;
$data['stop'] = $stop;
$data['max_page'] = $max_page;
$this->load->view('kendaraan_ubah/controller', $data);
?>
<div ng-controller="KendaraanubahCtrl">
    <!--Untuk STUK-->
    <!--Windows Modal-->
	<div class="modal fade" id="win-kendaraanstuk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add / Edit Data</h4>
				</div>
				<div class="modal-body">

					<div ng-show="errorshow" class="alert alert-danger" ng-bind-html='showErrorMessage()'></div>
					<input type="hidden" ng-model="obj.poks_id" >
					<div class="form-horizontal" role="form">
						<div class="form-group">
						   	<label for="poks_date" class="col-sm-3 control-label">Tgl</label>
						    <div class="col-sm-4 input-group input-group-sm">
                                <input m-datepicker de-focus ng-model="objstuk.poks_date" type="text" class="form-control input-sm" id="poks_date" >
                                <a  href="" class="input-group-addon btn btn-default btn-sm" data-toggle="datepicker"><span class="glyphicon glyphicon-calendar"></span></a>
                            </div>
					  	</div>

					  	<div class="form-group">
						   	<label for="poks_nostuk" class="col-sm-3 control-label">NO STUK Baru</label>
						    <div class="col-sm-4">
								<input de-focus ng-model="objstuk.poks_nostuk" type="text" class="form-control input-sm" id="poks_nostuk" placeholder="NO STUK">
						    </div>
					  	</div>

					  	<div class="form-group">
						   	<label for="poks_exp" class="col-sm-3 control-label">Tgl EXp</label>
						    <div class="col-sm-4 input-group input-group-sm">
                                <input m-datepicker de-focus ng-model="objstuk.poks_exp" type="text" class="form-control input-sm" id="poks_exp" >
                                <a  href="" class="input-group-addon btn btn-default btn-sm" data-toggle="datepicker"><span class="glyphicon glyphicon-calendar"></span></a>
                            </div>
					  	</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary btn-sm" ng-click="addItemSTUK()" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
    <!--Akhir STUK-->
    <!--Untuk Help-->
    <div style="z-index:1041;" class="modal fade" id="win-helpPO" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog"  style="width:700px;">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Help</h4>
                </div>
                <div d-grid="options" style="width:668px;" >        
                    <div class="dGrid">
                        <div class="caption">Master Group</div>
                        <div class="thead" d-head>
                            <div class="tr">
                                <div class="th" style="width:90px;">NOPOL</div>
                                <div class="th" style="width:180px;">NAMA</div>
                                <div class="th" style="width:300px;">ALAMAT</div>
                                <div class="th" style="width:30px;"></div>
                            </div>
                            <div class="tr" >
                                <div class="th" >
                                    <input autofocus id="pok_nopol" ng-change="eventGrid.setSearch(find)" d-rec-find ng-model="find.pok_nopol" class="form-control input-sm" placeholder="Search" type="text" style="width: 100%">
                                </div>
                                <div class="th" >
                                    <input ng-change="eventGrid.setSearch(find)" d-rec-find ng-model="find.po_name" class="form-control input-sm" placeholder="Search" type="text" style="width: 100%">
                                </div>
                                <div class="th" >
                                    <input ng-change="eventGrid.setSearch(find)" d-rec-find ng-model="find.po_alamat" class="form-control input-sm" placeholder="Search" type="text" style="width: 100%">
                                </div>
                                <div class="th" style="width:30px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="setscroll" style="height: 200px;" >
                        <!-- <div class="dGrid"> -->
                        <div class="dGrid tbody">                        
                            <div ng-keydown="closePO($event, item)" d-row="{{$index}}" ngModel="item" tabindex="{{$index}}"  class="tr"
                                 ng-repeat="item in eventGrid.getRecords()| orderBy:orderByField:reverseSort" >
                                <div d-col class='td' >{{item.pok_nopol}}</div>
                                <div d-col class='td' >{{item.po_name}}</div>
                                <div d-col class='td' >{{item.po_alamat}}</div>
                                <div d-col class="td">
                                    <a href="" class="btn btn-success btn-xs" ng-click="closePO(0, item)" ><span class="glyphicon glyphicon-plus"></span></a>
                                </div>
                            </div>
                        </div>
                        <!-- </div> -->
                    </div>
                    <div class="tfoot">
                        <ul class="pagination pagination-sm " style="margin:0 0 0 0;">
                            <li><a ng-click="eventGrid.setFirstPages(find)">&#8249;&#8249;</a></li>
                            <li><a ng-click="eventGrid.setPrevPages(find)">&#8249;</a></li>
                            <li><span>{{eventGrid.getTitlePages()}}</span></li>
                            <li><a ng-click="eventGrid.setNextPages(find);">&#8250;</a></li>
                            <li><a ng-click="eventGrid.setLastPages(find)">&#8250;&#8250;</a></li>
                        </ul>
                    </div>
                </div>                
            </div>
        </div>
    </div>
    <!--Untuk Help-->
    <!--Windows Modal-->
    <div class="modal fade" id="win-kendaraanubah" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add / Edit Data</h4>
                </div>
                <div class="modal-body">

                    <div ng-show="errorshow" class="alert alert-danger" ng-bind-html='showErrorMessage()'></div>
                    <input type="hidden" ng-model="obj.poku_id" >
                    <div class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="pok_id" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('pok_id'); ?>(F4)</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-sm-4 input-group input-group-sm" style="padding-right:2px;">
                                        <input tabindex="0" id="pok_id" autofocus type="text" ng-keydown="chosePO($event);" de-focus ng-model='obj.pok_id' class="form-control input-sm" id="pok_id">
                                        <a ng-keydown="chosePO($event);" ng-click="showHelpPO()" href="" class="input-group-addon btn btn-default btn-sm" data-toggle="datepicker"><span class="glyphicon glyphicon-search"></span></a>
                                    </div>
                                    <div class="col-sm-8" style="padding-left:0px;">
                                        <input de-focus ng-model="obj.pok_nopol" type="text" class="form-control input-sm" readonly ng-keydown="chosePO($event);">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="poku_date" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('poku_date'); ?></label>
                            <div class="col-sm-4 input-group input-group-sm">
                                <input m-datepicker de-focus ng-model="obj.poku_date" type="text" class="form-control input-sm" id="poku_date" placeholder="<?php echo $this->mdl->get_label('poku_date'); ?>">
                                <a  href="" class="input-group-addon btn btn-default btn-sm" data-toggle="datepicker"><span class="glyphicon glyphicon-calendar"></span></a>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="poku_nopol_lama" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('poku_nopol_lama'); ?></label>
                            <div class="col-sm-9">
                                <input de-focus ng-model="obj.poku_nopol_lama" type="text" readonly class="form-control input-sm" id="poku_nopol_lama" placeholder="<?php echo $this->mdl->get_label('poku_nopol_lama'); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="poku_nopol_baru" class="col-sm-3 control-label"><?php echo $this->mdl->get_label('poku_nopol_baru'); ?></label>
                            <div class="col-sm-9">
                                <input de-focus ng-model="obj.poku_nopol_baru" type="text" class="form-control input-sm" id="poku_nopol_baru" placeholder="<?php echo $this->mdl->get_label('poku_nopol_baru'); ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" ng-click="addItem()" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <?php //$this->load->view('template/toolbar_grid'); ?>

    <form class="navbar-form" style="padding-left:0;">
        <div class="row">
            <div class="col-md-1" style="text-align: right;">
                Trayek
            </div>
            <div class="col-md-3">
                <select ng-model="txtcaritrayek" class="form-control input-sm">
                    <option value="">Pilih Trayek</option>
                    <option ng-repeat="ty in trayeks" value="{{ty.ty_code}}">{{ty.ty_name}}</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-md-1" style="text-align: right;">
                Nama PO
            </div>
            <div class="col-md-3">
                <select ng-model="txtcaripo" class="form-control input-sm">
                    <option value="">Pilih PO</option>
                    <option ng-repeat="it in pos" value="{{it.po_code}}">{{it.po_name}}</option>
                </select>
            </div>
        </div>
        
        <button type="button" class="btn btn-success btn-sm" ng-click="search()"><span class="glyphicon glyphicon-search"></span> Cari</button>
    </form>

    <!-- id="tbl-scroll" -->
    <table id="tbl-scroll" class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <td colspan="21" style="text-align:right;">
                    <ul class="pagination pagination-sm" style="margin:0 0 0 0;">
                        <li><a   ng-click="onFirstPage()">&#8249;&#8249;</a></li>
                        <li><a   ng-click="onPrevPage()">&#8249;</a></li>
                        <li><span>Page {{nextpage / stop + 1}} of {{max_page / stop + 1}}</span></li>
                        <li><a   ng-click="onNextPage()">&#8250;</a></li>
                        <li><a   ng-click="onLastPage()">&#8250;&#8250;</a></li>
                    </ul>
                </td>
            </tr>
            <tr>
                <th>No</th>
                <th>NOPOL</th>
                <th>NO KPS</th>
                <th>NO STRUK</th>
                <th>NO LAMBUNG</th>
                <th>MERK</th>
                <th width="270px">Event</th>
            </tr>
            <tr>
                <th></th>
                <th><input type='text' class='form-control input-sm' ng-model='cari.pok_nopol'></input></th>
                <th><input type='text' class='form-control input-sm' ng-model='cari.pok_nokps'></input></th>
                <th><input type='text' class='form-control input-sm' ng-model='cari.pok_nostuk'></input></th>
                <th><input type='text' class='form-control input-sm' ng-model='cari.pok_nolambung'></input></th>
                <th><input type='text' class='form-control input-sm' ng-model='cari.pok_merk'></input></th>
                <th ></th>
            </tr>
        </thead>
        <tbody>
            <tr ng-doeventrow="item" set-obj-rows="setObjRows(item)" tabindex="{{$index}}" ng-repeat="item in kendaraanubah.items | filter : cari" class="first" ng-class='{"success":$index == selectedRow}' ng-click='selectRow($index)'>
                <td>{{$index+1}}</td>
                <td>{{item.pok_nopol}}</td>
                <td>{{item.pok_nokps}}</td>
                <td>{{item.pok_nostuk}}</td>
                <td>{{item.pok_nolambung}}</td>
                <td>{{item.pok_merk}}</td>
                <td>
                    <a   class="btn btn-success btn-xs" ng-click="showAdd($index, item)" ><span class="glyphicon glyphicon-edit"></span> Ubah NOPOL</a>
                    <a   class="btn btn-success btn-xs" ng-click="addSTUK($index, item)" ><span class="glyphicon glyphicon-new-window"></span>  Add STUK</a>
                    <a   class="btn btn-danger btn-xs" ng-click="removeItem($index, item)" ><span class="glyphicon glyphicon-floppy-remove"></span> Delete</a>
                </td>
            </tr>			
        </tbody>
        <tfoot>
            <tr>
                <td colspan="21" style="text-align:right;">
                    <ul class="pagination pagination-sm" style="margin:0 0 0 0;">
                        <li><a   ng-click="onFirstPage()">&#8249;&#8249;</a></li>
                        <li><a   ng-click="onPrevPage()">&#8249;</a></li>
                        <li><span>Page {{nextpage / stop + 1}} of {{max_page / stop + 1}}</span></li>
                        <li><a   ng-click="onNextPage()">&#8250;</a></li>
                        <li><a   ng-click="onLastPage()">&#8250;&#8250;</a></li>
                    </ul>
                </td>
            </tr>
        </tfoot>
    </table>
</div>
<script src="<?php echo base_url(); ?>assets/js/md5.js"></script>
<link href="<?php echo base_url(); ?>assets/lib/date-picker/date.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/lib/date-picker/date.js"></script>