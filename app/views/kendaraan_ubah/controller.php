<script src="<?php echo base_url(); ?>assets/lib/doevent/do-event-input.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.grid.js"></script>
<!--Library Grid-->
<link href="<?php echo base_url(); ?>assets/lib/doeventgrid/css/style.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/lib/doeventgrid/js/doevent-grid.js"></script>
<script type="text/javascript">
    'use strict';
    var ngDishub = angular.module('ngDishub', ['ngCsv', 'ngDoeventrow', 'dGrid']);
    ngDishub.controller('KendaraanubahCtrl', function($scope, $http, $sce, $document, eventGrid) {
        //Untuk grid
        $scope.eventGrid = eventGrid;
        $scope.record = eventGrid.getRecord();
        $scope.options = {
            stop : <?php echo isset($stop) ? $stop : 5; ?>,
            nextpage : 0,
            urlPager : '<?php echo site_url() . '/po_kendaraan/search/'; ?>'
        };

        $scope.errorshow = false;
        $scope.pesanerror = "";

        $scope.stop = <?php echo isset($stop) ? $stop : 5; ?>;
        $scope.max_page = <?php echo isset($max_page) ? $max_page : 5; ?>;
        $scope.nextpage = 0;

        $scope.edit = -1;

        $scope.fieldcaris = [{name: 'KendaraanUbah Name', value: 'po_name'}];
        $scope.fieldcari = $scope.fieldcaris[0];
        $scope.txtcari = '';
        //$scope.kendaraans = items;
        $scope.obj = <?php echo json_encode($this->mdl->get_field_names()); ?>;
        //$scope.kendaraanubah = {items: <?php echo count($rec) > 0 ? json_encode($rec) : "[]"; ?>};
        $scope.kendaraanubah = {items: []};

        $scope.showErrorMessage = function() {
            return $sce.trustAsHtml($scope.pesanerror);
        };

        //setting autofocus
        angular.element('#win-kendaraanubah').on('shown.bs.modal', function() {
            angular.element('input#pok_id').focus();
        });
        angular.element('#win-helpPO').on('shown.bs.modal', function() {
            angular.element('input#pok_nopol').focus();
        });
        angular.element('#win-kendaraanstuk').on('shown.bs.modal', function() {
            angular.element('input#poks_date').focus();
        });

        //Help
        $scope.showHelpPO = function() {
            $('#win-helpPO').modal('show');
        }
        $scope.closePO = function(evt, item) {
            if (evt === 0) {
                $scope.obj.pok_id = item.pok_id;
                $scope.obj.pok_nopol = item.pok_nopol;
                $scope.obj.poku_nopol_lama = item.pok_nopol;
                angular.element('#win-helpPO').modal('hide');
                angular.element('input#pok_id').focus();
            } else {
                if (evt.which === 13) {
                    $scope.obj.pok_id = item.pok_id;
                    $scope.obj.pok_nopol = item.pok_nopol;
                    $scope.obj.poku_nopol_lama = item.pok_nopol;
                    angular.element('#win-helpPO').modal('hide');
                    angular.element('input#pok_id').focus();
                }
            }
        }

        $scope.chosePO = function(ev) {
            $scope.pressed = ev.which;
            //console.log(ev.which);
            if (ev.which == 115) {
                $scope.showHelpPO();
                return false;
            }
        }

        //
        $scope.setNoLama = function() {
            console.log(angular.element('#pok_id').text());
            $scope.obj.poku_nopol_lama = angular.element('#pok_id').text().trim();
        }

        //Add data
        $scope.showAdd = function(index, item) {
            console.log(item);
            $scope.edit = -1;
            $scope.obj = {pok_nopol:item.pok_nopol,pok_id:item.pok_id, poku_nopol_lama:item.pok_nopol,poku_date:'<?php echo date('Y-m-d')?>'};
            $scope.errorshow = false;
            $('#win-kendaraanubah').modal('show');
        }

        $scope.addItem = function() {
            var evt = "";
            if ($scope.edit == -1) {
                evt = "insert_data";
            } else {
                evt = "edit_data";
            }
            var vparams = [];
            angular.forEach($scope.obj, function(value, key) {
                if (key != '$$hashKey')
                    vparams[key] = value;
            });

            $http.get('<?php echo site_url() . '/kendaraan_ubah/'; ?>' + evt, {
                params: vparams
            }).
            success(function(data) {
                if (data != undefined) {
                    if (data.success == true) {
                        $scope.obj.user_entry = '<?php echo $this->session->userdata('user_name'); ?>';
                        $scope.obj.date_edit = new Date();
                        if ($scope.edit == -1) {
                            $scope.obj.date_entry = new Date();
                            $scope.obj.poku_id = data.key;
                            $scope.kendaraanubah.items.push($scope.obj);
                        } else {
                            $scope.kendaraanubah.items[$scope.edit] = $scope.obj;
                        }
                        $scope.errorshow = false;
                        $('#win-kendaraanubah').modal('hide');
                    } else {
                        //$('#msg-error').html(data.msg);
                        $scope.pesanerror = data.msg;
                        $scope.errorshow = true;
                    }
                }
            });

        }
        
        $scope.addItemSTUK = function() {
        	var vparams = [];
            angular.forEach($scope.objstuk, function(value, key){
                if(key !== '$$hashKey')
                    vparams[key] = value;
            });

			$http.get('<?php echo site_url() .'/kendaraan_stuk/insert_data'; ?>' , {
				params: vparams
			}).
			success(function(data) {
				if(data!==undefined){
					if(data.success===true){
						$scope.objstuk.user_entry = '<?php echo $this->session->userdata('user_name'); ?>';
                        $scope.objstuk.date_edit = new Date();
						$scope.errorshow=false;
						$('#win-kendaraanstuk').modal('hide');
                	}else{
                		//$('#msg-error').html(data.msg);
						$scope.pesanerror=data.msg;
                		$scope.errorshow=true;                		
                	}
				}
			});	    		
	    	
	    };
        $scope.objstuk = {};
        $scope.addSTUK = function (index, item){
            $scope.objstuk.pok_id = item.pok_id;
            $scope.objstuk.poks_date = '<?php echo date('Y-m-d'); ?>';
            $scope.objstuk.poks_exp = '<?php echo date('Y-m-d'); ?>';
            $('#win-kendaraanstuk').modal('show');
        };
        
        //Edit data
        $scope.editItem = function(index, item) {
            $scope.edit = index;
            $scope.obj = item;
            $scope.obj.pok_nopol = item.poku_nopol_lama;
            $('#win-kendaraanubah').modal('show');
        }

        //Hapus data
        $scope.removeItem = function(index, item) {

            if (confirm('Anda yakin HAPUS data ini? ')) {
                $http.get('<?php echo site_url() . '/kendaraan_ubah/delete_data/'; ?>', {
                    params: {
                        'poku_id': item.poku_id
                    }
                }).
                success(function(data) {
                    if (data != undefined) {
                        $scope.kendaraanubah.items.splice(index, 1);
                        //$scope.max_page = data.max_page;
                    }
                });
            }
        }
        
        $scope.txtcaritrayek = '';
        $scope.txtcaripo = '';
        $scope.search = function() {
            $http.get('<?php echo site_url() . '/po_kendaraan/search/'; ?>', {
                params: {
                    'field': 'ty_code;po_code',
                    'value': $scope.txtcaritrayek + ';' + $scope.txtcaripo,
                    'stop': $scope.nextpage
                }
            }).
            success(function(data) {
                if (data != undefined) {
                    $scope.kendaraanubah = {items: data.rec};
                    $scope.max_page = data.max_page;
                    if ($scope.max_page < $scope.nextpage) {
                        $scope.nextpage = 0;
                    }
                }
            })
        }

        $scope.onKeydown = function(ev) {
            $scope.pressed = ev.which;
            if (ev.which == 13 || ev.which == 9)
                $scope.search();
        }

        $scope.onFirstPage = function() {
            if ($scope.nextpage > 0) {
                $scope.nextpage = 0;
                $scope.search();
            }
        }

        $scope.onNextPage = function() {
            if (($scope.nextpage + $scope.stop) <= $scope.max_page) {
                $scope.nextpage = $scope.nextpage + $scope.stop;
                //console.log($scope.nextpage);
                $scope.search();
            }
        }

        $scope.onPrevPage = function() {
            if ($scope.nextpage > 0) {
                $scope.nextpage = $scope.nextpage - $scope.stop;
                $scope.search();
            }
        }

        $scope.onLastPage = function() {
            if ($scope.nextpage < $scope.max_page) {
                $scope.nextpage = $scope.max_page;
                $scope.search();
            }
        }

        $scope.selectRow = function(row) {
            $scope.selectedRow = row;
        };

        $scope.cetak = function() {
            print_preview('<?php echo site_url() . '/' . $this->fn . '/cetak/'; ?>', '');
        }

        $document.bind('keydown', function(event) {
            switch (event.which)
            {
                case 65:
                    if (event.altKey) {
                        $scope.showAdd();
                        return false;
                    }
                    break;
                case 80 :
                    if (event.altKey) {
                        $scope.cetak();
                        return false;
                    }
                    break;
            }
        });
        
        //Untuk seracing
        $scope.trayeks = [];
        $scope.getTrayek = function(){
            $http.get('<?php echo site_url() . '/trayek/get_all/' . $this->session->userdata('hashkey'); ?>' ).
            success(function(data) {
                if (data != undefined) {
                    $scope.trayeks = data.rec;
                }
            });
        }
        $scope.getTrayek();
        
        $scope.pos = [];
        $scope.getPo = function(){
            $http.get('<?php echo site_url() . '/po/get_all/' . $this->session->userdata('hashkey'); ?>' ).
            success(function(data) {
                if (data != undefined) {
                    $scope.pos = data.rec;
                }
            });
        }
        $scope.getPo();

    }).directive('mDatepicker', mDate).directive('deFocus', deEvent);
</script>