<?php  
	$data['rec'] = $rec;
	$data['stop'] = $stop;
	$data['max_page'] = $max_page;
	$this->load->view('pejabat/controller', $data);
?>
<div ng-controller="PejabatCtrl">
	<!--Windows Modal-->
	<div class="modal fade" id="win-pejabat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add / Edit Data</h4>
				</div>
				<div class="modal-body">

					<div ng-show="errorshow" class="alert alert-danger" ng-bind-html='showErrorMessage()'></div>

					<div class="form-horizontal" role="form">
						<div class="form-group">
						   	<label for="p_nip" class="col-sm-3 control-label">NIP</label>
						    <div class="col-sm-9 input-group input-group-sm">
								<span class="input-group-addon">@</span>
						      	<input de-center="setcenter" de-focus ng-model="obj.p_nip" type="text" class="form-control input-sm" id="p_nip" placeholder="Nama Pejabat">
						    </div>
					  	</div>

						<div class="form-group">
						   	<label for="p_name" class="col-sm-3 control-label">Pejabat Name</label>
						    <div class="col-sm-9 input-group input-group-sm">
								<span class="input-group-addon">@</span>
						      	<input de-focus ng-model="obj.p_name" type="text" class="form-control input-sm" id="p_name" placeholder="Nama Pejabat">
						    </div>
					  	</div>

					  	<div class="form-group">
						   	<label for="p_pangkat" class="col-sm-3 control-label">Pangkat</label>
						    <div class="col-sm-9">
								<input de-focus ng-model="obj.p_pangkat" type="text" class="form-control input-sm" id="p_pangkat" placeholder="Pangkat">								
						    </div>
					  	</div>

					  	<div class="form-group">
						   	<label for="p_jabatan" class="col-sm-3 control-label">Jabatan</label>
						    <div class="col-sm-9">
						      	<input de-focus ng-model="obj.p_jabatan" type="text" class="form-control input-sm" id="p_jabatan" placeholder="Masukkan Jabatan">
						    </div>
					  	</div>
					  	<input type="hidden" ng-model="obj.p_id" />
					  	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary btn-sm" ng-click="addItem()" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<?php $this->load->view('template/toolbar_grid'); ?>
	
	<form class="navbar-form" style="padding-left:0;">
		<div class="form-group">
			<select class="form-control input-sm" ng-model="fieldcari" ng-options="c.name for c in fieldcaris"></select>
		</div>
			<div class="form-group">
				<input type="text" placeholder="Data yang dicari" class="form-control input-sm" ng-model="txtcari" ng-keydown="onKeydown($event)"/>
			</div>
		<button type="button" class="btn btn-success btn-sm" ng-click="search()"><span class="glyphicon glyphicon-search"></span> Cari</button>
	</form>
        
	<!-- id="tbl-scroll" -->
	<table class="table table-bordered table-striped table-hover">
		<thead>
			<td>No</td>
			<?php
            foreach ($this->mdl->get_rule() as $val) {
                if (isset($val['grid'])) {
                    if ($val['grid']){ ?>
                        <th>
                            <a   ng-click="orderByField='<?php echo $val['field']; ?>'; reverseSort = !reverseSort">
                                <?php echo $this->mdl->get_label($val['field']); ?>
                                <span ng-show="orderByField == '<?php echo $val['field']; ?>'">
                                    <span ng-show="!reverseSort" class="glyphicon glyphicon-chevron-up"></span>
                                    <span ng-show="reverseSort" class="glyphicon glyphicon-chevron-down"></span>
                                </span>
                            </a>
                        </th>                            
                    <?php }
                }else{?>
                    <th>
                        <a   ng-click="orderByField='<?php echo $val['field']; ?>'; reverseSort = !reverseSort">
                            <?php echo $this->mdl->get_label($val['field']); ?>
                            <span ng-show="orderByField == '<?php echo $val['field']; ?>'">
                                <span ng-show="!reverseSort" class="glyphicon glyphicon-chevron-up"></span>
                                <span ng-show="reverseSort" class="glyphicon glyphicon-chevron-down"></span>
                            </span>
                        </a>
                    </th>                        
                <?php }
            }
            ?>
            <td width="100px">User Entry</td>
            <td width="155px">Tgl Entry</td>
            <td width="155px">Tgl Edit</td>
            <td width="150px">Event</td>
		</thead>
		<tbody>
			<tr ng-doeventrow="item" set-obj-rows="setObjRows(item)" tabindex="{{$index}}" ng-repeat="item in pejabat.items | orderBy:orderByField:reverseSort" ng-class='{"success":$index==selectedRow}' ng-click='selectRow($index)'>
				<td >{{$index+1}}</td>
				<td >{{item.p_nip}}</td>
				<td >{{item.p_name}}</td>
				<td >{{item.p_pangkat}}</td>
				<td >{{item.p_jabatan}}</td>
                <td >{{item.user_entry}}</td>
                <td>{{item.date_entry | date: 'dd-MM-yyyy H:m:s'}}</td>
				<td>{{item.date_edit | date:'dd-MM-yyyy H:m:s'}}</td>
				<td >
					<a   class="btn btn-success btn-xs" ng-click="editItem($index,item)" ><span class="glyphicon glyphicon-edit"></span>  Edit</a>
					<a   class="btn btn-danger btn-xs" ng-click="removeItem($index, item)" ><span class="glyphicon glyphicon-floppy-remove"></span> Delete</a>
				</td>
			</tr>			
		</tbody>
		<tfoot>
			<tr>
				<td colspan="9" style="text-align:right;">
					<ul class="pagination pagination-sm" style="margin:0 0 0 0;">
						<li><a   ng-click="onFirstPage()">&#8249;&#8249;</a></li>
						<li><a   ng-click="onPrevPage()">&#8249;</a></li>
						<li><span>Page {{nextpage/stop+1}} of {{max_page/stop+1}}</span></li>
						<li><a   ng-click="onNextPage()">&#8250;</a></li>
						<li><a   ng-click="onLastPage()">&#8250;&#8250;</a></li>
					</ul>
				</td>
			</tr>
		</tfoot>
	</table>Isi {{objRows.p_nip}}
</div>