<script src="<?php echo base_url(); ?>assets/lib/doevent/do-event-input.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.grid.js"></script>
<script type="text/javascript">
	'use strict';
    //angular.module('csv', ['ngCsv']);
	var ngDishub = angular.module('ngDishub', ['ngCsv','ngDoeventrow']);
	ngDishub.controller('PejabatCtrl', function($scope, $http, $sce, $document) {
		$scope.errorshow=false;
		$scope.setcenter=false;
		$scope.pesanerror="";
		$scope.orderByField = 'p_nip';
        $scope.reverseSort = false;
        $scope.objRows = [];
        $scope.setObjRows = function(item){
        	$scope.objRows = item;
        }
		
		$scope.stop = <?php echo isset($stop)?$stop:5; ?>;
		$scope.max_page = <?php echo isset($max_page)?$max_page:5; ?>;
		$scope.nextpage = 0;

		$scope.edit = -1 ;
		
		$scope.fieldcaris = [{name:'Pejabat Name',value:'p_name'},{name:'Pangkat',value:'p_pangkat'},{name:'Jabatan',value:'p_jabatan'}];
		$scope.fieldcari = $scope.fieldcaris[0];
		$scope.txtcari = '';
		$scope.obj = {p_id:'0', p_name:'',p_pangkat:'',p_jabatan:'',p_nip:''};
		$scope.pejabat = { items : <?php echo count($rec)>0? json_encode($rec):"[]";?>};
		
		$scope.showErrorMessage = function() {
			return $sce.trustAsHtml($scope.pesanerror);
		};
		//Add data
		$scope.showAdd = function (){
        	$scope.edit = -1 ;
        	$scope.obj = {p_id:'0', p_name:'',p_pangkat:'',p_jabatan:'',p_nip:''};
        	$scope.errorshow=false;
        	$('#win-pejabat').modal('show');
        	//angular.element('#p_nip').focus();
        }

        $scope.addItem = function() {
        	var evt = "";
        	if ($scope.edit == -1 ){
		        evt = "insert_data";
		    }else{
		    	evt = "edit_data" ;
		    }
			$http.get('<?php echo site_url() .'/pejabat/'; ?>' + evt, {
				params: {
					p_name : $scope.obj.p_name,
                    p_pangkat : $scope.obj.p_pangkat,
					p_jabatan : $scope.obj.p_jabatan,
					p_id : $scope.obj.p_id,
					p_nip : $scope.obj.p_nip
				}
			}).
			success(function(data) {
				if(data!=undefined){
					if(data.success==true){
                        $scope.obj.user_entry = '<?php echo $this->session->userdata('user_name'); ?>';
                        $scope.obj.date_edit = new Date();
						if ($scope.edit == -1 ){
                            $scope.obj.date_entry = new Date();
                            $scope.obj.p_id = data.key;
							$scope.pejabat.items.push($scope.obj);
						}else{
                            $scope.pejabat.items[$scope.edit] = $scope.obj;
						}
               			$scope.errorshow=false;
               			$scope.setcenter = false;
						$('#win-pejabat').modal('hide');
                	}else{
                		//$('#msg-error').html(data.msg);
						$scope.pesanerror=data.msg;
                		$scope.errorshow=true;                		
                	}
				}
			});	    		
	    	
	    }
	    //Edit data
	    $scope.editItem = function(index, item){
	    	$scope.edit = index ;
	    	$scope.obj = item;
			$('#win-pejabat').modal('show');
	    }

		//Hapus data
		$scope.removeItem = function(index, item) {

	    	if(confirm('Anda yakin HAPUS data ini? ')){
				$http.get('<?php echo site_url() .'/pejabat/delete_data/'; ?>', {
					params: {
						'p_id' :  item.p_id
					}
				}).
				success(function(data) {
					if(data!=undefined){
						$scope.pejabat.items.splice(index, 1);
					}
				});	    		
	    	}
	    }
		
		$scope.search = function (){
			$http.get('<?php echo site_url() .'/pejabat/search/'; ?>', {
				params: {
					'field' : $scope.fieldcari.value,
					'value' : $scope.txtcari,
					'stop'  : $scope.nextpage
				}
			}).
			success(function(data) {
				if(data!=undefined){
					$scope.pejabat = { items : data.rec};
					$scope.max_page = data.max_page;
					if($scope.max_page<$scope.nextpage){
						$scope.nextpage=0;
					}
				}
			})
		}

		$scope.onKeydown= function(ev){
			$scope.pressed = ev.which;
			if (ev.which==13 || ev.which==9)
	            $scope.search();
		}

		$scope.onFirstPage = function(){
			if ($scope.nextpage  > 0 ){
				$scope.nextpage = 0;
				$scope.search();
			}
		}

		$scope.onNextPage = function(){
			if (($scope.nextpage + $scope.stop) <= $scope.max_page ){
				$scope.nextpage = $scope.nextpage+$scope.stop;
				//console.log($scope.nextpage);
				$scope.search();
			}
		}

		$scope.onPrevPage = function(){
			if ($scope.nextpage  >0 ){
				$scope.nextpage = $scope.nextpage-$scope.stop;
				$scope.search();
			}
		}		

		$scope.onLastPage = function(){
			if ($scope.nextpage  < $scope.max_page ){
				$scope.nextpage = $scope.max_page;
				$scope.search();
			}
		}

		$scope.selectRow = function(row) {
          $scope.selectedRow = row;
        };
		

		$scope.cetak = function (){
			print_preview('<?php echo site_url(). '/'. $this->fn .'/cetak/'; ?>','');
		}

		$document.bind('keydown', function(event) {
			console.log(event.which);
			switch (event.which)
            {
                case 65:
                    if(event.altKey){
                        $scope.showAdd();
                        return false;
                    }
                    break;
                case 80 :
                	if(event.altKey){
                        $scope.cetak();
                        return false;
                    }
                	break;
                
            }
    	})

	}).directive('deFocus', deEvent);
</script>