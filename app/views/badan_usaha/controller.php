<script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.grid.js"></script>
<script type="text/javascript">
    'use strict';
    var ngDishub = angular.module('ngDishub', ['ngCsv','ngDoeventrow']);
    ngDishub.controller('BadanUsahaCtrl', function($scope, $http, $sce, $document) {
        $scope.errorshow = false;
        $scope.pesanerror = "";

        $scope.stop = <?php echo isset($stop) ? $stop : 5; ?>;
        $scope.max_page = <?php echo isset($max_page) ? $max_page : 5; ?>;
        $scope.nextpage = 0;

        $scope.edit = -1;

        $scope.fieldcaris = [{name: 'badan_usaha Name', value: 'bu_name'}];
        $scope.fieldcari = $scope.fieldcaris[0];
        $scope.txtcari = '';
        $scope.obj = {bu_id: '0', bu_name: ''};
        $scope.badan_usaha = {items: <?php echo count($rec) > 0 ? json_encode($rec) : "[]"; ?>};

        $scope.showErrorMessage = function() {
            return $sce.trustAsHtml($scope.pesanerror);
        };
        //Add data
        $scope.showAdd = function() {
            $scope.edit = -1;
            $scope.obj = {bu_id: 0, bu_name: '', bu_pangkat: '', bu_jabatan: ''};
            $scope.errorshow = false;
            $scope.setcenter = !$scope.setcenter;
            $('#win-badan_usaha').modal('show');
        }

        $scope.addItem = function() {
            var evt = "";
            if ($scope.edit == -1) {
                evt = "insert_data";
            } else {
                evt = "edit_data";
            }
            $http.get('<?php echo site_url() . '/badan_usaha/'; ?>' + evt, {
                params: {
                    bu_name: $scope.obj.bu_name,
                    bu_id: $scope.obj.bu_id
                }
            }).
            success(function(data) {
                if (data != undefined) {
                    if (data.success == true) {
                        $scope.obj.user_entry = '<?php echo $this->session->userdata('user_name'); ?>';
                        $scope.obj.date_edit = new Date();
                        if ($scope.edit == -1 ){
                            $scope.obj.date_entry = new Date();
                            $scope.obj.bu_id = data.key;
                            $scope.badan_usaha.items.push($scope.obj);
                        } else {
                            $scope.badan_usaha.items[$scope.edit] = $scope.obj;
                        }
                        $scope.errorshow = false;
                        $('#win-badan_usaha').modal('hide');
                        $scope.setcenter = !$scope.setcenter;
                    } else {
                        //$('#msg-error').html(data.msg);
                        $scope.pesanerror = data.msg;
                        $scope.errorshow = true;
                    }
                }
            });

        }
        //Edit data
        $scope.editItem = function(index, item) {
            $scope.edit = index;
            $scope.obj = item;
            $('#win-badan_usaha').modal('show');
        }

        //Hapus data
        $scope.removeItem = function(index, item) {

            if (confirm('Anda yakin HAPUS data ini? ')) {
                $http.get('<?php echo site_url() . '/badan_usaha/delete_data/'; ?>', {
                    params: {
                        'bu_id': item.bu_id
                    }
                }).
                success(function(data) {
                    if (data != undefined) {
                        $scope.badan_usaha.items.splice(index, 1);
                    }
                });
            }
        }

        $scope.search = function() {
            $http.get('<?php echo site_url() . '/badan_usaha/search/'; ?>', {
                params: {
                    'field': $scope.fieldcari.value,
                    'value': $scope.txtcari,
                    'stop': $scope.nextpage
                }
            }).
            success(function(data) {
                if (data != undefined) {
                    $scope.badan_usaha = {items: data.rec};
                    $scope.max_page = data.max_page;
                    if ($scope.max_page < $scope.nextpage) {
                        $scope.nextpage = 0;
                    }
                }
            })
        }

        $scope.onKeydown = function(ev) {
            $scope.pressed = ev.which;
            if (ev.which == 13 || ev.which == 9)
                $scope.search();
        }

        $scope.onFirstPage = function() {
            if ($scope.nextpage > 0) {
                $scope.nextpage = 0;
                $scope.search();
            }
        }

        $scope.onNextPage = function() {
            if (($scope.nextpage + $scope.stop) <= $scope.max_page) {
                $scope.nextpage = $scope.nextpage + $scope.stop;
                //console.log($scope.nextpage);
                $scope.search();
            }
        }

        $scope.onPrevPage = function() {
            if ($scope.nextpage > 0) {
                $scope.nextpage = $scope.nextpage - $scope.stop;
                $scope.search();
            }
        }

        $scope.onLastPage = function() {
            if ($scope.nextpage < $scope.max_page) {
                $scope.nextpage = $scope.max_page;
                $scope.search();
            }
        }

        $scope.selectRow = function(row) {
            $scope.selectedRow = row;
        };

        $scope.cetak = function (){
            print_preview('<?php echo site_url(). '/'. $this->fn .'/cetak/'; ?>','');
        }

        $document.bind('keydown', function(event) {
            console.debug(event.which);
            switch (event.which)
            {
                case 65:
                    if(event.altKey){
                        $scope.showAdd();
                        return false;
                    }
                    break;
                case 80 :
                    if(event.altKey){
                        $scope.cetak();
                        return false;
                    }
                    break;
            }
        })

    }).directive('deCenter', deCenter)
    .directive('deFocus', deEvent);
</script>