
<?php  
	$data['rec'] = $rec;
	$data['stop'] = $stop;
	$data['max_page'] = $max_page;
	$this->load->view('badan_usaha/controller', $data);
?>
<div ng-controller="BadanUsahaCtrl">
	<!--Windows Modal-->
	<div class="modal fade" id="win-badan_usaha" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add / Edit Data</h4>
				</div>
				<div class="modal-body">

					<div ng-show="errorshow" class="alert alert-danger" ng-bind-html='showErrorMessage()'></div>

					<div class="form-horizontal" role="form">
						<div class="form-group">
						   	<label for="bu_name" class="col-sm-3 control-label">Badan Usaha</label>
						    <div class="col-sm-9 input-group input-group-sm">
								<span class="input-group-addon">@</span>
						      	<input de-focus ng-model="obj.bu_name" type="text" class="form-control input-sm" id="bu_name" placeholder="Nama badan_usaha">
						    </div>
					  	</div>

					  	<input type="hidden" ng-model="obj.bu_id" />
					  	
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary btn-sm" ng-click="addItem()" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
					<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> Close</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<?php $this->load->view('template/toolbar_grid'); ?>
	
	<form class="navbar-form" style="padding-left:0;">
		<div class="form-group">
			<select class="form-control input-sm" ng-model="fieldcari" ng-options="c.name for c in fieldcaris"></select>
		</div>
			<div class="form-group">
				<input type="text" placeholder="Data yang dicari" class="form-control input-sm" ng-model="txtcari" ng-keydown="onKeydown($event)"/>
			</div>
		<button type="button" class="btn btn-success btn-sm" ng-click="search()"><span class="glyphicon glyphicon-search"></span> Cari</button>
	</form>
        
	<!-- id="tbl-scroll" -->
	<table id="tbl-scroll" class="table table-bordered table-striped table-hover">
		<thead>
			<tr>
				<td>No</td>
				<td>badan_usaha Name</td>
				<td width="100px">User Entry</td>
				<td width="155px">Tgl Entry</td>
				<td width="155px">Tgl Edit</td>
				<td width="132px">Event</td>
			</tr>
		</thead>
		<tbody>
			<tr ng-doeventrow="item" set-obj-rows="setObjRows(item)" tabindex="{{$index}}" ng-repeat="item in badan_usaha.items" class="first" ng-class='{"success":$index==selectedRow}' ng-click='selectRow($index)'>
				<td>{{$index+1}}</td>
				<td>{{item.bu_name}}</td>
				<td>{{item.user_entry}}</td>
                <td>{{item.date_entry | date: 'dd-MM-yyyy H:m:s'}}</td>
                <td>{{item.date_edit | date:'dd-MM-yyyy H:m:s'}}</td>
				<td>
					<a   class="btn btn-success btn-xs" ng-click="editItem($index,item)" ><span class="glyphicon glyphicon-edit"></span>  Edit</a>
					<a   class="btn btn-danger btn-xs" ng-click="removeItem($index, item)" ><span class="glyphicon glyphicon-floppy-remove"></span> Delete</a>
				</td>
			</tr>			
		</tbody>
		<tfoot>
			<tr>
				<td colspan="6" style="text-align:right;">
					<ul class="pagination pagination-sm" style="margin:0 0 0 0;">
						<li><a   ng-click="onFirstPage()">&#8249;&#8249;</a></li>
						<li><a   ng-click="onPrevPage()">&#8249;</a></li>
						<li><span>Page {{nextpage/stop+1}} of {{max_page/stop+1}}</span></li>
						<li><a   ng-click="onNextPage()">&#8250;</a></li>
						<li><a   ng-click="onLastPage()">&#8250;&#8250;</a></li>
					</ul>
				</td>
			</tr>
		</tfoot>
	</table>
</div>
