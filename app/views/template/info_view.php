<div class="panel panel-danger">
	<div class="panel-heading">
		<h3 class="panel-title">Info Key</h3>
	</div>
	<div class="panel-body">
		<table>
			<tr>
				<td><span class="label label-success">ALT</span></td>
				<td><span class="label label-success">+</span></td>
				<td><span class="label label-success">A</span></td>
				<td><span class="label label-success">=</span></td>
				<td><span class="label label-success">Untuk Menampilkan form Add</span></td>
			</tr>
			<tr>
				<td><span class="label label-success">ALT</span></td>
				<td><span class="label label-success">+</span></td>
				<td><span class="label label-success">P</span></td>
				<td><span class="label label-success">=</span></td>
				<td><span class="label label-success">Untuk Mencetak Data</span></td>
			</tr>
			<tr>
				<td colspan="3">
					<a   class="btn btn-default btn-xs"><span class="glyphicon glyphicon-new-window"></span> Add</a>
				</td>
				<td><span class="label label-success">=</span></td>
				<td><span class="label label-success">Untuk Menambah data</span></td>
			</tr>
			<tr>
				<td colspan="3">
					<a   class="btn btn-default btn-xs"><span class="glyphicon glyphicon-print"></span> Print</a>
				</td>
				<td><span class="label label-success">=</span></td>
				<td><span class="label label-success">Untuk mencetak data</span></td>
			</tr>
			<tr>
				<td colspan="3">
					<a   class="btn btn-default btn-xs"><span class="glyphicon glyphicon-export"></span> Print</a>
				</td>
				<td><span class="label label-success">=</span></td>
				<td><span class="label label-success">Untuk export data</span></td>
			</tr>
			<tr>
				<td colspan="3">
					<a   class="btn btn-success btn-xs"><span class="glyphicon glyphicon-edit"></span></a>
				</td>
				<td><span class="label label-success">=</span></td>
				<td><span class="label label-success">Untuk Edit data</span></td>
			</tr>
			<tr>
				<td colspan="3">
					<a   class="btn btn-success btn-xs">E</a>
				</td>
				<td><span class="label label-success">=</span></td>
				<td><span class="label label-success">Untuk Edit data(Pada Record aktif)</span></td>
			</tr>
			<tr>
				<td colspan="3">
					<a   class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-floppy-remove"></span></a>
				</td>
				<td><span class="label label-success">=</span></td>
				<td><span class="label label-success">Untuk menghapus data</span></td>
			</tr>
			<tr>
				<td colspan="3">
					<a   class="btn btn-success btn-xs">D</a>
				</td>
				<td><span class="label label-success">=</span></td>
				<td><span class="label label-success">Untuk menghapus data(Pada Record aktif)</span></td>
			</tr>
			<tr>
				<td colspan="3">
					<a   class="btn btn-success btn-xs"><span class="glyphicon glyphicon-plus"></span></a>
				</td>
				<td><span class="label label-success">=</span></td>
				<td><span class="label label-success">Untuk manmpilkan detail data</span></td>
			</tr>
		</table>
		<div>
			
	</div>
</div>