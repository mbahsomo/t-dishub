<div class="btn-group toolbar navbar-right">
	<button type="button" class="btn btn-default btn-sm"  ng-click="showAdd();setcenter=true;"><span class="glyphicon glyphicon-new-window"></span> Add</button>
	<button type="button" class="btn btn-default btn-sm" ng-click="cetak();"><span class="glyphicon glyphicon-print"></span> Print</button>
	<div class="btn-group">
		<button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown"><span class="glyphicon glyphicon-export"></span> Export <span class="caret"></span>
	  	</button>
		<ul class="dropdown-menu" role="menu">
		    <li><a   ng-csv="pejabat.items" filename="pejabat.csv">&nbsp;Excel</a></li>
		    <li><a  >PDF</a></li>
		    <li><a  >Word</a></li>
	  	</ul>
  	</div>	  	
</div>