<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        body {
          padding-top: 40px;
          padding-bottom: 40px;
          background-color: #eee;
        }

        .form-signin {
          max-width: 330px;
          padding: 15px;
          margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
          margin-bottom: 10px;
        }
        .form-signin .checkbox {
          font-weight: normal;
        }
        .form-signin .form-control {
          position: relative;
          font-size: 16px;
          height: auto;
          padding: 10px;
          -webkit-box-sizing: border-box;
             -moz-box-sizing: border-box;
                  box-sizing: border-box;
        }
        .form-signin .form-control:focus {
          z-index: 2;
        }
        .form-signin input[type="text"] {
          margin-bottom: -1px;
          border-bottom-left-radius: 0;
          border-bottom-right-radius: 0;
        }
        .form-signin input[type="password"] {
          margin-bottom: 10px;
          border-top-left-radius: 0;
          border-top-right-radius: 0;
        }

    </style>
    
   <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/ico/favicon.png">
  </head>

  <body>

    <div class="container">
        
        <form class="form-signin" method="POST" action="<?php echo site_url('awal/dologin') ?>">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2 class="form-signin-heading">Please sign in</h2>
                    <input type="text" class="form-control" placeholder="Email address" autofocus name="nama" >
                    <input type="password" class="form-control" placeholder="Password" name="pass">
                    <label class="checkbox">
                        <input type="checkbox" value="remember-me" name="ingat"> Remember me
                    </label>
                    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                </div>
            </div>
        </form>
            
    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <div class="span12">
        <footer>
        <p align="center">&copy; mbahsomo@do-event.com</p>
        </footer>
    </div>


  </body>
</html>
