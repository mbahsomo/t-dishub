<!doctype html>
<html ng-app="ngDishub">
    <head>
        <title>Dishub</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <!--<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">-->
        <script type="text/javascript">
            var browser = '<?php echo BROWSER; ?>';
            var base_url = '<?php echo base_url(); ?>';
            var site_url = '<?php echo site_url(); ?>/';
        </script>

        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/t-table.css" rel="stylesheet">

        <!-- Just for debugging purposes. Don't actually copy this line! -->
        <!--[if lt IE 9]><script src="<?php echo base_url(); ?>assets/bootstrap/js/ie8-responsive-file-warning.js"></script><![endif]-->

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/html5shiv.js"></script>
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/respond.min.js"></script>
        <![endif]-->

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="<?php echo base_url(); ?>assets/js/jquery-2.0.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/angular.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>

    </head>
    <body>
        <!-- Fixed navbar -->
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand"  >T-Dishub</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?php echo site_url(); ?>">Home</a></li>
                        <li class="dropdown">
                            <a   class="dropdown-toggle" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-wrench"></span> Master<b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo site_url('master/tarif'); ?>"><span class="glyphicon glyphicon-wrench"></span> Tarif</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('master/trayek'); ?>"><span class="glyphicon glyphicon-wrench"></span> Trayek</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('master/po'); ?>"><span class="glyphicon glyphicon-wrench"></span> PO</a>
                                </li>
                                <!-- <li><a href="<?php echo site_url('master/kendaraan'); ?>"><span class="glyphicon glyphicon-wrench"></span> Kendaraan</a></li> -->
                                <li><a href="<?php echo site_url('master/trayekkendaraan'); ?>"><span class="glyphicon glyphicon-wrench"></span> Trayek Kendaraan</a></li>
                                <!-- <li class="dropdown-submenu">
                                    <a href="#"><span class="glyphicon glyphicon-wrench"></span> Lain - lain</a>
                                    <ul class="dropdown-menu"> -->
                                        <li><a href="<?php echo site_url('master/pejabat'); ?>"><span class="glyphicon glyphicon-wrench"></span> Pejabat</a></li>
                                        <li><a href="<?php echo site_url('master/propinsi'); ?>"><span class="glyphicon glyphicon-wrench"></span> Propinsi</a></li>
                                        <li><a href="<?php echo site_url('master/kota'); ?>"><span class="glyphicon glyphicon-wrench"></span> Kota</a></li>
                                        
                                        <li><a href="<?php echo site_url('master/terminal'); ?>"><span class="glyphicon glyphicon-wrench"></span> Terminal</a></li>
                                        <li><a href="<?php echo site_url('master/layanan'); ?>"><span class="glyphicon glyphicon-wrench"></span> Pelayanan</a></li>
                                        <li><a href="<?php echo site_url('master/jenisangkutan'); ?>"><span class="glyphicon glyphicon-wrench"></span> Jenis Angkutan</a></li>
                                        <li><a href="<?php echo site_url('master/jenistrayek'); ?>"><span class="glyphicon glyphicon-wrench"></span> Jenis Trayek</a></li>
                                        <li><a href="<?php echo site_url('master/badan_usaha'); ?>"><span class="glyphicon glyphicon-wrench"></span> Badan Usaha</a></li>
                                        <li role="presentation" class="divider"></li>
                                        <li><a href="<?php echo site_url('master/user'); ?>"><span class="glyphicon glyphicon-wrench"></span> User</a></li>                                    
                                   <!--  </ul>
                                </li> -->                                
                            </ul>
                        </li>
                        
                        <li class="dropdown">
                            <a   class="dropdown-toggle" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-edit"></span> Proses<b class="caret"></b>
                            </a> 
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?php echo site_url('entry/skid'); ?>"><span class="glyphicon glyphicon-edit"></span> Pembuaatan SKID</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('entry/kartupengawasan'); ?>"><span class="glyphicon glyphicon-edit"></span> Permohonan Kartu Pengawasan</a>
                                </li>
                                <li>
                                    <a href="<?php echo site_url('entry/pergantian_nomor_kendaraan'); ?>"><span class="glyphicon glyphicon-edit"></span> Pergantian Nomor Kendaraan dan Perpanjangan STUK</a>
                                </li>
<!--                                <li>
                                    <a href="<?php echo site_url('entry/perpanjangan_stuk'); ?>"><span class="glyphicon glyphicon-edit"></span> Perpanjangan STUK</a>
                                </li>-->
                            </ul>
                        </li>
                        
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-list-alt"></span> Report<b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('laporan/laporanpo'); ?>"><span class="glyphicon glyphicon-list-alt"></span> Laporan Jumlah Bus Per Trayek</a></li>
                                <li><a href="<?php echo site_url('laporan/laporankartupengawasan'); ?>"><span class="glyphicon glyphicon-list-alt"></span> Laporan Jml Trayek per PO</a></li>
                                <li><a href="<?php echo site_url('laporan/laporanpergantiannopol'); ?>"><span class="glyphicon glyphicon-list-alt"></span> Laporan KPS</a></li>
                            </ul>
                        </li>
                        
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown">
                                <span class="glyphicon glyphicon-calendar"></span> Histori<b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo site_url('entry/pergantian_nomor_kendaraan'); ?>"><span class="glyphicon glyphicon-calendar"></span> Perubahan Nopol Kendaraan</a></li>
                                <li><a href="<?php echo site_url('entry/perpanjangan_stuk'); ?>"><span class="glyphicon glyphicon-calendar"></span> Perpanjangan STUK</a></li>
                            </ul>
                        </li>
                        
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a  >User Online : <?php echo $this->session->userdata('user_name'); ?></a></li>
                        <li><a href="<?php echo site_url('awal/logout'); ?>">Log Out</a></li>
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </div>

        <!-- Load Library -->
        <script src="<?php echo base_url(); ?>assets/lib/doevent/do-event-input.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.print.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib/angularjs/angular-sanitize.js"></script>
        <script src="<?php echo base_url(); ?>assets/lib/angularjs/ng-csv.js"></script>        
        <!--script src="<?php echo base_url(); ?>assets/js/animasi-table.js"></script-->

        <div class="container">
            <div class="banner-atas"></div>
            <?php
            if (isset($path))
                $this->load->view('template/path_view');
            echo isset($content) ? $content : '';
            if (isset($info))
                $this->load->view('template/info_view', array('info' => $info));
            ?>
        </div> <!-- /container -->

        <div class="span12">
            <footer>
                <p align="center">&copy; mbahsomo@do-event.com</p>
            </footer>
        </div> 
    </body>
</html>
<?php
/* $menu = $this->session->userdata('user_menu');
  echo '<pre>';
  echo get_user_akses($menu,'User','um_cetak');
  echo '</pre>'; */
/* echo '<pre>';
  print_r($menu);
  echo '</pre>'; */
//echo get_user_akses($this->session->userdata('user_menu'), $this->fn, 'um_status');
?>