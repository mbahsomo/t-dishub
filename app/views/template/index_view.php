<script src="<?php echo base_url(); ?>assets/lib/chart/Chart.min.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/chart/angles.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.grid.js"></script>
<script type="text/javascript">
var ngDishub = angular.module('ngDishub', ['angles','ngDoeventrow']);

ngDishub.controller("chartCtrl", function ($scope, $http, $location) { 	
    console.log($location.path);
	$scope.chart = [
		{
			labels : <?php echo json_encode($nm_bulan); ?>,
			datasets : [
				{
					fillColor : "rgba(151,187,205,0)",
					strokeColor : "#e67e22",
					pointColor : "rgba(151,187,205,0)",
					pointStrokeColor : "#e67e22",
					data : <?php echo json_encode($grafik_po); ?>
				}
			], 
		}, {
			labels : <?php echo json_encode($nm_bulan); ?>,
			datasets : [
				{
					fillColor : "rgba(151,187,205,0)",
					strokeColor : "#e67e22",
					pointColor : "rgba(151,187,205,0)",
					pointStrokeColor : "#e67e22",
					data : <?php echo json_encode($grafik_kendaraan); ?>
				}
			], 
		}
	]
	
	$scope.options = { 
		segmentShowStroke : false
	}
})

ngDishub.controller('PoCtrl', function($scope, $http, $sce, $document, $filter) {
    $scope.errorshow = false;
    $scope.pesanerror = "";
    $scope.orderByField = 'po_code';
    $scope.reverseSort = false;

    $scope.stop = <?php echo isset($stop) ? $stop : 5; ?>;
    $scope.max_page = <?php echo isset($max_page) ? $max_page : 5; ?>;
    $scope.nextpage = 0;
    
    $scope.po = {items: <?php echo count($rec) > 0 ? json_encode($rec) : "[]"; ?>};

    $scope.search = function() {
        $http.get('<?php echo site_url() . '/po/search/'; ?>', {
            params: {
                'field': 'po_code',
                'value': '',
                'stop': $scope.nextpage
            }
        }).
        success(function(data) {
            if (data != undefined) {
                $scope.po = {items: data.rec};
                $scope.max_page = data.max_page;
                if ($scope.max_page < $scope.nextpage) {
                    $scope.nextpage = 0;
                }
            }
        })
    }

    $scope.onKeydown = function(ev) {
        $scope.pressed = ev.which;
        if (ev.which == 13 || ev.which == 9)
            $scope.search();
    }

    $scope.onFirstPage = function() {
        if ($scope.nextpage > 0) {
            $scope.nextpage = 0;
            $scope.search();
        }
    }

    $scope.onNextPage = function() {
        if (($scope.nextpage + $scope.stop) <= $scope.max_page) {
            $scope.nextpage = $scope.nextpage + $scope.stop;
            //console.log($scope.nextpage);
            $scope.search();
        }
    }

    $scope.onPrevPage = function() {
        if ($scope.nextpage > 0) {
            $scope.nextpage = $scope.nextpage - $scope.stop;
            $scope.search();
        }
    }

    $scope.onLastPage = function() {
        if ($scope.nextpage < $scope.max_page) {
            $scope.nextpage = $scope.max_page;
            $scope.search();
        }
    }

    $scope.selectRow = function(row) {
        $scope.selectedRow = row;
    };

    $scope.cetak = function (){
        print_preview('<?php echo site_url(). '/'. $this->fn .'/cetak/'; ?>','');
    }

    $document.bind('keydown', function(event) {
        //console.debug(event.which);
        switch (event.which)
        {
            case 65:
                if(event.altKey){
                    $scope.showAdd();
                    return false;
                }
                break;
            case 80 :
                if(event.altKey){
                    $scope.cetak();
                    return false;
                }
                break;               
        }
    });

    //Untuk detail kendaraan
    $scope.loadDetail = function(kode){
        $http.get('<?php echo site_url() .'/po_kendaraan/search/'; ?>', {
            params: {
                'field' : 'po_code',
                'value' : kode,
                'stop'  : 0
            }
        }).
        success(function(data) {
            if(data!=undefined){
                return data.rec;
            }
        });
    }

    $scope.terminals;

    $scope.loadTerminal = function(){
        $http.get('<?php echo site_url() .'/terminal/get_all/'; ?>').
        success(function(data) {
             $scope.terminals = data.rec;
        })
    }

    $scope.loadTerminal();

    $scope.searchPO = function(kode){
        $http.get('<?php echo site_url() . '/po/search/'; ?>', {
            params: {
                'field': 'terminal',
                'value': kode,
                'stop': 0
            }
        }).
        success(function(data) {
            if (data != undefined) {
                $scope.po = {items: data.rec};
                $scope.max_page = data.max_page;
                if ($scope.max_page < $scope.nextpage) {
                    $scope.nextpage = 0;
                }
            }
        })
    }
})
</script>
<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading"><span class="glyphicon glyphicon-wrench"></span> Dashboard</div>
            <div class="panel-body">

                <div class="row" ng-controller="chartCtrl">
                	<!-- Untuk PO -->
                	<div class="col-xs-11" style="width : 100%;">
                        <div class="well" id="po" ng-controller="PoCtrl">
		                	<div class="alert alert-success">Daftar PO</div>
                            <!--Pencarian-->
                            <button type="button" class="btn btn-default btn-sm" data-toggle="collapse" data-target="#search-po">
                              <span class="glyphicon glyphicon-search"></span> Search
                            </button>
                            <div class="whell collapse" id="search-po"  style="margin:10px 10px 10px 10px;">
                                
                                <!--Untuk Form Pencarian-->
                                <form action="" method="POST" class="form-horizontal" role="form">
                                    <div class="form-group">
                                        <label for="inputterminal3" class="col-sm-2 control-label">Terminal</label>
                                        <div class="col-sm-10">
                                            <select ng-model="cboterminal" class="form-control" >
                                                <option  ng-repeat="tr in terminals" value="{{tr.t_code}}">{{tr.t_name}}</option>
                                            </select>
                                        </div>
                                    </div>
                            
                                    <div class="form-group">
                                        <div class="col-sm-10 col-sm-offset-2">
                                            <button type="submit" ng-click="searchPO(cboterminal)" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-search"></span> Cari</button>
                                        </div>
                                    </div>
                                </form>
                                <!--Untuk Form Pencarian-->
                                
                            </div>
                            
                            <!--Pencarian-->

		                	<table id="tblpo" class="table table-bordered table-striped table-hover" style="font-size: 11px;">
						        <thead>
						            <tr>
						                <td colspan="20" style="text-align:right;">
						                    <ul class="pagination pagination-sm" style="margin:0 0 0 0;">
						                        <li><a ng-click="onFirstPage()">&#8249;&#8249;</a></li>
						                        <li><a ng-click="onPrevPage()">&#8249;</a></li>
						                        <li><span>Page {{nextpage / stop + 1}} of {{max_page / stop + 1}}</span></li>
						                        <li><a ng-click="onNextPage()">&#8250;</a></li>
						                        <li><a ng-click="onLastPage()">&#8250;&#8250;</a></li>
						                    </ul>
						                </td>
						            </tr>
						            <tr>
						                <?php
						                foreach ($this->Po_model->get_rule() as $val) {
						                    if (isset($val['grid'])) {
						                        if ($val['grid']){ ?>
						                            <th>
						                                <a href="#po" ng-click="orderByField='<?php echo $val['field']; ?>'; reverseSort = !reverseSort">
						                                    <?php echo $this->Po_model->get_label($val['field']); ?>
						                                    <span ng-show="orderByField == '<?php echo $val['field']; ?>'">
						                                        <span ng-show="!reverseSort" class="glyphicon glyphicon-chevron-up"></span>
						                                        <span ng-show="reverseSort" class="glyphicon glyphicon-chevron-down"></span>
						                                    </span>
						                                </a>
						                            </th>                            
						                        <?php }
						                    }else{?>
						                        <th>
						                            <a href="#po" ng-click="orderByField='<?php echo $val['field']; ?>'; reverseSort = !reverseSort">
						                                <?php echo $this->Po_model->get_label($val['field']); ?>
						                                <span ng-show="orderByField == '<?php echo $val['field']; ?>'">
						                                    <span ng-show="!reverseSort" class="glyphicon glyphicon-chevron-up"></span>
						                                    <span ng-show="reverseSort" class="glyphicon glyphicon-chevron-down"></span>
						                                </span>
						                            </a>
						                        </th>                        
						                    <?php }
						                }
						                ?>
                                        <th>Event</th>
						            </tr>
						        </thead>
						        <tbody ng-doeventrow="item" ng-repeat="item in po.items | orderBy:orderByField:reverseSort" tabindex="{{$index}}" ng-class='{"success":$index == selectedRow}' ng-click='selectRow($index)'>

                                    <tr  >
                                        <?php
                                        foreach ($this->Po_model->get_rule() as $val) {
                                            if (isset($val['grid'])) {
                                                if ($val['grid']){
                                                    echo "<td>{{item." . (isset($val['fieldLabel']) ? $val['fieldLabel']: $val['field']) .
                                                        "}}</td>";
                                                }
                                            }else {
                                                echo "<td>{{item." . (isset($val['fieldLabel'])? $val['fieldLabel']: $val['field']) .
                                                    "}}</td>";
                                            }
                                        }
                                        ?>
                                        <td>
                                            <a class="btn btn-success btn-xs" data-toggle="collapse" data-target="#detail{{$index}}"><span
                        class="glyphicon glyphicon-plus" ></span></a>
                                        </td>
                                    </tr>
                                    <tr >
                                       <td colspan="8" class="collapse" id="detail{{$index}}">
                                           <table class="hidden-detail table table-bordered table-striped table-hover" ng-class='{"show-detail":$index ==
                                           selectedRow}' style="font-size: 14px;">
                                               <thead>
                                                   <tr>
                                                       <th>NO</th>
                                                       <th>NOPOL</th>
                                                       <th>NO STUK</th>
                                                       <th>TG UJI</th>
                                                       <th>MERK</th>
                                                       <th>TYPE</th>
                                                       <th>JNS ANGKUTAN</th>
                                                       <th>TERMINAL</th>
                                                   </tr>
                                               </thead>
                                               <tbody>
                                                    <tr ng-doeventrow tabindex="{{$index}}"  ng-repeat="itmdtl in
                                                    item.detail">
                                                        <td>{{$index+1}}</td>
                                                        <td>{{itmdtl.pok_nopol}}</td>
                                                        <td>{{itmdtl.pok_nostuk}}</td>
                                                        <td>{{itmdtl.pok_tgl_uji}}</td>
                                                        <td>{{itmdtl.pok_merk}}</td>
                                                        <td>{{itmdtl.pok_type}}</td>
                                                        <td>{{itmdtl.ja_name}}</td>
                                                        <td>{{itmdtl.terminal}}</td>
                                                    </tr>
                                               </tbody>
                                           </table>
                                       </td> 
                                    </tr>

						        </tbody>						        
						    </table>
                            <style type="text/css">
                                .hidden-detail {
                                    display: none;
                                }
                                .show-detail{
                                    display: inline;
                                }
                            </style>
                        </div>
                    </div>
                	<!-- End PO -->
                    <div class="col-xs-6">
                        <div class="well">
                        	<div class="alert alert-success">Grafik PO</div>
                        	<canvas height="400px" width="500px" linechart options="options" data="chart[0]" id="lineChart" ></canvas>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="well">
                        	<div class="alert alert-success">Permohonan Kartu Pengawasan</div>
                        	<canvas height="400px" width="500px" linechart options="options" data="chart[1]" id="linechart" ></canvas>
                        </div>
                    </div>
                    
                    <div class="col-xs-6">
                        <div class="well">
                        	<div class="alert alert-success">Daftar Kartu Pengawasan</div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
	<div class="col-xs-6 col-sm-4">
		<div class="panel panel-primary">
			<div class="panel-heading"><span class="glyphicon glyphicon-wrench"></span> Master Data</div>
			<div class="panel-body">
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('master/pejabat');?>"><span class="glyphicon glyphicon-wrench"></span> Pejabat</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('master/propinsi');?>"><span class="glyphicon glyphicon-wrench"></span> Propinsi</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('master/kota');?>"><span class="glyphicon glyphicon-wrench"></span> Kota</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('master/terminal');?>"><span class="glyphicon glyphicon-wrench"></span> Terminal</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('master/layanan');?>"><span class="glyphicon glyphicon-wrench"></span> Pelayanan</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('master/jenisangkutan');?>"><span class="glyphicon glyphicon-wrench"></span> Jenis Angkutan</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('master/jenistrayek');?>"><span class="glyphicon glyphicon-wrench"></span> Jenis Trayek</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('master/badan_usaha');?>"><span class="glyphicon glyphicon-wrench"></span> Badan Usaha</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('master/user');?>"><span class="glyphicon glyphicon-wrench"></span> User</a>
			</div>
		</div>
	</div>

	<div class="col-xs-6 col-sm-4">
		<div class="panel panel-primary">
			<div class="panel-heading"><span class="glyphicon glyphicon-edit"></span> Entry Data</div>
			<div class="panel-body">
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('entry/tarif');?>"><span class="glyphicon glyphicon-edit"></span> Tarif</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('entry/trayek');?>"><span class="glyphicon glyphicon-edit"></span> Trayek</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('entry/po');?>"><span class="glyphicon glyphicon-edit"></span> PO</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('entry/kartupengawasan');?>"><span class="glyphicon glyphicon-edit"></span> Permohonan Kartu Pengawasan</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('entry/pergantian_nomor_kendaraan');?>"><span class="glyphicon glyphicon-edit"></span> Pergantian Nomor Kendaraan</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('entry/perpanjangan_stuk');?>"><span class="glyphicon glyphicon-edit"></span> Perpanjangan STUK</a>
			</div>
		</div>
	</div>
	
	<div class="col-xs-6 col-sm-4">
		<div class="panel panel-primary">
			<div class="panel-heading"><span class="glyphicon glyphicon-list-alt"></span> Laporan</div>
			<div class="panel-body">
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('laporan/laporanpo');?>"><span class="glyphicon glyphicon-list-alt"></span> Laporan PO</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('laporan/laporankartupengawasan');?>"><span class="glyphicon glyphicon-list-alt"></span> Laporan Kartu Pengawasan</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('laporan/laporanpergantiannopol');?>"><span class="glyphicon glyphicon-list-alt"></span> Laporan Pergantian Nomor Kendaraan</a>
				<a class="btn btn-default btn-block btn-sm" href="<?php echo site_url('laporan/laporanperpanjanganstuk');?>"><span class="glyphicon glyphicon-list-alt"></span> Laporan Perpanjangan STUK</a>
			</div>
		</div>
	</div>
	
	
</div>