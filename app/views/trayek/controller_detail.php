<script type="text/javascript">
    ngDishub.controller('TrayekDetailCtrl', function($scope, $http, $sce, $document, trayekDetail) {
        $scope.listTrayekDetail = trayekDetail.getTrayekDetail;
        $scope.getTrayekId = trayekDetail.getTrayekId;

        $scope.terminals = <?php echo count($terminal)>0? json_encode($terminal):"[]";?>;

        $scope.showErrorMessage = function() {
            return $sce.trustAsHtml($scope.pesanerror);
        };

        //Add data
        $scope.showAdd = function (){
            //alert($scope.getTrayekId());
            if($scope.getTrayekId()>0){
                $scope.edit = -1 ;
                var obj = {
                    tyj_id:0,
                    ty_code:$scope.getTrayekId(), 
                    j_code:'',
                    tyj_urut : $scope.listTrayekDetail.length+1
                };
                trayekDetail.add(obj);
            }else{
                alert('Pilih Trayek Dulu ..!');
            }
        }

        $scope.addItem = function(item, index) {
            var evt = "";
            if (item.tyj_id === 0 ){
                evt = "insert_data";
            }else{
                evt = "edit_data" ;
            }
            $http.get('<?php echo site_url() .'/trayek_jalan/'; ?>' + evt, {
                params: {
                    tyj_id : item.tyj_id,
                    ty_code : item.ty_code,
                    j_code : item.j_code,
                    tyj_urut : item.tyj_urut
                }
            }).
                success(function(data) {
                    if(data!==undefined){
                        if(data.success===true){
                            if (item.tyj_id === 0 ){
                                item.tyj_id = data.key;
                            }
                            trayekDetail.edit(index, item);
                        }else{
                            $scope.pesanerror=data.msg;
                            $scope.errorshow=true;
                        }
                    }
                });

        }
        
        //Hapus data
        $scope.removeItem = function(item,index) {
            console.log(item.tyj_id);
            if(confirm('Anda yakin HAPUS data ini? ')){
                if(item.tyj_id!=0){
                    $http.get('<?php echo site_url() .'/trayek_jalan/delete_data/'; ?>', {
                        params: {
                            'tyj_id' :  item.tyj_id
                        }
                    }).
                    success(function(data) {
                        if(data!=undefined){
                            trayekDetail.delete(index);
                        }
                    });
                }else{
                    trayekDetail.delete(index);
                }
            }
        }

        $scope.getStatus = function (sts){
            if(sts=='B'){
                return 'Berangkat';
            }else if(sts=='M'){
                return 'Melintas';
            }
            else if(sts=='D'){
                return 'Datang';
            }
        }
        
        
    }).directive('deFocus', deEvent);
</script>