<link href="<?php echo base_url(); ?>assets/lib/auto-complete/auto-complete.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/lib/auto-complete/auto-complete.js"></script>
<?php
$data['rec'] = $rec;
$data['stop'] = $stop;
$data['max_page'] = $max_page;
$this->load->view('trayek/controller', $data);
?>

<div ng-controller="TrayekCtrl">
    <!--Windows Modal-->
    <div class="modal fade" id="win-trayek" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add / Edit Data</h4>
                </div>
                <div class="modal-body">

                    <div ng-show="errorshow" class="alert alert-danger" ng-bind-html='showErrorMessage()'></div>

                    <div class="form-horizontal" role="form">

                        <div class="form-group">
                            <label for="t_code_tujuan" class="col-sm-3 control-label">
                                <?php echo $this->mdl->get_label('t_code_asal');?>
                            </label>
                            <div class="col-sm-9 input-group input-group-sm">
                                <autocomplete id="asal" ng-blur="setNoTrayek()" ng-model="obj.tasal"
                                    items="terminals"                   
                                    key-field="t_code"
                                    text-field="t_code"
                                    no-item-text="">
                                    {{item.t_code}}
                                </autocomplete>
                                <span id="sasal" class="input-group-addon">{{obj.tasal.t_name}}</span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="t_code_tujuan" class="col-sm-3 control-label"><?php echo $this->mdl->get_label
                                    ('t_code_tujuan');
                                ?></label>
                            <div class="col-sm-9 input-group input-group-sm">
                                <autocomplete id="tujuan" ng-blur="setNoTrayek()" ng-model="obj.ttujuan"
                                    items="terminals"                   
                                    key-field="t_code"
                                    text-field="t_code"
                                    no-item-text="">
                                    {{item.t_code}}
                                </autocomplete>
                                <span id="stujuan" class="input-group-addon">{{obj.ttujuan.t_name}}</span>

                                <!-- <select ng-change="setNoTrayek()" de-focus ng-model="obj.t_code_tujuan" class="form-control input-sm">
                                    <option ng-repeat="terminal in terminals" value="{{terminal
                                        .t_code}}">{{terminal.t_name}}</option>
                                </select> -->
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ty_code" class="col-sm-3 control-label"><?php echo $this->mdl->get_label
                                    ('ty_code');
                                ?></label>
                            <div class="col-sm-4  input-group input-group-sm">
                                <span class="input-group-addon">@</span>
                                <input de-focus ng-focus="setNoTrayek()" ng-model="obj.ty_code" type="text" class="form-control input-sm" id="ty_code" placeholder="<?php echo $this->mdl->get_label
                                    ('ty_code');
                                ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ty_name" class="col-sm-3 control-label"><?php echo $this->mdl->get_label
                                    ('ty_name');
                                ?></label>
                            <div class="col-sm-9">
                                <input de-focus ng-model="obj.ty_name" type="text" class="form-control input-sm" id="ty_name" placeholder="<?php echo $this->mdl->get_label
                                    ('ty_name');
                                ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ty_normal" class="col-sm-3 control-label"><?php echo $this->mdl->get_label
                                    ('ty_normal');
                                ?></label>
                            <div class="col-sm-9">
                                <input de-focus ng-model="obj.ty_normal" type="text" class="form-control input-sm" id="ty_normal" placeholder="<?php echo $this->mdl->get_label
                                    ('ty_normal');
                                ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="jt_id" class="col-sm-3 control-label"><?php echo $this->mdl->get_label
                                    ('jt_id');
                                ?></label>
                            <div class="col-sm-9">
                                <select de-focus ng-model="obj.jt_id" class="form-control input-sm">
                                    <option ng-repeat="jenistrayek in jenisTrayeks" value="{{jenistrayek
                                    .jt_id}}">{{jenistrayek.jt_name}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="l_id" class="col-sm-3 control-label"><?php echo $this->mdl->get_label
                                    ('l_id');
                                ?></label>
                            <div class="col-sm-9">
                                <select de-focus ng-model="obj.l_id" class="form-control input-sm">
                                    <option ng-repeat="layanan in layanans" value="{{layanan
                                    .l_id}}">{{layanan.l_name}}</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="ty_type" class="col-sm-3 control-label">Status</label>
                            <div class="col-sm-9">
                                <select de-focus ng-model="obj.ty_type"  class="form-control">
                                    <option ng-repeat="status in statuss" value="{{status.value}}">{{status.name}}</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" ng-click="addItem()" ><span class="glyphicon glyphicon-floppy-disk"></span> Simpan</button>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-folder-open"></span> Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div class="btn-group toolbar navbar-right" style="margin-top: 10px;">
        <button type="button" class="btn btn-default btn-sm"  ng-click="showAdd()"><span class="glyphicon glyphicon-new-window"></span> Add</button>
        <button type="button" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-print"></span> Print</button>
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown"><span class="glyphicon glyphicon-export"></span> Export <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
                <li><a >Excel</a></li>
                <li><a >PDF</a></li>
                <li><a >Word</a></li>
            </ul>
        </div>
    </div>

    <form class="navbar-form" style="padding-left:0;">
        <div class="form-group">
            <select class="form-control input-sm" ng-model="fieldcari" ng-options="c.name for c in fieldcaris"></select>
        </div>
        <div class="form-group">
            <input type="text" placeholder="Data yang dicari" class="form-control input-sm" ng-model="txtcari" ng-keydown="onKeydown($event)"/>
        </div>
        <button type="button" class="btn btn-success btn-sm" ng-click="search()"><span class="glyphicon glyphicon-search"></span> Cari</button>
    </form>

    <div class="table-responsive">
        <table class="table table-bordered detail">
            <thead>
            <tr>
                <th ><?php echo $this->mdl->get_label('ty_code'); ?></th>
                <th ><?php echo $this->mdl->get_label('ty_name'); ?></th>
                <th ><?php echo $this->mdl->get_label('ty_normal'); ?></th>
                <th ><?php echo $this->mdl->get_label('t_code_asal'); ?></th>
                <th ><?php echo $this->mdl->get_label('t_code_tujuan'); ?></th>
                <th>User Entry</th>
                <th>Tgl Entry</th>
                <th>Tgl Edit</th>
                <th width="100px" >Event</th>
            </tr>
            </thead>
            <tbody ng-doeventrow="item" tabindex="{{$index}}" ng-repeat="item in trayek.items" ng-class='{"success":$index==selectedRow}' ng-click='selectRow($index)'>
                <tr >
                    <td >{{item.ty_code}}</td>
                    <td >{{item.ty_name}}</td>
                    <td >{{item.ty_normal}}</td>
                    <td >{{item.asal}}</td>
                    <td >{{item.tujuan}}</td>
                    <td>{{item.user_entry}}</td>
                    <td>{{item.date_entry}}</td>
                    <td>{{item.date_edit}}</td>
                    <td width="100px">
                        <a  class="btn btn-success btn-xs" ng-click="editItem($index,
                        item)" ><span class="glyphicon glyphicon-edit"></span></a>
                        <a  class="btn btn-danger btn-xs" ng-click="removeItem($index, item)" ><span class="glyphicon
                        glyphicon-floppy-remove"></span></a>

                        <a class="btn btn-success btn-xs" data-toggle="collapse" data-target="#detail{{$index}}"><span
                        class="glyphicon glyphicon-plus" ></span></a>
                        <!-- <a href="#grid-detail" class="btn btn-success btn-xs" ng-click="setTrayekId(item.ty_code)" ><span
                                class="glyphicon glyphicon-plus"></span></a> -->
                    </td>
                </tr>
                <tr>
                    <td colspan="9" class="collapse" id="detail{{$index}}">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <h3 class="panel-title">Detail Jalan</h3>
                            </div>
                            <div class="panel-body">
                                <!--Table detail-->
                                <table class="table table-bordered table-striped table-hover detail">
                                    <caption style="text-align:right;">
                                        <button type="button" class="btn btn-default btn-sm"  ng-click="showAddDetail($index)"><span class="glyphicon glyphicon-new-window"></span> Add</button>
                                    </caption>
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th >Jalan</th>
                                            <th >Urutan</th>
                                            <th width="100px" >Event</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-doeventrow="item" tabindex="{{$index}}" ng-repeat="itemd in item.detail" ng-class='{"success":$index==selectedRow}' ng-click='selectRow
                                        ($index)'>
                                            <td>{{$index+1}}</td>
                                            <td >
                                                <select class="form-control input-sm" ng-model="itemd.j_code" ng-blur="addItemDetail(itemd, $index)">
                                                    <?php 
                                                        foreach ($jalan as $value) { ?>
                                                        <option value="<?php echo $value['j_code']; ?>"><?php echo $value['j_name']; ?></option>
                                                    <?php    }
                                                    ?>                    
                                                </select>
                                            </td>
                                            <td >
                                                <input style="text-align:right;" ng-model="itemd.tyj_urut" class="form-control input-sm" ng-blur="addItemDetail(itemd)" />
                                            </td>            
                                            <td width="100px">
                                                <a class="btn btn-success btn-xs" ng-click="addItemDetail(itemd)" ><span class="glyphicon glyphicon-floppy-save"></span></a>
                                                <a class="btn btn-danger btn-xs" ng-click="removeItemDetail(itemd, $index)" ><span class="glyphicon glyphicon-floppy-remove"></span></a>                
                                            </td>
                                        </tr>            
                                    </tbody>

                                </table>                               
                            </div>
                        </div>
                            
                    </td>
                </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="9" style="text-align:right;">
                    <ul class="pagination pagination-sm" style="margin:0 0 0 0;">
                        <li><a  ng-click="onFirstPage()">&#8249;&#8249;</a></li>
                        <li><a  ng-click="onPrevPage()">&#8249;</a></li>
                        <li><span>Page {{nextpage/stop+1}} of {{max_page/stop+1}}</span></li>
                        <li><a  ng-click="onNextPage()">&#8250;</a></li>
                        <li><a  ng-click="onLastPage()">&#8250;&#8250;</a></li>
                    </ul>
                </td>
            </tr>
            </tfoot>
        </table>
    </div>
</div>