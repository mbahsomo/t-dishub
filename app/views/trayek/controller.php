<script src="<?php echo base_url(); ?>assets/lib/doevent/do-event-input.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/doevent/doevent.grid.js"></script>
<script src="<?php echo base_url(); ?>assets/lib/mask/mask.js"></script>
<script type="text/javascript">
	'use strict';
	var ngDishub = angular.module('ngDishub', ['ngCsv','ngDoeventrow','ui.mask','directives']);
    
	ngDishub.controller('TrayekCtrl', function($scope, $http, $sce, $document) {
		$scope.errorshow=false;
		$scope.pesanerror="";
		
		$scope.stop = <?php echo isset($stop)?$stop:5; ?>;
		$scope.max_page = <?php echo isset($max_page)?$max_page:5; ?>;
		$scope.nextpage = 0;
        $scope.statuss = [
            {value:'A',name:'Antar Kota'},
            {value:'D',name:'Dalam Kota'}
        ];

		$scope.edit = -1 ;
		$scope.fieldcaris = [{name:'Kode',value:'ty_code'},{name:'Nama',value:'k_name'},{name:'Keterangan',value:'k_keterangan'},{name:'Region',value:'k_region'}];
		$scope.fieldcari = $scope.fieldcaris[0];
		$scope.txtcari = '';
		$scope.layanans = <?php echo count($layanan)>0? json_encode($layanan):"[]";?>;
        $scope.terminals = <?php echo count($terminal)>0? json_encode($terminal):"[]";?>;
        $scope.jenisTrayeks = <?php echo count($jenis_trayek)>0? json_encode($jenis_trayek):"[]";?>;
        $scope.obj = <?php echo json_encode($this->mdl->get_field_names());?>;
		$scope.trayek = { items : <?php echo count($rec)>0? json_encode($rec):"[]";?>};
        
		
		$scope.showErrorMessage = function() {
			return $sce.trustAsHtml($scope.pesanerror);
		};
		//Add data
		$scope.showAdd = function (){
        	$scope.edit = -1 ;
            $scope.obj = <?php echo json_encode($this->mdl->get_field_names());?>;
        	$scope.errorshow=false;
        	$('#win-trayek').modal('show');
        }

        $scope.addItem = function() {
        	var evt = "";
        	if ($scope.edit == -1 ){
		        evt = "insert_data";
		    }else{
		    	evt = "edit_data" ;
		    }
			$http.get('<?php echo site_url() .'/trayek/'; ?>' + evt, {
				params: {
					ty_code : $scope.obj.ty_code,
                    ty_name : $scope.obj.ty_name,
                    t_code_tujuan : $scope.obj.ttujuan.t_code,
                    t_code_asal : $scope.obj.tasal.t_code,
                    ty_normal : $scope.obj.ty_normal,
					jt_id : $scope.obj.jt_id,
					l_id : $scope.obj.l_id,
					ty_type : $scope.obj.ty_type
				}
			}).
			success(function(data) {
				if(data!==undefined){
					if(data.success===true){
						$scope.obj.user_entry = '<?php echo $this->session->userdata('user_name'); ?>';
                        $scope.obj.date_edit = '<?php echo date('d-M-Y H:m:s') ?>';
                        $scope.obj.asal = $scope.obj.tasal.t_name;
                        $scope.obj.tujuan = $scope.obj.ttujuan.t_name;
						if ($scope.edit === -1 ){
                            $scope.obj.date_entry = '<?php echo date('d-M-Y H:m:s') ?>';
                            //$scope.obj.ty_code = data.key;
                            $scope.trayek.items.push($scope.obj);
						}else{
							$scope.trayek.items[$scope.edit] = $scope.obj;
						}
               			$scope.errorshow=false;
						$('#win-trayek').modal('hide');
                	}else{
                		//$('#msg-error').html(data.msg);
						$scope.pesanerror=data.msg;
                		$scope.errorshow=true;                		
                	}
				}
			});	    		
	    	
	    }
	    //Edit data
	    $scope.editItem = function(index, item){
	    	$scope.edit = index ;
	    	item.tasal = $scope.getDataTerminal(item.t_code_asal);
			item.ttujuan = $scope.getDataTerminal(item.t_code_tujuan);
            $scope.obj = item;
			angular.element('#asal input').val( item.tasal.t_code );
			angular.element('#tujuan input').val( item.ttujuan.t_code );
//			angular.element('#sasal').text( item.asal.t_name );
//			angular.element('#stujuan').text( item.tujuan.t_name );
	    	$('#win-trayek').modal('show');
	    }

        $scope.getDataTerminal = function (id){
            var hasil = {}; 
            angular.forEach($scope.terminals, function(value, key){
                if (id==value.t_code){
                    hasil = value;
                    return false;
                }
            });
            console.log(hasil);
            return hasil;
        }

		//Hapus data
		$scope.removeItem = function(index, item) {

	    	if(confirm('Anda yakin HAPUS data ini? ')){
				$http.get('<?php echo site_url() .'/trayek/delete_data/'; ?>', {
					params: {
						'ty_code' :  $scope.trayek.items[index].ty_code
					}
				}).
				success(function(data) {
					if(data!=undefined){
						$scope.trayek.items.splice(index, 1);
						//$scope.max_page = data.max_page;
					}
				});	    		
	    	}
	    }
		
		$scope.search = function (){
			$http.get('<?php echo site_url() .'/trayek/search/'; ?>', {
				params: {
					'field' : $scope.fieldcari.value,
					'value' : $scope.txtcari,
					'stop'  : $scope.nextpage
				}
			}).
			success(function(data) {
				if(data!=undefined){
					$scope.trayek = { items : data.rec};
					$scope.max_page = data.max_page;
					if($scope.max_page<$scope.nextpage){
						$scope.nextpage=0;
					}
				}
			})
		}

		$scope.onKeydown= function(ev){
			$scope.pressed = ev.which;
			if (ev.which==13 || ev.which==9)
	            $scope.search();

		}

		$scope.onFirstPage = function(){
			if ($scope.nextpage  > 0 ){
				$scope.nextpage = 0;
				$scope.search();
			}
		}

		$scope.onNextPage = function(){
			if (($scope.nextpage + $scope.stop) <= $scope.max_page ){
				$scope.nextpage = $scope.nextpage+$scope.stop;
				//console.log($scope.nextpage);
				$scope.search();
			}
		}

		$scope.onPrevPage = function(){
			if ($scope.nextpage  >0 ){
				$scope.nextpage = $scope.nextpage-$scope.stop;
				$scope.search();
			}
		}		

		$scope.onLastPage = function(){
			if ($scope.nextpage  < $scope.max_page ){
				$scope.nextpage = $scope.max_page;
				$scope.search();
			}
		}

		$scope.selectRow = function(row) {
          $scope.selectedRow = row;
        };

        $scope.cetak = function (){
			print_preview('<?php echo site_url(). '/'. $this->fn .'/cetak/'; ?>','');
		}

		$document.bind('keydown', function(event) {
			console.debug(event.which);
			switch (event.which)
            {
                case 65:
                    if(event.altKey){
                        $scope.showAdd();
                        return false;
                    }
                    break;
                case 80 :
                	if(event.altKey){
                        $scope.cetak();
                        return false;
                    }
                	break;
                case 38:
                    if (!event.ctrlKey) {
                        var selected =  $("tr.success").first();
                        if (selected.prev().html() != null) {
                            selected.prev().addClass("success");
                            selected.removeClass("success");
                        }
                    }
                    break;
                case 40:
                    if (!event.ctrlKey) {
                        var selected = $("tr.success").first();
                        if (selected.next().html() != null) {
                            selected.next().addClass("success");
                            selected.removeClass("success");
                        }
                    }
                    break;
            }
    	});
		
		$scope.SearchTerminal = function (id){
			var hasil="";
			angular.forEach($scope.terminals , function(value, key){
				if(id==value.t_code){
					hasil = value.t_name;
					return false;
				}				
			});
			return hasil;
		}
        
        $scope.SearchTerminalObject = function (id){
			var hasil="";
			angular.forEach($scope.terminals , function(value, key){
				if(id==value.t_code){
					hasil = value;
					return false;
				}				
			});
			return hasil;
		}

		$scope.setNoTrayek = function(){
			$http.get('<?php echo site_url() .'/trayek/lastnumber'; ?>').
			success(function(data) {
				if(data!=undefined){
					if(data.success==true){
						$scope.obj.ty_code = $scope.obj.tasal.t_code +  $scope.obj.ttujuan.t_code + data.kode;
                	}
				}
			});	
			
		}

		//Untuk Detail
		//Add data
        $scope.showAddDetail = function (index){
        	
            var dtl = $scope.trayek.items[index].detail;
            if( $scope.trayek.items[index].ty_code >0){
                $scope.edit = -1 ;
                var obj = {
                    tyj_id:0,
                    ty_code: $scope.trayek.items[index].ty_code, 
                    j_code:'',
                    tyj_urut : dtl.length+1
                };
                $scope.trayek.items[index].detail.push(obj);
            }else{
                alert('Pilih Trayek Dulu ..!');
            }
        }

        $scope.addItemDetail = function(item) {
            var evt = "";
            if (item.tyj_id === 0 ){
                evt = "insert_data";
            }else{
                evt = "edit_data" ;
            }
            $http.get('<?php echo site_url() .'/trayek_jalan/'; ?>' + evt, {
                params: {
                    tyj_id : item.tyj_id,
                    ty_code : item.ty_code,
                    j_code : item.j_code,
                    tyj_urut : item.tyj_urut
                }
            }).
                success(function(data) {
                    if(data!==undefined){
                        if(data.success===true){
                            if (item.tyj_id === 0 ){
                                item.tyj_id = data.key;
                            }
                        }else{
                            $scope.pesanerror=data.msg;
                            $scope.errorshow=true;
                        }
                    }
                });

        }
        
        //Hapus data
        $scope.removeItemDetail = function(item, index) {
            console.log(item.tyj_id);
            if(confirm('Anda yakin HAPUS data ini? ')){
                if(item.tyj_id!=0){
                    $http.get('<?php echo site_url() .'/trayek_jalan/delete_data/'; ?>', {
                        params: {
                            'tyj_id' :  item.tyj_id
                        }
                    }).
                    success(function(data) {
                        if(data!=undefined){
                            $scope.trayek.items[index].detail.delete(index);
                        }
                    });
                }else{
                    $scope.trayek.items[index].detail.delete(index);
                }
            }
        }

	}).directive('deFocus', deEvent);
</script>